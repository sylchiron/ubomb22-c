#include "main_menu.h"
#include "common.h"
#include "resources.h"
#include "world_repo.h"

#include <math.h>

#include <allegro5/allegro_native_dialog.h>


Screen const mainMenuScreen = { &initMainMenu, &drawMainMenu, &processMainMenuEvent, NULL, &reinitMainMenu, &deinitMainMenu };


void initMainMenu(Screen * screen)
{
   MainMenu * mainMenu = (MainMenu *)al_malloc(sizeof(*mainMenu));
   *mainMenu = (MainMenu){ .difficultMode = true };
   screen->data = mainMenu;

   reinitMainMenu(screen);

   FontIndex bigButFont = FONT_BIG_BUTTON, smallButFont = FONT_SMALL_BUTTON;
   Button buttons[MENU_COUNT] = {
         [MENU_ABOUT]          = { .15f, .5f, smallButFont, 0xFFA020, "About" },
         [MENU_PLAY_FILE]      = { .5f, .6f, bigButFont, 0x80B000, "Play" },
         [MENU_PLAY_DIFFICULT] = { .8f, .55f, smallButFont, 0x800080, "Difficult mode" },
         [MENU_EDIT_NEW]       = { .3f, .8f, bigButFont, 0x00A0C0, "Editor" },
         [MENU_EDIT_FILE]      = { .3f, .9f, smallButFont, 0x20C0E0, "Open" },
         [MENU_EXIT]           = { .7f, .8f, bigButFont, 0xC00000, "Quit" },
      };
   static uint64_t const shortcuts[][MENU_COUNT] = { {
         [MENU_ABOUT] = KEY(F2),
         [MENU_EXIT] = KEY_COMB_1(CTRL, Q),
         [MENU_PLAY_FILE] = KEY_COMB_1(CTRL, R),
         [MENU_PLAY_DIFFICULT] = KEY_COMB_1(CTRL, D),
         [MENU_EDIT_NEW] = KEY_COMB_1(CTRL, N),
         [MENU_EDIT_FILE] = KEY_COMB_1(CTRL, O),
      }, {} };
   mainMenu->buttonList = createButtonList(MENU_COUNT, buttons, &mainMenuAction, shortcuts);
}


void drawMainMenu(Screen * screen)
{
   MainMenu * mainMenu = (MainMenu *)screen->data;
   al_clear_to_color(COLOR(0xFFFFFF));

   double boxProgression = fmod(screen->currentTime, 10.) / 10.;
   bool boxIsSolid = fmod(screen->currentTime, 100.) < 10.;
   drawPlayer(DIRECTION_RIGHT,
      (DISPLAY_W + globRes.cellW * 2) * boxProgression - globRes.cellW * 2, DISPLAY_H * .07f - globRes.cellH / 2.f, 1.f);
   drawTile(boxIsSolid ? TILE_SOLID_BOX : TILE_BOX,
      (DISPLAY_W + globRes.cellW * 2) * boxProgression - globRes.cellW, DISPLAY_H * .07f - globRes.cellH / 2.f, 1.f);
   double bombProgression = fmod(screen->currentTime, 2.) / 2.;
   drawBomb(bombProgression, DISPLAY_W * .1f - globRes.cellW / 2.f, DISPLAY_H * .25f - globRes.cellH / 2.f, 1.f);
   drawBomb(bombProgression, DISPLAY_W * .9f - globRes.cellW / 2.f, DISPLAY_H * .25f - globRes.cellH / 2.f, 1.f);
   drawTile(TILE_BOMB_RANGE_INC, DISPLAY_W * .15f - globRes.cellW / 2.f, DISPLAY_H * .57f - globRes.cellH / 2.f, 1.f);
   double princessHeartState = fmod(screen->currentTime, 7.),
      heartState = 1. <= princessHeartState && princessHeartState < 3. ? fabs(.5 - fmod(princessHeartState - 1., 1.)) * 2. : 1.,
      princessState = 3.5 <= princessHeartState && princessHeartState < 5.5 ? fabs(4.5 - princessHeartState) : 1.;
   drawTile(TILE_PRINCESS, DISPLAY_W * .495f - globRes.cellW / 2.f, DISPLAY_H * .48f - globRes.cellH / 2.f, princessState);
   drawTile(TILE_HEART, DISPLAY_W * .515f - globRes.cellW / 2.f, DISPLAY_H * .48f - globRes.cellH / 2.f, heartState * .8);
   drawPlayer(DIRECTION_DOWN, DISPLAY_W * .35f - globRes.cellW / 2.f, DISPLAY_H * .6f - globRes.cellH / 2.f, 1.f);
   drawPlayer(DIRECTION_LEFT, DISPLAY_W * .65f - globRes.cellW / 2.f, DISPLAY_H * .6f - globRes.cellH / 2.f, 1.f);
   if (mainMenu->difficultMode) {
      drawMonster(DIRECTION_DOWN, DISPLAY_W * .8f - globRes.cellW / 2.f, DISPLAY_H * .48f - globRes.cellH / 2.f, 1.f);
   }
   drawTile(TILE_BOMB_NB_INC, DISPLAY_W * .1f - globRes.cellW / 2.f, DISPLAY_H * .8f - globRes.cellH / 2.f, 1.f);
   drawMonster(DIRECTION_RIGHT, DISPLAY_W * .57f - globRes.cellW / 2.f, DISPLAY_H * .8f - globRes.cellH / 2.f, 1.f);
   drawDoor(mainMenu->buttonList->pointedButtonNum == MENU_EXIT,
      DISPLAY_W * .87f - globRes.cellW / 2.f, DISPLAY_H * .8f - globRes.cellH / 2.f, 1.f);
   drawTile(TILE_KEY, DISPLAY_W * .4f - globRes.cellW / 2.f, DISPLAY_H * .9f - globRes.cellH / 2.f, 1.f);

   drawCenteredText(FONT_BIGGEST, 0xA52A2A, .5f, .25f, "UBomb 2022");
   drawButtons(screen->currentTime, mainMenu->buttonList);
}


void mainMenuAction(Screen * screen, int actionCode)
{
   MainMenu * mainMenu = (MainMenu *)screen->data;
   switch (actionCode) {
      case MENU_ABOUT:
         al_show_native_message_box(glob.display, "About", "UBomb 2022 C",
            "Made by Sylvain Chiron\nUniversité de Bordeaux, Computer Science department\n"
            "For a study project led by Laurent Réveillère\nSprites given by Laurent Réveillère", NULL, 0);
         break;
      case MENU_EXIT:
         quitScreen();
         break;
      case MENU_PLAY_FILE: {
         World * world = getWorldFromDialog();
         if (world != NULL) {
            world->difficultMode = mainMenu->difficultMode;
            changeScreen(&gameScreen, world);
         }
      }  break;
      case MENU_PLAY_DIFFICULT:
         mainMenu->difficultMode = !mainMenu->difficultMode;
         mainMenu->buttonList->buttons[MENU_PLAY_DIFFICULT].text =
            mainMenu->difficultMode ? "Difficult mode" : "Non-difficult mode";
         break;
      case MENU_EDIT_NEW: {
         World * world = createWorld();
         world->difficultMode = mainMenu->difficultMode;
         changeScreen(&editorScreen, world);
      }  break;
      case MENU_EDIT_FILE: {
         World * world = getWorldFromDialog();
         if (world != NULL) {
            world->difficultMode = mainMenu->difficultMode;
            changeScreen(&editorScreen, world);
         }
      }  break;
      default: ;
   }
}

void processMainMenuEvent(Screen * screen, ALLEGRO_EVENT * evt)
{
   MainMenu * mainMenu = (MainMenu *)screen->data;
   if (evt->type == ALLEGRO_EVENT_DISPLAY_RESIZE) {
      recomputeButtonsBounds(mainMenu->buttonList, 1.f, 1.f);
   }
   processButtonsEvent(screen, mainMenu->buttonList, evt);
}


void reinitMainMenu(Screen * screen)
{
   MainMenu * mainMenu = (MainMenu *)screen->data;
   resizeDisplay(640, 480);
   if (mainMenu->buttonList != NULL) {
      recomputeButtonsBounds(mainMenu->buttonList, 1.f, 1.f);
      ALLEGRO_MOUSE_STATE mouseState;
      al_get_mouse_state(&mouseState);
      updateButtons(screen->currentTime, mainMenu->buttonList, mouseState.x, mouseState.y);
   }
}

void deinitMainMenu(Screen * screen)
{
   MainMenu * mainMenu = (MainMenu *)screen->data;
   destroyButtonList(mainMenu->buttonList);
   al_free(mainMenu);
}
