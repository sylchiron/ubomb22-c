#include "world.h"
#include "common.h"


World * createWorld()
{
   World * world = (World *)al_malloc(sizeof(*world));
   *world = (World){ .fileFormat = WORLD_FF_LEGACY_RLE };
   return world;
}

World * cloneWorld(World * originalWorld)
{
   World * world = createWorld();
   *world = *originalWorld;

   if (world->nbPlayers > 0) {
      world->players = (Player * *)al_calloc(world->nbPlayers, sizeof(*world->players));
   }

   if (world->nbLevels > 0) {
      int playerNum = 0;
      world->levels = (Level *)al_calloc(world->nbLevels, sizeof(*world->levels));
      for (int levelNum = 0; levelNum < world->nbLevels; ++levelNum) {
         Level * level = &world->levels[levelNum];
         *level = originalWorld->levels[levelNum];
         level->cells = (LevelCell *)al_calloc(level->width * level->height, sizeof(*level->cells));
         LevelMatrix(level, cells);
         forAllEntities(&originalWorld->levels[levelNum], pos, entity) {
            Entity * newEntity = addNewEntity(&cells[pos.y][pos.x], entity);
            if (newEntity->type == ENTITY_PLAYER) {
               world->players[playerNum] = &newEntity->player;
               ++playerNum;
            }
         }
      }
   }

   world->animatedEntities = NULL;
   world->directionsToPlayers = NULL;
   if (world->filePath != NULL) {
      world->filePath = (char *)al_malloc((strlen(world->filePath) + 1) * sizeof(*world->filePath));
      strcpy(world->filePath, originalWorld->filePath);
   }
   if (world->title != NULL) {
      world->title = (char *)al_malloc((strlen(world->title) + 1) * sizeof(*world->title));
      strcpy(world->title, originalWorld->title);
   }

   return world;
}

void destroyWorld(World * world)
{
   al_free(world->filePath);
   al_free(world->title);
   al_free(world->directionsToPlayers);
   AnimatedEntity * nextAnimEnt = world->animatedEntities, * animEnt;
   while ((animEnt = nextAnimEnt) != NULL) {
      nextAnimEnt = animEnt->next;
      al_free(animEnt);
   }
   for (int playerNum = 0; playerNum < world->nbPlayers; ++playerNum) {
      if (world->players[playerNum]->charProps.nbLives <= 0) {
         al_free(world->players[playerNum]);
      }
   }
   al_free(world->players);
   for (int levelNum = 0; levelNum < world->nbLevels; ++levelNum) {
      Level * level = &world->levels[levelNum];
      forAllEntitiesWithRemove(level, pos, entity) {
         al_free(entity);
      }
      al_free(level->cells);
   }
   al_free(world->levels);
   al_free(world);
}


void addLevel(World * world, int newLevelNum, int width, int height)
{
   Level * levels = (Level *)al_calloc(world->nbLevels + 1, sizeof(*levels));
   memcpy(levels, world->levels, newLevelNum * sizeof(*levels));
   memcpy(levels + newLevelNum + 1, world->levels + newLevelNum, (world->nbLevels - newLevelNum) * sizeof(*levels));
   al_free(world->levels);
   ++world->nbLevels;
   world->levels = levels;
   Level * level = &world->levels[newLevelNum];
   *level = (Level){ width, height, (LevelCell *)al_calloc(width * height, sizeof(*level->cells)) };
}

void removeLevel(World * world, int removedLevelNum)
{
   Level * level = &world->levels[removedLevelNum];
   forAllEntitiesWithRemove(level, pos, entity) {
      setPlayerToNull(world, entity);
      al_free(entity);
   }
   removeNullPlayers(world);
   al_free(world->levels[removedLevelNum].cells);
   --world->nbLevels;
   if (world->nbLevels > 0) {
      Level * levels = (Level *)al_calloc(world->nbLevels, sizeof(*levels));
      memcpy(levels, world->levels, removedLevelNum * sizeof(*levels));
      memcpy(levels + removedLevelNum, world->levels + removedLevelNum + 1, (world->nbLevels - removedLevelNum) * sizeof(*levels));
      al_free(world->levels);
      world->levels = levels;
   } else {
      al_free(world->levels);
      world->levels = NULL;
   }
}

void cloneLevel(World * world, int originalLevelNum, int newLevelNum)
{
   Level originalLevel = world->levels[originalLevelNum];
   addLevel(world, newLevelNum, originalLevel.width, originalLevel.height);
   Level * level = &world->levels[newLevelNum];
   level->surrounding = originalLevel.surrounding;
   level->canonicalMonster = originalLevel.canonicalMonster;
   LevelMatrix(level, cells);
   int nbNewPlayers = 0;
   forAllEntities(&originalLevel, pos, entity) {
      if (entity->type == ENTITY_PLAYER) {
         ++nbNewPlayers;
      }
      addNewEntity(&cells[pos.y][pos.x], entity);
   }
   if (nbNewPlayers > 0) {
      int playerNum = world->nbPlayers;
      world->nbPlayers += nbNewPlayers;
      world->players = (Player * *)al_realloc(world->players, world->nbPlayers * sizeof(*world->players));
      forAllEntities(level, pos, entity) {
         if (entity->type == ENTITY_PLAYER) {
            world->players[playerNum] = &entity->player;
            ++playerNum;
         }
      }
   }
}

void resizeLevel(World * world, Level * level, int width, int height, int xShift, int yShift)
{
   Level tmpLevel = { width, height, (LevelCell *)al_calloc(width * height, sizeof(*tmpLevel.cells)) };
   LevelMatrix(level, oldCells);
   LevelMatrix(&tmpLevel, newCells);
   for (int y = 0; y < level->height; ++y) {
      for (int x = 0; x < level->width; ++x) {
         int newX = x + xShift, newY = y + yShift;
         if (0 <= newX && newX < width && 0 <= newY && newY < height) {
            newCells[newY][newX] = oldCells[y][x];
         } else {
            Entity * nextEntity = oldCells[y][x].first, * entity;
            while ((entity = nextEntity) != NULL) {
               nextEntity = entity->next;
               setPlayerToNull(world, entity);
               al_free(entity);
            }
         }
      }
   }
   removeNullPlayers(world);
   al_free(level->cells);
   level->width = width;
   level->height = height;
   level->cells = tmpLevel.cells;
}

void accessLevel(World * world, double currentTime)
{
   Level * level = &world->levels[world->currentLevelNum];
   if (level->accessed) {
      return;
   }
   forAllEntities(level, pos, entity) {
      if (isAmong(EntityType, entity->type, ENTITY_PLAYER, ENTITY_MONSTER, ENTITY_BOMB)) {
         AnimatedEntity * animEnt = addNewAnimatedEntity(world, &(AnimatedEntity){ world->currentLevelNum, pos, entity });
         if (entity->type == ENTITY_PLAYER) {
            entity->player.animatedEntity = animEnt;
         } else if (entity->type == ENTITY_MONSTER) {
            entity->monster.nextMoveTime = currentTime + 1. / entity->monster.velocity * (1. + (double)rand() / (RAND_MAX + 1.));
         } else if (entity->type == ENTITY_BOMB) {
            entity->bomb.creationInstant += currentTime;
            entity->bomb.explosionInstant += currentTime;
         }
      }
   }
   level->accessed = true;
}


Direction getOppositeDirection(Direction dir)
{
   return (dir + DIRECTION_COUNT / 2) % DIRECTION_COUNT;
}

Direction getRandomDirection()
{
   return (double)rand() * DIRECTION_COUNT / (RAND_MAX + 1.);
}

Position shiftedPos(Position pos, Direction dir, int amount)
{
   static Position const deltas[] = { { 0, -1 }, { 1, 0 }, { 0, 1 }, { -1, 0 } };
   return (Position){ pos.x + deltas[dir].x * amount, pos.y + deltas[dir].y * amount };
}
Position nextPos(Position pos, Direction dir)
{
   return shiftedPos(pos, dir, 1);
}

bool isInsideLevel(Level const * level, Position pos)
{
   return 0 <= pos.x && pos.x < level->width && 0 <= pos.y && pos.y < level->height;
}


void addEntity(LevelCell * cell, Entity * entity)
{
   entity->prev = cell->last;
   entity->next = NULL;
   cell->last = entity;
   *(entity->prev == NULL ? &cell->first : &entity->prev->next) = entity;
}

Entity * addNewEntity(LevelCell * cell, Entity const * entity)
{
   Entity * newEntity = (Entity *)al_malloc(sizeof(*newEntity));
   *newEntity = *entity;
   addEntity(cell, newEntity);
   return newEntity;
}

void detachEntity(LevelCell * cell, Entity * entity)
{
   *(entity->prev == NULL ? &cell->first : &entity->prev->next) = entity->next;
   *(entity->next == NULL ? &cell->last : &entity->next->prev) = entity->prev;
   entity->prev = entity->next = NULL;
}

void removeEntity(LevelCell * cell, Entity * entity)
{
   detachEntity(cell, entity);
   al_free(entity);
}

void moveEntity(Entity * entity, Level * level, Position oldPos, Position newPos)
{
   detachEntity(getLevelCell(level, oldPos), entity);
   addEntity(getLevelCell(level, newPos), entity);
}


bool canStartOnSameCell(Entity const * ent1, Entity const * ent2)
{
   static EntityType const alwaysAloneTypes[] =
         { ENTITY_STONE, ENTITY_TREE, ENTITY_FRAGILE_TREE, ENTITY_BOX, ENTITY_SOLID_BOX, ENTITY_DOOR },
      otherIncompatibilities[][ENTITY_COUNT] = {
         [ENTITY_PLAYER] = { ENTITY_PRINCESS, ENTITY_BONUS },
         [ENTITY_PRINCESS] = { ENTITY_PLAYER, ENTITY_PRINCESS },
         [ENTITY_BONUS] = { ENTITY_PLAYER },
         [ENTITY_BOMB] = { ENTITY_BOMB },
      };
   return !isAmongArray(&ent1->type, alwaysAloneTypes, sizeof(ent1->type), arraySize(alwaysAloneTypes)) &&
      !isAmongArray(&ent2->type, alwaysAloneTypes, sizeof(ent2->type), arraySize(alwaysAloneTypes)) &&
      !isAmongArray(&ent2->type, otherIncompatibilities[ent1->type], sizeof(ent2->type), ENTITY_COUNT) &&
      (ent1->type != ENTITY_BONUS || ent2->type != ENTITY_BONUS || ent1->bonus.type != ent2->bonus.type);
}

bool entityIsCharacter(EntityType type)
{
   return type == ENTITY_PLAYER || type == ENTITY_MONSTER;
}

bool entityIsMovable(EntityType type)
{
   return entityIsCharacter(type) || type == ENTITY_BOX || type == ENTITY_SOLID_BOX;
}

bool doesBonusUseFactor(BonusType type)
{
   return type == BONUS_SPEED;
}

void setPlayerToNull(World * world, Entity * entity)
{
   if (entity->type == ENTITY_PLAYER) {
      for (int playerNum = 0; playerNum < world->nbPlayers; ++playerNum) {
         if (world->players[playerNum] == &entity->player) {
            world->players[playerNum] = NULL;
         }
      }
   }
}

void removeNullPlayers(World * world)
{
   int nbNullFound = 0;
   for (int playerNum = 0; playerNum < world->nbPlayers; ++playerNum) {
      if (world->players[playerNum] == NULL) {
         ++nbNullFound;
      } else {
         world->players[playerNum - nbNullFound] = world->players[playerNum];
      }
   }
   if (nbNullFound > 0) {
      world->nbPlayers -= nbNullFound;
      if (world->nbPlayers <= 0) {
         al_free(world->players);
         world->players = NULL;
      } else {
         world->players = (Player * *)al_realloc(world->players, world->nbPlayers * sizeof(*world->players));
      }
   }
}

int getNbActivePlayers(World const * world)
{
   int count = 0;
   for (int playerNum = 0; playerNum < world->nbPlayers; ++playerNum) {
      if (world->players[playerNum]->animatedEntity != NULL) {
         ++count;
      }
   }
   return count;
}


AnimatedEntity * addNewAnimatedEntity(World * world, AnimatedEntity const * animEnt)
{
   AnimatedEntity * newAnimEnt = (AnimatedEntity *)al_malloc(sizeof(*newAnimEnt));
   *newAnimEnt = *animEnt;
   newAnimEnt->next = world->animatedEntities;
   world->animatedEntities = newAnimEnt;
   return newAnimEnt;
}

void removeAnimatedEntity(AnimatedEntity * * animEntPtr)
{
   AnimatedEntity * animEnt = *animEntPtr;
   *animEntPtr = animEnt->next;
   al_free(animEnt);
}
