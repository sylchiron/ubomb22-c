#ifndef UBOMB_WORLD_H
#define UBOMB_WORLD_H


#include <stdbool.h>


#define LevelMatrix(level, cellsVarName) \
   LevelCell (* cellsVarName)[(level)->width] = (LevelCell (* )[(level)->width])(level)->cells
#define forAllEntities(level, pos, entity) \
   for (Position pos = {}; pos.y < (level)->height; ++pos.y) \
      for (pos.x = 0; pos.x < (level)->width; ++pos.x) \
         for (Entity * entity = getLevelCell(level, pos)->first; entity != NULL; entity = entity->next)
#define forAllConstEntities(level, pos, entity) \
   for (Position pos = {}; pos.y < (level)->height; ++pos.y) \
      for (pos.x = 0; pos.x < (level)->width; ++pos.x) \
         for (Entity const * entity = getConstLevelCell(level, pos)->first; entity != NULL; entity = entity->next)
#define forAllEntitiesWithRemove(level, pos, entity) \
   Entity * entity ## _next, * entity; \
   for (Position pos = {}; pos.y < level->height; ++pos.y) \
      for (pos.x = 0; pos.x < level->width && (entity ## _next = getLevelCell(level, pos)->first, true); ++pos.x) \
         while ((entity = entity ## _next) != NULL && (entity ## _next = entity->next, true))


typedef enum {
   DIRECTION_NONE = -1,
   DIRECTION_UP,
   DIRECTION_RIGHT,
   DIRECTION_DOWN,
   DIRECTION_LEFT,
   DIRECTION_COUNT
} Direction;

typedef enum {
   ENTITY_NONE,
   ENTITY_PLAYER,
   ENTITY_MONSTER,
   ENTITY_PRINCESS,
   ENTITY_STONE,
   ENTITY_TREE,
   ENTITY_FRAGILE_TREE,
   ENTITY_BOX,
   ENTITY_SOLID_BOX,
   ENTITY_BONUS,
   ENTITY_DOOR,
   ENTITY_BOMB,
   ENTITY_COUNT
} EntityType;

typedef enum {
   BONUS_NONE,
   BONUS_KEY,
   BONUS_HEART,
   BONUS_BOMB_NUMBER,
   BONUS_BOMB_RANGE,
   BONUS_SPEED,
   BONUS_COUNT
} BonusType;

typedef enum {
   SURROUNDING_DEFAULT,
   SURROUNDING_STONE,
   SURROUNDING_TREE,
   SURROUNDING_COUNT
} SurroundingType;

typedef struct Entity Entity;
typedef struct AnimatedEntity AnimatedEntity;

typedef struct {
   int x, y;
} Position;

typedef struct {
   int quantity;
   bool taken;
} Princess;

typedef struct {
   bool running;
   Position from, to;
   double duration, endInstant;
} Movement;

typedef struct {
   BonusType type;
   union {
      int amount;
      double factor;
   };
} Bonus;

typedef struct {
   bool isOpen;
   int levelIncr;
   Position targetPos;
} Door;

typedef struct {
   Movement move;
   Direction dir;
   int nbLives;
   double invincibilityDuration, invincibilityEndInstant;
   bool frozen;
} Character;

typedef struct {
   Character charProps;
   int maxNbBombs, nbPlacedBombs, bombRange;
   double bombBurningTime, bombExplosionTime;
   int nbKeys;
   Door * tookDoor;
   int nbTakenPrincesses;
   AnimatedEntity * animatedEntity;
} Player;

typedef struct {
   Character charProps;
   double velocity, nextMoveTime;
} Monster;

typedef struct {
   Player * player;
   int range, rangeReached;
   double creationInstant, explosionInstant, explosionDuration;
   bool exploded, stopped[DIRECTION_COUNT];
} Bomb;

struct Entity {
   EntityType type;
   union {
      Player player;
      Monster monster;
      Character character;
      Princess princess;
      Movement move;
      Bonus bonus;
      Door door;
      Bomb bomb;
   };
   Entity * prev, * next;
};

typedef struct {
   Entity * first, * last;
} LevelCell;

typedef struct {
   int width, height;
   LevelCell * cells;
   SurroundingType surrounding;
   Monster canonicalMonster;
   bool accessed;
} Level;

struct AnimatedEntity {
   int levelNum;
   Position pos;
   Entity * entity;
   AnimatedEntity * next;
};

typedef enum {
   WORLD_FF_UNKNOWN = -1,
   WORLD_FF_LEGACY,
   WORLD_FF_LEGACY_RLE,
   WORLD_FF_ENHANCED,
   WORLD_FF_ENHANCED_RLE,
   WORLD_FF_COUNT,
} WorldFileFormat;

typedef struct {
   int nbPlayers;
   Player * * players;
   int nbNeededPrincesses;

   int nbLevels;
   Level * levels;
   int currentLevelNum;

   AnimatedEntity * animatedEntities;
   bool difficultMode;
   Direction * directionsToPlayers;

   char * filePath, * title;
   WorldFileFormat fileFormat;
} World;


World * createWorld();
World * cloneWorld(World * originalWorld);
void destroyWorld(World * world);

void addLevel(World * world, int newLevelNum, int width, int height);
void removeLevel(World * world, int removedLevelNum);
void cloneLevel(World * world, int originalLevelNum, int newLevelNum);
void resizeLevel(World * world, Level * level, int width, int height, int xShift, int yShift);
void accessLevel(World * world, double currentTime);

Direction getRandomDirection();
Direction getOppositeDirection(Direction dir);
Position shiftedPos(Position pos, Direction dir, int amount);
Position nextPos(Position pos, Direction dir);
bool isInsideLevel(Level const * level, Position pos);
static inline LevelCell * getLevelCell(Level * level, Position pos)
{
   LevelMatrix(level, cells);
   return &cells[pos.y][pos.x];
}
static inline LevelCell const * getConstLevelCell(Level const * level, Position pos)
{
   const LevelMatrix(level, cells);
   return &cells[pos.y][pos.x];
}

void addEntity(LevelCell * cell, Entity * entity);
Entity * addNewEntity(LevelCell * cell, Entity const * entity);
void detachEntity(LevelCell * cell, Entity * entity);
void removeEntity(LevelCell * cell, Entity * entity);
void removeIncompatibleEntities(LevelCell * cell, Entity const * newEntity);
void moveEntity(Entity * entity, Level * level, Position oldPos, Position newPos);

bool canStartOnSameCell(Entity const * ent1, Entity const * ent2);
bool entityIsCharacter(EntityType type);
bool entityIsMovable(EntityType type);
bool doesBonusUseFactor(BonusType type);
void setPlayerToNull(World * world, Entity * entity);
void removeNullPlayers(World * world);
int getNbActivePlayers(World const * world);

AnimatedEntity * addNewAnimatedEntity(World * world, AnimatedEntity const * animEnt);
void removeAnimatedEntity(AnimatedEntity * * animEntPtr);


// draw
void drawEntity(double currentTime, bool forEditor, Entity const * entity, float drawX, float drawY, float opacity);
void drawLevel(double currentTime, bool forEditor, Level const * level, int x1, int y1, int x2, int y2);


#endif
