#include "editor_ui.h"
#include "common.h"
#include "resources.h"
#include "editor.h"
#include "world_repo.h"

#include <ctype.h>

#include <allegro5/allegro_primitives.h>


void editorComputeWidgetsBounds(Editor * editor)
{
   editor->toolbars.bounds = (Bounds){ 0, 0, DISPLAY_W, editor->toolbars.nbBars * editor->toolbars.barHeight };
   editor->entListPanel.bounds = (Bounds){ 0, editor->toolbars.bounds.y2, 60, DISPLAY_H };
   editor->propPanel.bounds = (Bounds){ DISPLAY_W - 160, editor->toolbars.bounds.y2, DISPLAY_W, DISPLAY_H };
   editor->levelCanvas.bounds = (Bounds){ editor->entListPanel.bounds.x2, editor->toolbars.bounds.y2,
      editor->propPanel.bounds.x1, DISPLAY_H };

   EntitiesListPanel * entList = &editor->entListPanel;
   entList->nbPages = (nbDefaultEntities - 1) / (getBoundsH(entList->bounds) / entList->heightForType - 1) + 1;
   entList->curPageNum = entList->selEntityNum * entList->nbPages / nbDefaultEntities;
}

void editorInitButtons(Editor * editor)
{
   destroyButtonList(editor->buttonList);

   Button buttons[EDITOR_COUNT] = {
         [EDITOR_UNDO]          = { .text = "Undo" },
         [EDITOR_REDO]          = { .text = "Redo" },
         [EDITOR_PLACEHOLDER_1] = { .text = "" },
         [EDITOR_HELP]          = { .text = "Help" },
         [EDITOR_EXIT]          = { .text = "Quit" },
         [EDITOR_NEW]           = { .text = "New" },
         [EDITOR_OPEN]          = { .text = "Open" },
         [EDITOR_SAVE]          = { .text = "Save" },
         [EDITOR_SAVE_AS]       = { .text = "Save as" },
         [EDITOR_PLAY]          = { .text = "Play" },
         [EDITOR_LEVEL_FIRST]   = { .text = "<< First" },
         [EDITOR_LEVEL_PREV]    = { .text = "< Prev" },
         [EDITOR_LEVEL_CHOOSE]  = { .editable = true, .eraseOnEdit = true },
         [EDITOR_LEVEL_NEXT]    = { .text = "Next >" },
         [EDITOR_LEVEL_LAST]    = { .text = "Last >>" },
         [EDITOR_LEVEL_ADD]     = { .text = "Add level" },
         [EDITOR_LEVEL_CLONE]   = { .text = "Clone level" },
         [EDITOR_LEVEL_REMOVE]  = { .text = "Remove level" },
         [EDITOR_LEVEL_MOVE_BEFORE] = { .text = "<<" },
         [EDITOR_LEVEL_MOVE_AFTER]  = { .text = ">>" },
         [EDITOR_WORLD_PROPERTIES]  = { .text = "World properties", .color = 0x0040A0 },
         [EDITOR_LEVEL_PROPERTIES]  = { .text = "Level properties", .color = 0x00A0A0 },
         [EDITOR_LEVEL_INC_SIZE]    = { .text = "Increase size" },
         [EDITOR_LEVEL_CROP]        = { .text = "Crop", .color = 0xC00000 },
         [EDITOR_LEVEL_SIZE_CHOOSE] = { .editable = true },
         [EDITOR_ENTITIES_PUT]      = { .text = "Put entities", .color = 0xE07000 },
         [EDITOR_ENTITIES_MOVE]     = { .text = "Move entities", .color = 0xC000C0 },
         [EDITOR_ENTITIES_PROPS]    = { .text = "Entities’ props", .color = 0xA06040 },
         [EDITOR_ENTITIES_REMOVE]   = { .text = "Remove entities", .color = 0xC00000 },
         [EDITOR_PLAY_AT_POS]       = { .text = "Play at position", .color = 0x40A020 },

         [EDITOR_ENTITY_PANEL_PREV] = { .text = "<" },
         [EDITOR_ENTITY_PANEL_NEXT] = { .text = ">" },

         [EDITOR_WORLD_TITLE_LABEL] = { .text = "Title" },
         [EDITOR_WORLD_TITLE] = { .editable = true },
         [EDITOR_WORLD_FILE_FORMAT_LABEL] = { .text = "Format" },
         [EDITOR_WORLD_NB_PRINCESSES_LABEL] = { .text = "Number of princesses to find" },
         [EDITOR_WORLD_NB_PRINCESSES] = { .editable = true },
         [EDITOR_LEVEL_SURROUNDINGS_LABEL] = { .text = "Surroundings" },
         [EDITOR_LEVEL_MONSTER_NB_LIVES_LABEL] = { .text = "Number of lives" },
         [EDITOR_LEVEL_MONSTER_NB_LIVES] = { .editable = true },
         [EDITOR_LEVEL_MONSTER_MOVE_DURATION_LABEL] = { .text = "Move duration (s)" },
         [EDITOR_LEVEL_MONSTER_MOVE_DURATION] = { .editable = true },
         [EDITOR_LEVEL_MONSTER_INVINCIBILITY_DURATION_LABEL] = { .text = "Invincibility duration (s)" },
         [EDITOR_LEVEL_MONSTER_INVINCIBILITY_DURATION] = { .editable = true },
         [EDITOR_LEVEL_MONSTER_VELOCITY_LABEL] = { .text = "Velocity (1/s)" },
         [EDITOR_LEVEL_MONSTER_VELOCITY] = { .editable = true },
         [EDITOR_LEVEL_MONSTER_APPLY_TO_ALL] = { .text = "Apply to all monsters of the level" },
         [EDITOR_ENTITY_CHARACTER_NB_LIVES_LABEL] = { .text = "Number of lives" },
         [EDITOR_ENTITY_CHARACTER_NB_LIVES] = { .editable = true },
         [EDITOR_ENTITY_CHARACTER_MOVE_DURATION_LABEL] = { .text = "Move duration (s)" },
         [EDITOR_ENTITY_CHARACTER_MOVE_DURATION] = { .editable = true },
         [EDITOR_ENTITY_CHARACTER_INVINCIBILITY_DURATION_LABEL] = { .text = "Invincibility duration (s)" },
         [EDITOR_ENTITY_CHARACTER_INVINCIBILITY_DURATION] = { .editable = true },
         [EDITOR_ENTITY_PLAYER_DIRECTION_LABEL] = { .text = "Direction" },
         [EDITOR_ENTITY_PLAYER_MAX_NB_BOMBS_LABEL] = { .text = "Maximum number of placed bombs" },
         [EDITOR_ENTITY_PLAYER_MAX_NB_BOMBS] = { .editable = true },
         [EDITOR_ENTITY_PLAYER_BOMB_RANGE_LABEL] = { .text = "Bombs’ range" },
         [EDITOR_ENTITY_PLAYER_BOMB_RANGE] = { .editable = true },
         [EDITOR_ENTITY_PLAYER_BOMB_BURNING_TIME_LABEL] = { .text = "Bombs’ burning duration" },
         [EDITOR_ENTITY_PLAYER_BOMB_BURNING_TIME] = { .editable = true },
         [EDITOR_ENTITY_PLAYER_BOMB_EXPLOSION_TIME_LABEL] = { .text = "Bombs’ explosion duration" },
         [EDITOR_ENTITY_PLAYER_BOMB_EXPLOSION_TIME] = { .editable = true },
         [EDITOR_ENTITY_MONSTER_VELOCITY_LABEL] = { .text = "Velocity (1/s)" },
         [EDITOR_ENTITY_MONSTER_VELOCITY] = { .editable = true },
         [EDITOR_ENTITY_BONUS_AMOUNT] = { .editable = true },
         [EDITOR_ENTITY_DOOR_OPEN_LABEL] = { .text = "Open" },
         [EDITOR_ENTITY_DOOR_LEVEL_INCR_LABEL] = { .text = "Level change" },
         [EDITOR_ENTITY_DOOR_LEVEL_INCR] = { .editable = true },
         [EDITOR_ENTITY_DOOR_TARGET_POS_LABEL] = { .text = "Target position", .color = 0xC000C0 },
         [EDITOR_ENTITY_DOOR_TARGET_POS] = { .editable = true },
         [EDITOR_ENTITY_BOMB_RANGE_LABEL] = { .text = "Range" },
         [EDITOR_ENTITY_BOMB_RANGE] = { .editable = true },
         [EDITOR_ENTITY_BOMB_BURNING_DURATION_LABEL] = { .text = "Burning duration (s)" },
         [EDITOR_ENTITY_BOMB_BURNING_DURATION] = { .editable = true },
         [EDITOR_ENTITY_BOMB_EXPLOSION_DURATION_LABEL] = { .text = "Explosion duration (s)" },
         [EDITOR_ENTITY_BOMB_EXPLOSION_DURATION] = { .editable = true },
      };
   editorUpdateButtonTexts(editor, buttons);
   buttons[editor->editMode].highlighted = true;
   FontIndex fontIndex = FONT_SMALL_BUTTON;

   // Top toolbar
   int rowNum = 0;
   for (int buttonNum = 0; buttonNum < EDITOR_TOPBAR_END; ++buttonNum) {
      while (getButtonText(&buttons[buttonNum]) == NULL) {
         ++buttonNum;
      }
      int rowFirstButNum = buttonNum;
      while (getButtonText(&buttons[buttonNum]) != NULL) {
         ++buttonNum;
      }
      int nbInRow = buttonNum - rowFirstButNum;
      for (int buttonOfRowNum = rowFirstButNum; buttonOfRowNum < buttonNum; ++buttonOfRowNum) {
         Button * button = &buttons[buttonOfRowNum];
         button->x = (editor->toolbars.bounds.x1 + (buttonOfRowNum - rowFirstButNum + .5f)) / nbInRow;
         button->y = (editor->toolbars.bounds.y1 + (rowNum + .5f) * editor->toolbars.barHeight) / DISPLAY_H;
         button->fontIndex = fontIndex;
      }
      ++rowNum;
   }

   // Other buttons
   for (int buttonNum = EDITOR_TOPBAR_END + 1; buttonNum < EDITOR_COUNT; ++buttonNum) {
      buttons[buttonNum].fontIndex = fontIndex;
   }
   float propPanelW =  getBoundsW(editor->propPanel.bounds) - 2.f;
   for (int buttonNum = EDITOR_PROPERTIES; buttonNum < EDITOR_PROPERTIES_END; ++buttonNum) {
      Button * button = &buttons[buttonNum];
      button->maxW = propPanelW;
      button->relativeY = true;
      button->vAlignTop = true;
   }
   buttons[EDITOR_PROPERTIES_WORLD + 1].relativeY = buttons[EDITOR_PROPERTIES_LEVEL + 1].relativeY =
      buttons[EDITOR_PROPERTIES_ENTITY_CHARACTER + 1].relativeY = buttons[EDITOR_ENTITY_MONSTER_VELOCITY_LABEL].relativeY =
      buttons[EDITOR_PROPERTIES_ENTITY_BONUS + 1].relativeY = buttons[EDITOR_PROPERTIES_ENTITY_DOOR + 1].relativeY =
      buttons[EDITOR_PROPERTIES_ENTITY_BOMB + 1].relativeY = false;

   static uint64_t const shortcuts[][EDITOR_COUNT] = { {
         [EDITOR_UNDO] = KEY_COMB_1(CTRL, Z),
         [EDITOR_REDO] = KEY_COMB_2(CTRL, SHIFT, Z),
         [EDITOR_HELP] = KEY(F1),
         [EDITOR_EXIT] = KEY_COMB_1(CTRL, Q),
         [EDITOR_NEW] = KEY_COMB_1(CTRL, N),
         [EDITOR_OPEN] = KEY_COMB_1(CTRL, O),
         [EDITOR_SAVE] = KEY_COMB_1(CTRL, S),
         [EDITOR_SAVE_AS] = KEY_COMB_1(ALT, S),
         [EDITOR_PLAY] = KEY_COMB_1(CTRL, R),
         [EDITOR_LEVEL_FIRST] = KEY(HOME),
         [EDITOR_LEVEL_PREV] = KEY(PGUP),
         [EDITOR_LEVEL_NEXT] = KEY(PGDN),
         [EDITOR_LEVEL_LAST] = KEY(END),
      }, {
         [EDITOR_REDO] = KEY_COMB_1(CTRL, Y),
      }, {} };
   editor->buttonList = createButtonList(EDITOR_COUNT, buttons, editorAction, shortcuts);

   editorRepositionButtons(editor);
}

void editorUpdateButtonTexts(Editor * editor, Button * buttons)
{
   World * world = editor->world;
   Level * level = &world->levels[world->currentLevelNum];
   char const * worldFFTexts[] =
         { "Legacy uncompressed", "Legacy RLE-compressed", "Enhanced uncompressed", "Enhanced RLE-compressed" },
      * levelSurrTexts[] = { "Default", "Stone", "Trees" };
   ALLEGRO_USTR * texts[EDITOR_COUNT] = {
         [EDITOR_LEVEL_CHOOSE] = al_ustr_newf("%d / %d", world->currentLevelNum + 1, world->nbLevels),
         [EDITOR_LEVEL_SIZE_CHOOSE] = al_ustr_newf("%d×%d", level->width, level->height),
         [EDITOR_WORLD_TITLE] = al_ustr_new(world->title != NULL ? world->title : "(None)"),
         [EDITOR_WORLD_FILE_FORMAT] = al_ustr_new(worldFFTexts[world->fileFormat]),
         [EDITOR_WORLD_NB_PRINCESSES] = al_ustr_newf("%d", world->nbNeededPrincesses),
         [EDITOR_LEVEL_SURROUNDINGS] = al_ustr_new(levelSurrTexts[level->surrounding]),
         [EDITOR_LEVEL_MONSTER_NB_LIVES] = al_ustr_newf("%d", level->canonicalMonster.charProps.nbLives),
         [EDITOR_LEVEL_MONSTER_MOVE_DURATION] = al_ustr_newf("%lg", level->canonicalMonster.charProps.move.duration),
         [EDITOR_LEVEL_MONSTER_INVINCIBILITY_DURATION] =
            al_ustr_newf("%lg", level->canonicalMonster.charProps.invincibilityDuration),
         [EDITOR_LEVEL_MONSTER_VELOCITY] = al_ustr_newf("%lg", level->canonicalMonster.velocity),
      };
   Entity * propEntity = editor->propPanel.entity;
   if (propEntity == NULL) {
      ALLEGRO_MOUSE_STATE mouseState;
      al_get_mouse_state(&mouseState);
      if (isInBounds(editor->levelCanvas.strictBounds, mouseState.x, mouseState.y)) {
         Level * level = &editor->world->levels[editor->world->currentLevelNum];
         Position tilePos = editorGetPointedTilePos(editor, mouseState.x, mouseState.y);
         propEntity = getLevelCell(level, tilePos)->last;
      }
   }
   bool propBonusUsesFactor = propEntity != NULL && propEntity->type == ENTITY_BONUS && doesBonusUseFactor(propEntity->bonus.type);
   if (propEntity != NULL) {
      Character * charProps = NULL;
      switch (propEntity->type) {
         case ENTITY_PLAYER:
            charProps = &propEntity->player.charProps;
            char const * directionName = directionNames[propEntity->player.charProps.dir];
            texts[EDITOR_ENTITY_PLAYER_DIRECTION] = al_ustr_new(directionName);
            al_ustr_set_chr(texts[EDITOR_ENTITY_PLAYER_DIRECTION], 0, toupper(directionName[0]));
            texts[EDITOR_ENTITY_PLAYER_MAX_NB_BOMBS] = al_ustr_newf("%d", propEntity->player.maxNbBombs);
            texts[EDITOR_ENTITY_PLAYER_BOMB_RANGE] = al_ustr_newf("%d", propEntity->player.bombRange);
            texts[EDITOR_ENTITY_PLAYER_BOMB_BURNING_TIME] = al_ustr_newf("%lg", propEntity->player.bombBurningTime);
            texts[EDITOR_ENTITY_PLAYER_BOMB_EXPLOSION_TIME] = al_ustr_newf("%lg", propEntity->player.bombExplosionTime);
            break;
         case ENTITY_MONSTER:
            charProps = &propEntity->monster.charProps;
            texts[EDITOR_ENTITY_MONSTER_VELOCITY] = al_ustr_newf("%lg", propEntity->monster.velocity);
            break;
         case ENTITY_BONUS:
            texts[EDITOR_ENTITY_BONUS_AMOUNT] = propBonusUsesFactor ? al_ustr_newf("%lg", propEntity->bonus.factor) :
               al_ustr_newf("%d", propEntity->bonus.amount);
            break;
         case ENTITY_PRINCESS:
            texts[EDITOR_ENTITY_BONUS_AMOUNT] = al_ustr_newf("%d", propEntity->princess.quantity);
            break;
         case ENTITY_DOOR:
            texts[EDITOR_ENTITY_DOOR_OPEN] = al_ustr_new(propEntity->door.isOpen ? "Yes" : "No");
            texts[EDITOR_ENTITY_DOOR_LEVEL_INCR] = al_ustr_newf("%d", propEntity->door.levelIncr);
            texts[EDITOR_ENTITY_DOOR_TARGET_POS] =
               al_ustr_newf("%d×%d", propEntity->door.targetPos.x, propEntity->door.targetPos.y);
            break;
         case ENTITY_BOMB:
            texts[EDITOR_ENTITY_BOMB_RANGE] = al_ustr_newf("%d", propEntity->bomb.range);
            texts[EDITOR_ENTITY_BOMB_BURNING_DURATION] = al_ustr_newf("%lg", propEntity->bomb.explosionInstant);
            texts[EDITOR_ENTITY_BOMB_EXPLOSION_DURATION] = al_ustr_newf("%lg", propEntity->bomb.explosionDuration);
            break;
         default: ;
      }
      if (charProps != NULL) {
         texts[EDITOR_ENTITY_CHARACTER_NB_LIVES] = al_ustr_newf("%d", charProps->nbLives);
         texts[EDITOR_ENTITY_CHARACTER_MOVE_DURATION] = al_ustr_newf("%lg", charProps->move.duration);
         texts[EDITOR_ENTITY_CHARACTER_INVINCIBILITY_DURATION] = al_ustr_newf("%lg", charProps->invincibilityDuration);
      }
   }
   if (buttons == NULL) {
      buttons = editor->buttonList->buttons;
   }
   buttons[EDITOR_ENTITY_BONUS_AMOUNT_LABEL].text = propBonusUsesFactor ? "Factor" : "Quantity";
   for (int buttonNum = 0; buttonNum < EDITOR_COUNT; ++buttonNum) {
      Button * button = &buttons[buttonNum];
      if (texts[buttonNum] != NULL) {
         if (button->ustr != NULL && editor->buttonList->editedButtonNum == buttonNum) {
            al_ustr_free(texts[buttonNum]);
         } else {
            al_ustr_free(button->ustr);
            button->ustr = texts[buttonNum];
         }
      }
   }
   if (editor->buttonList != NULL) {
      editorRepositionButtons(editor);
   }
}

void editorRepositionButtons(Editor * editor)
{
   int buttonNumInRow = 0;
   for (int buttonNum = EDITOR_ENTITY; buttonNum < EDITOR_ENTITY_END; ++buttonNum) {
      Button * button = &editor->buttonList->buttons[buttonNum];
      if (getButtonText(button) != NULL) {
         ++buttonNumInRow;
         button->x = (editor->entListPanel.bounds.x1 + editor->entListPanel.bounds.x2) * buttonNumInRow / 3.f / DISPLAY_W;
         button->y = (editor->entListPanel.bounds.y2 - editor->entListPanel.heightForType / 2.f) / DISPLAY_H;
      }
   }

   float x = (editor->propPanel.bounds.x1 + editor->propPanel.bounds.x2) / 2.f / DISPLAY_W;
   for (int buttonNum = EDITOR_PROPERTIES; buttonNum < EDITOR_PROPERTIES_END; ++buttonNum) {
      Button * buttons = editor->buttonList->buttons + buttonNum;
      if (buttons[0].text != NULL) {
         buttons[0].x = x;
         buttons[0].y = (float)editor->propPanel.spacing / DISPLAY_H;
         if (buttons[1].ustr != NULL) {
            ++buttonNum;
            buttons[1].x = x;
            buttons[1].y = (float)editor->propPanel.labelSpacing / DISPLAY_H;
         }
      }
   }
   Button * buttons = editor->buttonList->buttons;
   int bigFontH = al_get_font_line_height(globRes.fonts[FONT_SECTION_TITLE]),
      smallFontH = al_get_font_line_height(globRes.fonts[FONT_SMALL_BUTTON]);
   float y = (float)(editor->propPanel.bounds.y1 + bigFontH + editor->propPanel.spacing * 2) / DISPLAY_H;
   buttons[EDITOR_PROPERTIES_WORLD + 1].y = buttons[EDITOR_PROPERTIES_LEVEL + 1].y = y;
   buttons[EDITOR_PROPERTIES_LEVEL_MONSTER + 1].y = (float)(bigFontH + editor->propPanel.spacing * 2) / DISPLAY_H;

   Entity * propEntity = editor->propPanel.entity;
   if (propEntity == NULL) {
      ALLEGRO_MOUSE_STATE mouseState;
      al_get_mouse_state(&mouseState);
      if (isInBounds(editor->levelCanvas.strictBounds, mouseState.x, mouseState.y)) {
         Level * level = &editor->world->levels[editor->world->currentLevelNum];
         Position tilePos = editorGetPointedTilePos(editor, mouseState.x, mouseState.y);
         propEntity = getLevelCell(level, tilePos)->last;
      }
   }
   EntityType propEntityType = propEntity == NULL ? ENTITY_NONE : propEntity->type;
   y += (float)(bigFontH + smallFontH * 3 + editor->propPanel.spacing * 4) / DISPLAY_H;
   buttons[EDITOR_PROPERTIES_ENTITY_CHARACTER + 1].y = buttons[EDITOR_PROPERTIES_ENTITY_DOOR + 1].y =
      buttons[EDITOR_PROPERTIES_ENTITY_BOMB + 1].y = y;
   buttons[EDITOR_ENTITY_MONSTER_VELOCITY_LABEL].y = y +
      (float)(smallFontH * 7 + (editor->propPanel.spacing + editor->propPanel.labelSpacing) * 3) / DISPLAY_H;
   buttons[EDITOR_PROPERTIES_ENTITY_BONUS + 1].y = y +
      (float)(propEntityType == ENTITY_PRINCESS ? 0 : smallFontH + editor->propPanel.spacing) / DISPLAY_H;

   recomputeButtonsBounds(editor->buttonList, 1.f, 1.f);
}


void drawToolbars(Toolbars const * toolbars, ButtonList const * buttonList)
{
   Bounds bounds = toolbars->bounds;
   al_draw_filled_rectangle(bounds.x1, bounds.y1, bounds.x2, bounds.y2, COLOR(0xE0E0E0));
   for (int topBarI = 1; topBarI <= toolbars->nbBars; ++topBarI) {
      al_draw_filled_rectangle(bounds.x1, bounds.y1 + topBarI * toolbars->barHeight - 1,
         bounds.x2, bounds.y1 + topBarI * toolbars->barHeight, COLOR(0x404040));
   }
   Button * moveButs = buttonList->buttons + EDITOR_LEVEL_MOVE_BEFORE;
   drawCenteredText(moveButs[0].fontIndex, 0x808080, (moveButs[0].x + moveButs[1].x) / 2.f, moveButs[0].y, "Move level");
}

void drawEntitiesListPanel(EntitiesListPanel const * panel)
{
   Bounds bounds = panel->bounds;
   al_draw_filled_rectangle(bounds.x1, bounds.y1, bounds.x2, bounds.y2, COLOR(0xE0E0E0));
   al_draw_filled_rectangle(bounds.x2 - 1, bounds.y1, bounds.x2, bounds.y2, COLOR(0x404040));
   int firstEntityNum = panel->curPageNum * nbDefaultEntities / panel->nbPages,
      lastEntityNum = (panel->curPageNum + 1) * nbDefaultEntities / panel->nbPages;
   if (firstEntityNum <= panel->selEntityNum && panel->selEntityNum < lastEntityNum) {
      int selectedNumOnPage = panel->selEntityNum - firstEntityNum;
      al_draw_filled_rectangle(bounds.x1, bounds.y1 + selectedNumOnPage * panel->heightForType,
         bounds.x2, bounds.y1 + (selectedNumOnPage + 1) * panel->heightForType, COLOR(0x808080));
   }
   float drawX = bounds.x1 + (getBoundsW(bounds) - globRes.cellW) / 2.f,
      drawY = bounds.y1 + (panel->heightForType - globRes.cellH) / 2.f;
   for (int entityNum = firstEntityNum; entityNum < lastEntityNum; ++entityNum) {
      if (defaultEntities[entityNum].type == ENTITY_NONE) {
         al_draw_line(drawX + globRes.cellW * .35f, drawY + globRes.cellH * .35f,
            drawX + globRes.cellW * .65f, drawY + globRes.cellH * .65f, COLOR(0xC00000), 2.f);
         al_draw_line(drawX + globRes.cellW * .35f, drawY + globRes.cellH * .65f,
            drawX + globRes.cellW * .65f, drawY + globRes.cellH * .35f, COLOR(0xC00000), 2.f);
      } else {
         drawEntity(0., true, &defaultEntities[entityNum], drawX, drawY, 1.f);
      }
      drawY += panel->heightForType;
   }
}

void drawPropertiesPanel(PropertiesPanel const * panel, Editor const * editor)
{
   Bounds bounds = panel->bounds;
   al_draw_filled_rectangle(bounds.x1, bounds.y1, bounds.x2, bounds.y2, COLOR(0xE0E0E0));
   al_draw_filled_rectangle(bounds.x1, bounds.y1, bounds.x1 + 1, bounds.y2, COLOR(0x404040));
   ALLEGRO_FONT * font = globRes.fonts[FONT_SECTION_TITLE], * smallFont = globRes.fonts[FONT_SMALL_BUTTON];
   int fontH = al_get_font_line_height(font), smallFontH = al_get_font_line_height(smallFont);
   float x = (bounds.x1 + bounds.x2) / 2.f, y = bounds.y1 + panel->spacing;
   Entity const * entity = panel->entity;
   int firstButton = 0, lastButton = 0;
   if (editor->editMode == EDITOR_WORLD_PROPERTIES) {
      al_draw_text(font, COLOR(0x606060), x, y, ALLEGRO_ALIGN_CENTRE, "World properties");
      firstButton = EDITOR_PROPERTIES_WORLD;
      lastButton = EDITOR_PROPERTIES_LEVEL;
   } else if (editor->editMode == EDITOR_LEVEL_PROPERTIES) {
      al_draw_text(font, COLOR(0x606060), x, y, ALLEGRO_ALIGN_CENTRE, "Level properties");
      y = editor->buttonList->buttons[EDITOR_PROPERTIES_LEVEL_MONSTER - 1].bounds.y2 + panel->spacing;
      al_draw_text(font, COLOR(0x606060), x, y, ALLEGRO_ALIGN_CENTRE, "Default monster");
      firstButton = EDITOR_PROPERTIES_LEVEL;
      lastButton = EDITOR_PROPERTIES_ENTITY;
   } else if (editor->editMode == EDITOR_LEVEL_CROP) {
      ALLEGRO_MOUSE_STATE mouseState;
      al_get_mouse_state(&mouseState);
      if (isInBounds(editor->levelCanvas.bounds, mouseState.x, mouseState.y)) {
         Position tilePos = editorGetPointedTilePos(editor, mouseState.x, mouseState.y);
         Bounds cropBounds;
         editorGetCropBounds(&editor->world->levels[editor->world->currentLevelNum], tilePos, &cropBounds);
         al_draw_text(font, COLOR(0x606060), x, y, ALLEGRO_ALIGN_CENTRE, "Cropping");
         y += fontH + panel->spacing;
         al_draw_textf(smallFont, COLOR(0x808080), x, y, ALLEGRO_ALIGN_CENTRE, "%d×%d – %d×%d",
            cropBounds.x1, cropBounds.y1, cropBounds.x2 - 1, cropBounds.y2 - 1);
         y += smallFontH + panel->spacing;
         al_draw_textf(smallFont, COLOR(0x808080), x, y, ALLEGRO_ALIGN_CENTRE, "%d×%d",
            getBoundsW(cropBounds), getBoundsH(cropBounds));
      }
   } else {
      int levelNum = panel->entityLevelNum;
      Position tilePos = panel->entityPos;
      if (panel->entity == NULL) {
         ALLEGRO_MOUSE_STATE mouseState;
         al_get_mouse_state(&mouseState);
         if (isInBounds(editor->levelCanvas.strictBounds, mouseState.x, mouseState.y)) {
            levelNum = editor->world->currentLevelNum;
            tilePos = editorGetPointedTilePos(editor, mouseState.x, mouseState.y);
         } else {
            levelNum = 0;
            tilePos = (Position){ -1, -1 };
         }
      }
      Level const * level = &editor->world->levels[levelNum];
      if (isInsideLevel(level, tilePos)) {
         int nbEntities = 0;
         for (Entity const * entity2 = getConstLevelCell(level, tilePos)->first; entity2 != NULL; entity2 = entity2->next) {
            ++nbEntities;
         }
         entity = entity != NULL ? entity : getConstLevelCell(level, tilePos)->last;
         al_draw_text(font, COLOR(0x606060), x, y, ALLEGRO_ALIGN_CENTRE, "Cell properties");
         y += fontH + panel->spacing;
         al_draw_textf(smallFont, COLOR(0x808080), x, y, ALLEGRO_ALIGN_CENTRE, "Level %d, %d×%d",
            levelNum + 1, tilePos.x, tilePos.y);
         y += smallFontH + panel->spacing;
         if (nbEntities > 0) {
            al_draw_textf(smallFont, COLOR(0x808080), x, y, ALLEGRO_ALIGN_CENTRE,
               nbEntities == 1 ? "1 entity" : "%d entities", nbEntities);
            y += smallFontH + panel->spacing;
         }
      }
      if (entity != NULL) {
         static char const * entityTypeNames[] = { "Nothing", "Player", "Monster", "Princess", "Stone", "Tree", "Fragile tree",
               "Box", "Solid box", "Bonus", "Door", "Bomb" };
         static char const * bonusTypeNames[] = { "Nothing", "Key", "Health", "Bomb number", "Bomb range", "Speed" };
         al_draw_text(font, COLOR(0x606060), x, y, ALLEGRO_ALIGN_CENTRE, "Entity properties");
         y += fontH + panel->spacing;
         al_draw_text(smallFont, COLOR(0x606060), x, y, ALLEGRO_ALIGN_CENTRE, entityTypeNames[entity->type]);
         switch (entity->type) {
            case ENTITY_PLAYER:
            case ENTITY_MONSTER:
               firstButton = EDITOR_PROPERTIES_ENTITY_CHARACTER;
               lastButton = EDITOR_ENTITY_MONSTER_VELOCITY_LABEL;
               break;
            case ENTITY_BONUS:
               y += smallFontH + panel->spacing;
               al_draw_text(smallFont, COLOR(0x606060), x, y, ALLEGRO_ALIGN_CENTRE, bonusTypeNames[entity->bonus.type]);
            case ENTITY_PRINCESS:
               firstButton = EDITOR_PROPERTIES_ENTITY_BONUS;
               lastButton = EDITOR_PROPERTIES_ENTITY_DOOR;
               break;
            case ENTITY_DOOR:
               firstButton = EDITOR_PROPERTIES_ENTITY_DOOR;
               lastButton = EDITOR_PROPERTIES_ENTITY_BOMB;
               break;
            case ENTITY_BOMB:
               firstButton = EDITOR_PROPERTIES_ENTITY_BOMB;
               lastButton = EDITOR_PROPERTIES_END;
               break;
            default: ;
         }
      }
   }
   for (int buttonNum = EDITOR_PROPERTIES; buttonNum < EDITOR_PROPERTIES_END; ++buttonNum) {
      Button * button = &editor->buttonList->buttons[buttonNum];
      if (getButtonText(button)) {
         button->hidden = buttonNum < firstButton || lastButton <= buttonNum;
      }
   }
   if (entity != NULL && entity->type == ENTITY_MONSTER) {
      for (int buttonNum = EDITOR_ENTITY_PLAYER_DIRECTION_LABEL; buttonNum <= EDITOR_ENTITY_MONSTER_VELOCITY; ++buttonNum) {
         Button * button = &editor->buttonList->buttons[buttonNum];
         button->hidden = !button->hidden;
      }
   }
}

void drawEditorUI(Editor const * editor, double currentTime)
{
   drawToolbars(&editor->toolbars, editor->buttonList);
   drawEntitiesListPanel(&editor->entListPanel);
   drawPropertiesPanel(&editor->propPanel, editor);
   drawButtons(currentTime, editor->buttonList);
}


void editorSelectEntityToPut(Editor * editor, int mouseX, int mouseY)
{
   EntitiesListPanel * panel = &editor->entListPanel;
   if (editor->clickedWidget == panel && isInBounds(panel->bounds, mouseX, mouseY)) {
      int firstEntityNum = panel->curPageNum * nbDefaultEntities / panel->nbPages,
         lastEntityNum = (panel->curPageNum + 1) * nbDefaultEntities / panel->nbPages,
         selEntityNum = firstEntityNum + (mouseY - panel->bounds.y1) / panel->heightForType;
      if (selEntityNum < lastEntityNum) {
         panel->selEntityNum = selEntityNum;
      }
   }
}

void processEditorUIEventBefore(Screen * screen, ALLEGRO_EVENT * evt)
{
   Editor * editor = (Editor *)screen->data;
   processButtonsEvent(screen, editor->buttonList, evt);
   switch (evt->type) {
      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
         if (evt->mouse.button == 1) {
            if (isInBounds(editor->entListPanel.bounds, evt->mouse.x, evt->mouse.y)) {
               editor->clickedWidget = &editor->entListPanel;
               editorSelectEntityToPut(editor, evt->mouse.x, evt->mouse.y);
            } else if (isInBounds(editor->levelCanvas.bounds, evt->mouse.x, evt->mouse.y)) {
               editor->clickedWidget = &editor->levelCanvas;
            }
         }
         break;
      case ALLEGRO_EVENT_MOUSE_AXES:
      case ALLEGRO_EVENT_MOUSE_WARPED: {
         ALLEGRO_MOUSE_STATE mouseState;
         al_get_mouse_state(&mouseState);
         if ((mouseState.buttons & 1) == 1) {
            editorSelectEntityToPut(editor, mouseState.x, mouseState.y);
         }
      }  break;
   }
}

void processEditorUIEventAfter(Screen * screen, ALLEGRO_EVENT * evt)
{
   Editor * editor = (Editor *)screen->data;
   if (evt->type == ALLEGRO_EVENT_MOUSE_BUTTON_UP && evt->mouse.button == 1) {
      editor->clickedWidget = NULL;
   }
}
