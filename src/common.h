#ifndef UBOMB_COMMON_H
#define UBOMB_COMMON_H


#include <allegro5/allegro.h>


#define arraySize(array) (sizeof(array) / sizeof(*array))
#define isAmong(type, value, ...) \
   isAmongArray((type[]){ value }, (type[]){ __VA_ARGS__ }, sizeof(type), sizeof((type[]){ __VA_ARGS__ }) / sizeof(type))

#define DISPLAY_W al_get_display_width(glob.display)
#define DISPLAY_H al_get_display_height(glob.display)

#define KEY(key) ALLEGRO_KEY_ ## key
#define KEY_COMB(modifiers, key) ((uint64_t)(modifiers) << 32 | KEY(key))
#define KEY_COMB_1(mod, key) KEY_COMB(ALLEGRO_KEYMOD_ ## mod, key)
#define KEY_COMB_2(mod1, mod2, key) KEY_COMB(ALLEGRO_KEYMOD_ ## mod1 | ALLEGRO_KEYMOD_ ## mod2, key)
#define KEY_COMB_3(mod1, mod2, mod3, key) \
   KEY_COMB(ALLEGRO_KEYMOD_ ## mod1 | ALLEGRO_KEYMOD_ ## mod2 | ALLEGRO_KEYMOD_ ## mod3, key)


typedef struct {
   ALLEGRO_DISPLAY * display;
   int reqDispW, reqDispH;
   ALLEGRO_EVENT_QUEUE * evtQueue;
   bool quit;
} Global;


static inline bool isAmongArray(void const * value, void const * array, size_t valueSize, size_t arrayLength)
{
   for (size_t valueNum = 0; valueNum < arrayLength; ++valueNum) {
      if (memcmp(value, (char *)array + valueNum * valueSize, valueSize) == 0) {
         return true;
      }
   }
   return false;
}

static inline uint64_t getKeyComb(ALLEGRO_EVENT * evt)
{
   static int const allModifiers = ALLEGRO_KEYMOD_SHIFT | ALLEGRO_KEYMOD_CTRL |
         ALLEGRO_KEYMOD_ALT | ALLEGRO_KEYMOD_ALTGR | ALLEGRO_KEYMOD_COMMAND;
   return (uint64_t)(evt->keyboard.modifiers & allModifiers) << 32 | evt->keyboard.keycode;
}

static inline ALLEGRO_COLOR COLOR(uint32_t code)
{
   return al_map_rgb(code >> 16 & 0xFF, code >> 8 & 0xFF, code & 0xFF);
}
static inline ALLEGRO_COLOR COLOR_A(uint32_t code)
{
   return al_premul_rgba(code >> 24 & 0xFF, code >> 16 & 0xFF, code >> 8 & 0xFF, code & 0xFF);
}


extern Global glob;


#endif
