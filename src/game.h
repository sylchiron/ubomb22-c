#ifndef UBOMB_GAME_H
#define UBOMB_GAME_H


#include "screen.h"
#include "world.h"


typedef enum {
   GAME_STATUS_PLAYING,
   GAME_STATUS_PAUSED,
   GAME_STATUS_WON,
   GAME_STATUS_LOST,
} GameStatus;

typedef struct {
   ALLEGRO_BITMAP * bmp;
   ALLEGRO_USTR * text;
} TextShadow;

typedef struct {
   GameStatus status;
   World * world;
   double suspensionTime;
   TextShadow statusTextShadows[4];
   Direction decorationsDirs[20];
} Game;


void initGame(Screen * screen);
void drawGame(Screen * screen);
void processGameEvent(Screen * screen, ALLEGRO_EVENT * evt);
void updateGame(Screen * screen);
void deinitGame(Screen * screen);

int gameGetMinStatusBarWidth(int levelNum);
void gamePrepareEndDecorations(Game * game);


extern int const gameStatusBarHeight;


#endif
