#include "game.h"
#include "common.h"
#include "resources.h"
#include "world_actions.h"


Screen const gameScreen = { &initGame, &drawGame, &processGameEvent, &updateGame, NULL, &deinitGame };


void gameInitLevel(Game * game, int levelNum, double currentTime)
{
   Level * level = &game->world->levels[levelNum];
   game->world->currentLevelNum = levelNum;
   accessLevel(game->world, currentTime);

   int widthForLevel = level->width * globRes.cellW, widthForStatusBar = gameGetMinStatusBarWidth(levelNum);
   resizeDisplay(widthForLevel > widthForStatusBar ? widthForLevel : widthForStatusBar,
      level->height * globRes.cellH + getNbActivePlayers(game->world) * gameStatusBarHeight);
   clearKeyboardState();
}

void initGame(Screen * screen)
{
   Game * game = (Game *)al_malloc(sizeof(*game));
   *game = (Game){ .world = (World *)screen->data };
   screen->data = game;
   gameInitLevel(game, 0, screen->currentTime);
}


void processGameEvent(Screen * screen, ALLEGRO_EVENT * evt)
{
   Game * game = (Game *)screen->data;
   if (evt->type == ALLEGRO_EVENT_KEY_CHAR || (evt->type == ALLEGRO_EVENT_KEY_UP && evt->keyboard.keycode == KEY(PAUSE))) {
      uint64_t keyComb = getKeyComb(evt);
      switch (keyComb) {
         case KEY_COMB_1(CTRL, Q):
         case KEY(ESCAPE):
            quitScreen();
            break;
         case KEY(ENTER):
         case KEY(SPACE):
            if (game->status == GAME_STATUS_WON || game->status == GAME_STATUS_LOST) {
               quitScreen();
            }
            break;
         case KEY(PAUSE):
         case KEY(F3):
         case KEY_COMB_1(CTRL, P):
            if (isAmong(GameStatus, game->status, GAME_STATUS_PLAYING, GAME_STATUS_PAUSED)) {
               game->suspensionTime = screen->currentTime - game->suspensionTime;
               game->status = game->status == GAME_STATUS_PAUSED ? GAME_STATUS_PLAYING : GAME_STATUS_PAUSED;
            }
            break;
      }
   }
}


void processGameInput(Game * game, double currentTime)
{
   static const int keys[][6] = {
         { ALLEGRO_KEY_UP, ALLEGRO_KEY_RIGHT, ALLEGRO_KEY_DOWN, ALLEGRO_KEY_LEFT, ALLEGRO_KEY_SPACE, ALLEGRO_KEY_ENTER },
         { ALLEGRO_KEY_1, ALLEGRO_KEY_3, ALLEGRO_KEY_4, ALLEGRO_KEY_2, ALLEGRO_KEY_LCTRL, ALLEGRO_KEY_LSHIFT },
         { ALLEGRO_KEY_6, ALLEGRO_KEY_8, ALLEGRO_KEY_9, ALLEGRO_KEY_7, ALLEGRO_KEY_RCTRL, ALLEGRO_KEY_RSHIFT },
      };
   ALLEGRO_KEYBOARD_STATE kbState;
   al_get_keyboard_state(&kbState);
   for (int playerNum = 0; playerNum < game->world->nbPlayers; ++playerNum) {
      Player * player = game->world->players[playerNum];
      if (player->animatedEntity != NULL && !player->charProps.frozen && playerNum < arraySize(keys)) {
         if (al_key_down(&kbState, keys[playerNum][4])) {
            tryPlacingBomb(player, game->world, currentTime);
         } else if (al_key_down(&kbState, keys[playerNum][5])) {
            bool keyUsed = playerUseKey(player, game->world);
            if (keyUsed) {
               tryMovingPlayer(player, game->world, player->charProps.dir, currentTime);
            }
         }
         for (Direction dir = 0; dir < DIRECTION_COUNT; ++dir) {
            if (al_key_down(&kbState, keys[playerNum][dir]) &&
                  !al_key_down(&kbState, keys[playerNum][getOppositeDirection(dir)])) {
               tryMovingPlayer(player, game->world, dir, currentTime);
            }
         }
      }
   }
}


void gameSetUpEnd(Game * game, GameStatus status)
{
   resizeDisplay(400, 200);
   game->status = status;
   gamePrepareEndDecorations(game);
}

void updateGame(Screen * screen)
{
   Game * game = (Game *)screen->data;
   if (game->status != GAME_STATUS_PLAYING) {
      return;
   }
   double currentTime = screen->currentTime - game->suspensionTime;
   World * world = game->world;
   int nbAlivePlayers = 0, nbTakenPrincesses = 0, nbFrozenPlayers = 0;
   bool invalidateDirsToPlayers = false;
   for (AnimatedEntity * * animEntPtr = &world->animatedEntities; *animEntPtr != NULL;) {
      AnimatedEntity * animEnt = *animEntPtr;
      bool remove = false;
      Level * level = &world->levels[animEnt->levelNum];
      switch (animEnt->entity->type) {
         case ENTITY_PLAYER: {
            Player * player = &animEnt->entity->player;
            if (currentTime >= player->charProps.move.endInstant) {
               player->charProps.frozen = false;
            } else if (player->charProps.frozen) {
               ++nbFrozenPlayers;
            }
            loseLifeAgainstMonsters(&player->charProps, getLevelCell(level, animEnt->pos), currentTime);
            bool playerMoved = tryFinishingPlayerMove(player, world, currentTime);
            if (playerMoved) {
               invalidateDirsToPlayers = true;
            }
            if (player->charProps.nbLives <= 0) {
               player->animatedEntity = NULL;
               for (int playerNum = 0; playerNum < world->nbPlayers; ++playerNum) {
                  if (world->players[playerNum] == player) {
                     world->players[playerNum] = (Player *)al_malloc(sizeof(*world->players[playerNum]));
                     *world->players[playerNum] = *player;
                     break;
                  }
               }
               remove = true;
            } else {
               ++nbAlivePlayers;
            }
         }  break;
         case ENTITY_MONSTER:
            updateMonster(animEnt, world, currentTime);
            if (animEnt->entity->monster.charProps.nbLives <= 0) {
               remove = true;
            }
            break;
         case ENTITY_BOMB: {
            Bomb * bomb = &animEnt->entity->bomb;
            bool hadExploded = bomb->exploded;
            updateBomb(animEnt, level, currentTime);
            if (!hadExploded && bomb->exploded) {
               invalidateDirsToPlayers = true;
            }
            if (bomb->rangeReached >= bomb->range) {
               if (bomb->player != NULL) {
                  --bomb->player->nbPlacedBombs;
               }
               remove = true;
            }
         }  break;
         default: ;
      }
      if (remove) {
         removeEntity(getLevelCell(level, animEnt->pos), animEnt->entity);
         removeAnimatedEntity(animEntPtr);
      } else {
         animEntPtr = &animEnt->next;
      }
   }
   int commonDestLevelNum = -1;
   for (int playerNum = 0; playerNum < world->nbPlayers; ++playerNum) {
      Player * player = world->players[playerNum];
      nbTakenPrincesses += player->nbTakenPrincesses;
      if (player->animatedEntity != NULL) {
         int destLevelNum = -2;
         if (player->tookDoor != NULL && !player->charProps.frozen) {
            destLevelNum = player->animatedEntity->levelNum + player->tookDoor->levelIncr;
         }
         if (commonDestLevelNum == -1) {
            commonDestLevelNum = destLevelNum;
         } else if (commonDestLevelNum != destLevelNum) {
            commonDestLevelNum = -2;
         }
      }
   }
   if (0 <= commonDestLevelNum && commonDestLevelNum < world->nbLevels) {
      gameInitLevel(game, commonDestLevelNum, currentTime);
      Level * newLevel = &world->levels[world->currentLevelNum];
      for (int playerNum = 0; playerNum < world->nbPlayers; ++playerNum) {
         Player * player = world->players[playerNum];
         AnimatedEntity * animEnt = player->animatedEntity;
         if (animEnt != NULL) {
            Level * oldLevel = &world->levels[animEnt->levelNum];
            double doorXRatio = (double)animEnt->pos.x / oldLevel->width, doorYRatio = (double)animEnt->pos.y / oldLevel->height;
            detachEntity(getLevelCell(oldLevel, animEnt->pos), animEnt->entity);
            animEnt->levelNum = world->currentLevelNum;
            animEnt->pos = isInsideLevel(newLevel, player->tookDoor->targetPos) ? player->tookDoor->targetPos :
               (Position){ doorXRatio * newLevel->width, doorYRatio * newLevel->height };
            addEntity(getLevelCell(newLevel, animEnt->pos), animEnt->entity);
         }
         player->tookDoor = NULL;
      }
      invalidateDirsToPlayers = true;
   }
   if (invalidateDirsToPlayers) {
      al_free(world->directionsToPlayers);
      world->directionsToPlayers = NULL;
   }
   if (nbTakenPrincesses >= world->nbNeededPrincesses && nbFrozenPlayers <= 0) {
      gameSetUpEnd(game, GAME_STATUS_WON);
   } else if (nbAlivePlayers <= 0) {
      gameSetUpEnd(game, GAME_STATUS_LOST);
   }

   processGameInput(game, currentTime);
}


void deinitGame(Screen * screen)
{
   Game * game = (Game *)screen->data;
   for (int tsNum = 0; tsNum < arraySize(game->statusTextShadows); ++tsNum) {
      al_destroy_bitmap(game->statusTextShadows[tsNum].bmp);
      al_ustr_free(game->statusTextShadows[tsNum].text);
   }
   destroyWorld(game->world);
   al_free(game);
}
