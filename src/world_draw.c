#include "world.h"
#include "common.h"
#include "resources.h"

#include <math.h>

#include <allegro5/allegro_primitives.h>


bool charIsTranslucent(Character const * charProps, double currentTime)
{
   double oneBlinkDuration = charProps->invincibilityDuration / 10.;
   return currentTime < charProps->invincibilityEndInstant &&
      fmod(charProps->invincibilityEndInstant - currentTime, oneBlinkDuration) >= oneBlinkDuration / 2.;
}

void computeSmoothMoveCoords(double currentTime, float * drawX, float * drawY, Movement const * move)
{
   if (currentTime < move->endInstant) {
      double remainingMoveProportion = (move->endInstant - currentTime) / move->duration;
      *drawX = (move->to.x + (move->from.x - move->to.x) * remainingMoveProportion) * globRes.cellW;
      *drawY = (move->to.y + (move->from.y - move->to.y) * remainingMoveProportion) * globRes.cellH;
   }
}

void drawEntity(double currentTime, bool forEditor, Entity const * entity, float drawX, float drawY, float opacity)
{
   static TileIndex const entityTileIndexes[ENTITY_COUNT] = {
         [ENTITY_PRINCESS] = TILE_PRINCESS,
         [ENTITY_STONE] = TILE_STONE,
         [ENTITY_TREE] = TILE_TREE,
         [ENTITY_FRAGILE_TREE] = TILE_FRAGILE_TREE,
         [ENTITY_BOX] = TILE_BOX,
         [ENTITY_SOLID_BOX] = TILE_SOLID_BOX,
      }, bonusTileIndexes[BONUS_COUNT] = {
         [BONUS_KEY] = TILE_KEY,
         [BONUS_HEART] = TILE_HEART,
      };
   if (entityIsCharacter(entity->type) && charIsTranslucent(&entity->character, currentTime)) {
      opacity *= .3f;
   }
   switch (entity->type) {
      case ENTITY_NONE:
         break;
      case ENTITY_PLAYER:
         drawPlayer(entity->player.charProps.dir, drawX, drawY, opacity);
         break;
      case ENTITY_MONSTER:
         drawMonster(entity->monster.charProps.dir, drawX, drawY, opacity);
         break;
      case ENTITY_BONUS:
         switch (entity->bonus.type) {
            case BONUS_BOMB_NUMBER:
               drawTile(entity->bonus.amount >= 0 ? TILE_BOMB_NB_INC : TILE_BOMB_NB_DEC, drawX, drawY, opacity);
               break;
            case BONUS_BOMB_RANGE:
               drawTile(entity->bonus.amount >= 0 ? TILE_BOMB_RANGE_INC : TILE_BOMB_RANGE_DEC, drawX, drawY, opacity);
               break;
            case BONUS_SPEED:
               drawTile(entity->bonus.factor >= 1. ? TILE_SPEED_INC : TILE_SPEED_DEC, drawX, drawY, opacity);
               break;
            default:
               drawTile(bonusTileIndexes[entity->bonus.type], drawX, drawY, opacity);
         }
         break;
      case ENTITY_DOOR:
         if (forEditor) {
            drawEditorDoor(entity->door.isOpen, entity->door.levelIncr, drawX, drawY, opacity);
         } else {
            drawDoor(entity->door.isOpen, drawX, drawY, opacity);
         }
         break;
      case ENTITY_BOMB: {
         Bomb const * bomb = &entity->bomb;
         if (!bomb->exploded) {
            double bombExplosionDuration = bomb->explosionInstant - bomb->creationInstant,
               bombProgression = (currentTime - bomb->creationInstant) / bombExplosionDuration;
            drawBomb(bombProgression, drawX, drawY, opacity);
         }
      }  break;
      default:
         drawTile(entityTileIndexes[entity->type], drawX, drawY, opacity);
   }
}

void drawEntityAtPos(double currentTime, bool forEditor, Entity const * entity, Position pos)
{
   float drawX = pos.x * globRes.cellW, drawY = pos.y * globRes.cellH;
   if (entityIsMovable(entity->type)) {
      computeSmoothMoveCoords(currentTime, &drawX, &drawY, &entity->move);
   }
   drawEntity(currentTime, forEditor, entity, drawX, drawY, 1.f);
}


void drawBombExplosions(double currentTime, Bomb const * bomb, Position pos)
{
   double explosionProgression = (currentTime - bomb->explosionInstant) / bomb->explosionDuration;
   for (Direction dir = 0; dir < DIRECTION_COUNT; ++dir) {
      if (!bomb->stopped[dir]) {
         Position endPos = shiftedPos(pos, dir, bomb->range);
         drawExplosion(explosionProgression, (pos.x + explosionProgression * (endPos.x - pos.x)) * globRes.cellW,
            (pos.y + explosionProgression * (endPos.y - pos.y)) * globRes.cellH);
      }
   }
}


void drawLevelContents(double currentTime, bool forEditor, Level const * level)
{
   forAllConstEntities(level, pos, entity) {
      if (!isAmong(EntityType, entity->type, ENTITY_PLAYER, ENTITY_MONSTER, ENTITY_BOX, ENTITY_SOLID_BOX, ENTITY_BOMB)) {
         drawEntityAtPos(currentTime, forEditor, entity, pos);
      }
   }
   forAllConstEntities(level, pos, entity) {
      if (isAmong(EntityType, entity->type, ENTITY_PLAYER, ENTITY_MONSTER, ENTITY_BOX, ENTITY_SOLID_BOX)) {
         drawEntityAtPos(currentTime, forEditor, entity, pos);
      }
   }
   forAllConstEntities(level, pos, entity) {
      if (entity->type == ENTITY_BOMB) {
         drawEntityAtPos(currentTime, forEditor, entity, pos);
      }
   }
   forAllConstEntities(level, pos, entity) {
      if (entity->type == ENTITY_BOMB && entity->bomb.exploded) {
         drawBombExplosions(currentTime, &entity->bomb, pos);
      }
   }
}


void drawLevelSurrounding(Level const * level, int marginX, int marginY)
{
   int widthForLevel = level->width * globRes.cellW, heightForLevel = level->height * globRes.cellH;
   if (level->surrounding == SURROUNDING_DEFAULT) {
      al_draw_filled_rectangle(-marginX, -marginY, widthForLevel + marginX, heightForLevel + marginY, COLOR(0x000000));
      al_draw_filled_rectangle(-globRes.cellW / 4.f, -globRes.cellH / 4.f,
         widthForLevel + globRes.cellW / 4.f, heightForLevel + globRes.cellH / 4.f, COLOR(0x808080));
      al_draw_filled_rectangle(0, 0, widthForLevel, heightForLevel, COLOR(0xFFFFFF));
   } else {
      TileIndex tile = level->surrounding == SURROUNDING_STONE ? TILE_STONE : TILE_TREE;
      for (int y = 0; y < level->height; ++y) {
         for (int x = 0; x * globRes.cellW < marginX; ++x) {
            drawTile(tile, (-x - 1) * globRes.cellW, y * globRes.cellH, 1.f);
            drawTile(tile, (x + level->width) * globRes.cellW, y * globRes.cellH, 1.f);
         }
      }
      for (int x = -(marginX - 1 / globRes.cellW) - 1; x * globRes.cellW < widthForLevel + marginX; ++x) {
         for (int y = 0; y * globRes.cellH < marginY; ++y) {
            drawTile(tile, x * globRes.cellW, (-y - 1) * globRes.cellH, 1.f);
            drawTile(tile, x * globRes.cellW, (y + level->height) * globRes.cellH, 1.f);
         }
      }
   }
}


void drawLevel(double currentTime, bool forEditor, Level const * level, int x1, int y1, int x2, int y2)
{
   int fullWidth = x2 - x1, fullHeight = y2 - y1;
   int widthForLevel = level->width * globRes.cellW, heightForLevel = level->height * globRes.cellH;
   ALLEGRO_TRANSFORM translation;
   int marginX = ceilf((fullWidth - widthForLevel) / 2.f), marginY = ceilf((fullHeight - heightForLevel) / 2.f);
   al_set_clipping_rectangle(x1, y1, fullWidth, fullHeight);
   al_identity_transform(&translation);
   al_translate_transform(&translation, x1 + marginX, y1 + marginY);
   al_use_transform(&translation);

   if (widthForLevel < fullWidth || heightForLevel < fullHeight) {
      drawLevelSurrounding(level, marginX, marginY);
   }
   drawLevelContents(currentTime, forEditor, level);

   al_identity_transform(&translation);
   al_use_transform(&translation);
   al_reset_clipping_rectangle();
}
