#include "common.h"
#include "resources.h"
#include "game.h"

#include <math.h>

#include <allegro5/allegro_primitives.h>


int const gameStatusBarHeight = 55;


int gameGetMinStatusBarWidth(int levelNum)
{
   return (12 + (int)log10(levelNum + 1)) * globRes.cellW;
}

void drawTextShadow(TextShadow * shadow, int textW, int textH, ALLEGRO_FONT * font, ALLEGRO_USTR * text, float x, float y)
{
   if (shadow->text == NULL) {
      shadow->text = al_ustr_dup(text);
   } else if (al_ustr_equal(shadow->text, text) && shadow->bmp != NULL) {
      al_draw_bitmap(shadow->bmp, x - textW, y - textH, 0);
      return;
   } else {
      al_ustr_assign(shadow->text, text);
   }
   al_destroy_bitmap(shadow->bmp);
   ALLEGRO_BITMAP * textBmp = al_create_bitmap(textW * 2, textH * 2);
   al_set_target_bitmap(textBmp);
   al_clear_to_color(COLOR_A(0x00000000));
   al_draw_ustr(globRes.fonts[FONT_VALUE], COLOR_A(0x00000080), textW, textH * .5f, ALLEGRO_ALIGN_CENTRE, text);
   al_lock_bitmap(textBmp, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY);
   shadow->bmp = al_create_bitmap(textW * 2, textH * 2);
   al_set_target_bitmap(shadow->bmp);
   al_clear_to_color(COLOR_A(0x00000000));
   al_lock_bitmap(shadow->bmp, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY);
   static int const radius = 3, divisor = (radius * 2 + 1) * (radius * 2 + 1);
   for (int textY = 0; textY < al_get_bitmap_height(textBmp); ++textY) {
      for (int textX = 0; textX < al_get_bitmap_width(textBmp); ++textX) {
         int r = 0, g = 0, b = 0, a = 0;
         for (int shadowY = textY - radius; shadowY <= textY + radius; ++shadowY) {
            for (int shadowX = textX - radius; shadowX <= textX + radius; ++shadowX) {
               unsigned char r2, g2, b2, a2;
               al_unmap_rgba(al_get_pixel(textBmp, shadowX, shadowY), &r2, &g2, &b2, &a2);
               r += r2; g += g2; b += b2; a += a2;
            }
         }
         r /= divisor; g /= divisor; b /= divisor; a /= divisor;
         al_put_pixel(textX, textY, al_premul_rgba(r, g, b, a));
      }
   }
   al_unlock_bitmap(shadow->bmp);
   al_set_target_backbuffer(glob.display);
   al_draw_bitmap(shadow->bmp, x - textW, y - textH, 0);
   al_destroy_bitmap(textBmp);
}

void drawGameStatusBars(Game * game)
{
   static float const borderThickness = 5.f, horizPadding = 15.f, smallSpacing = 5.f;
   float y = DISPLAY_H;
   for (int playerNum = game->world->nbPlayers - 1; playerNum >= 0; --playerNum) {
      if (game->world->players[playerNum]->animatedEntity == NULL) {
         continue;
      }

      y -= gameStatusBarHeight;
      float x = horizPadding, y2 = y + borderThickness + 5.f;

      al_draw_filled_rectangle(0.f, y, DISPLAY_W, y + borderThickness, COLOR(0x000000));

      int levelNum = game->world->currentLevelNum + 1, invertedLevelNum = 0, nbFigures = 0;
      while (levelNum != 0) {
         ++nbFigures;
         invertedLevelNum = invertedLevelNum * 10 + levelNum % 10;
         levelNum /= 10;
      }
      while (nbFigures > 0) {
         drawBannerDigit(invertedLevelNum % 10, x, y2);
         x += globRes.digitW;
         invertedLevelNum /= 10;
         --nbFigures;
      }

      x += horizPadding;
      al_draw_filled_rectangle(x, y, x + borderThickness, y + gameStatusBarHeight, COLOR(0x000000));
      x += borderThickness + horizPadding;

      Player * player = game->world->players[playerNum];
      int const values[] =
         { player->charProps.nbLives, player->maxNbBombs - player->nbPlacedBombs, player->bombRange, player->nbKeys };
      for (int valueNum = 0; valueNum < arraySize(values); ++valueNum) {
         drawBannerImg(valueNum, x, y2);
         x += globRes.cellW + smallSpacing;
         ALLEGRO_USTR * valueStr = al_ustr_newf("%d", values[valueNum]);
         int valueWidth = al_get_text_width(globRes.fonts[FONT_VALUE], al_cstr(valueStr)),
            valueHeight = al_get_font_line_height(globRes.fonts[FONT_VALUE]);
         drawTextShadow(&game->statusTextShadows[valueNum], valueWidth, valueHeight, globRes.fonts[FONT_VALUE], valueStr,
            x + valueWidth * .5f + 3.f, y2 + globRes.cellH * .5f + 3.f);
         al_draw_ustr(globRes.fonts[FONT_VALUE], COLOR(0x000000), x, y2 + (globRes.cellH - valueHeight) * .5f,
            ALLEGRO_ALIGN_LEFT, valueStr);
         al_ustr_free(valueStr);
         x += valueWidth + horizPadding * 2.f;
      }
   }
}


void gamePrepareEndDecorations(Game * game)
{
   for (int decorI = 0; decorI < arraySize(game->decorationsDirs); ++decorI) {
      game->decorationsDirs[decorI] = getRandomDirection();
   }
}

void drawGameEndDecorations(Game * game)
{
   float x = (DISPLAY_W - globRes.cellW * arraySize(game->decorationsDirs) / 2.f) / 2.f;
   for (int decorI = 0; decorI < arraySize(game->decorationsDirs); decorI += 2) {
      if (game->status == GAME_STATUS_WON) {
         if (decorI % 4 < 2) {
            drawTile(TILE_PRINCESS, x, 0, 1.f);
            drawPlayer(game->decorationsDirs[decorI + 1], x, DISPLAY_H - globRes.cellH, 1.f);
         } else {
            drawPlayer(game->decorationsDirs[decorI], x, 0, 1.f);
            drawTile(TILE_PRINCESS, x, DISPLAY_H - globRes.cellH, 1.f);
         }
      } else {
         drawMonster(game->decorationsDirs[decorI], x, 0, 1.f);
         drawMonster(game->decorationsDirs[decorI + 1], x, DISPLAY_H - globRes.cellH, 1.f);
      }
      x += globRes.cellW;
   }
}


void drawGame(Screen * screen)
{
   Game * game = (Game *)screen->data;
   Level * level = &game->world->levels[game->world->currentLevelNum];
   al_clear_to_color(COLOR(0xFFFFFF));
   switch (game->status) {
      case GAME_STATUS_PLAYING:
         drawLevel(screen->currentTime - game->suspensionTime, false, level,
            0, 0, DISPLAY_W, DISPLAY_H - getNbActivePlayers(game->world) * gameStatusBarHeight);
         drawGameStatusBars(game);
         break;
      case GAME_STATUS_PAUSED:
         drawLevel(game->suspensionTime, false, level,
            0, 0, DISPLAY_W, DISPLAY_H - getNbActivePlayers(game->world) * gameStatusBarHeight);
         drawGameStatusBars(game);
         drawCenteredText(FONT_BIGGEST, 0x008080, .5f, .45f, "Pause");
         break;
      case GAME_STATUS_WON:
         drawCenteredText(FONT_BIGGEST, 0x008080, .5f, .5f, "Won!");
         drawGameEndDecorations(game);
         break;
      case GAME_STATUS_LOST:
         drawCenteredText(FONT_BIGGEST, 0xFF0000, .5f, .5f, "Game over");
         drawGameEndDecorations(game);
         break;
   }
}
