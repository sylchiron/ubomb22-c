#include "common.h"
#include "resources.h"
#include "screen.h"
#include "world_repo.h"

#include <locale.h>

#include <allegro5/allegro_image.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>


Global glob = {};


void init()
{
   al_init();
   al_init_primitives_addon();
   al_init_image_addon();
   al_init_font_addon();
   al_init_ttf_addon();
   al_init_native_dialog_addon();

   al_install_keyboard();
   al_install_mouse();

   srand(time(NULL));

   setlocale(LC_ALL, "C");
}

void initDevices()
{
   al_set_new_window_title("UBomb 2022 C");
   al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
   al_set_new_display_option(ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST);
   al_set_new_display_flags(ALLEGRO_WINDOWED);

   glob.evtQueue = al_create_event_queue();
   al_register_event_source(glob.evtQueue, al_get_keyboard_event_source());
   al_register_event_source(glob.evtQueue, al_get_mouse_event_source());
}

void deinitDevices()
{
   if (glob.evtQueue != NULL) {
      al_destroy_event_queue(glob.evtQueue);
   }
   if (glob.display != NULL) {
      al_destroy_display(glob.display);
   }
}


int main(int argc, char * argv[])
{
   init();
   if (!loadResources()) {
      goto loadingFailed;
   }
   initDevices();

   initWorldRepo();

   changeScreen(&mainMenuScreen, NULL);

   while (!glob.quit) {
      screenIteration();
   }

loadingFailed:
   deinitDevices();
   destroyResources();
}
