#include "world_actions.h"
#include "common.h"


bool tryPlacingBomb(Player * player, World * world, double currentTime)
{
   if (player->nbPlacedBombs >= player->maxNbBombs) {
      return false;
   }
   LevelCell * cell = getLevelCell(&world->levels[player->animatedEntity->levelNum], player->animatedEntity->pos);
   for (Entity * entity = cell->first; entity != NULL; entity = entity->next) {
      if (entity->type == ENTITY_BOMB) {
         return false;
      }
   }
   ++player->nbPlacedBombs;
   Entity * entity = addNewEntity(cell, &(Entity){ .type = ENTITY_BOMB, .bomb = {
         .player = player, .range = player->bombRange, .creationInstant = currentTime,
         .explosionInstant = currentTime + player->bombBurningTime, .explosionDuration = player->bombExplosionTime,
      } });
   addNewAnimatedEntity(world, &(AnimatedEntity){ player->animatedEntity->levelNum, player->animatedEntity->pos, entity });
   return true;
}


bool loseLife(Character * charProps, double currentTime)
{
   if (charProps->frozen || currentTime < charProps->invincibilityEndInstant) {
      return false;
   }
   --charProps->nbLives;
   if (charProps->nbLives > 0) {
      charProps->invincibilityEndInstant = currentTime + charProps->invincibilityDuration;
   } else {
      charProps->frozen = true;
   }
   return true;
}


bool loseLifeAgainstMonsters(Character * charProps, LevelCell * cell, double currentTime)
{
   if (charProps->frozen || currentTime < charProps->invincibilityEndInstant) {
      return false;
   }
   for (Entity * entity = cell->first; entity != NULL; entity = entity->next) {
      if (entity->type == ENTITY_MONSTER) {
         return loseLife(charProps, currentTime);
      }
   }
   return false;
}


bool explode(LevelCell * cell, double currentTime)
{
   bool stop = false;
   Entity * nextEntity = cell->first, * entity;
   while ((entity = nextEntity) != NULL) {
      nextEntity = entity->next;
      switch (entity->type) {
         case ENTITY_PLAYER:
         case ENTITY_MONSTER:
            loseLife(&entity->character, currentTime);
            break;
         case ENTITY_STONE:
         case ENTITY_TREE:
         case ENTITY_SOLID_BOX:
         case ENTITY_DOOR:
            stop = true;
            break;
         case ENTITY_BOX:
         case ENTITY_FRAGILE_TREE:
            stop = true;
            removeEntity(cell, entity);
            break;
         case ENTITY_BONUS:
            if (entity->bonus.type != BONUS_KEY) {
               removeEntity(cell, entity);
            }
            break;
         case ENTITY_BOMB:
            if (entity->bomb.explosionInstant > currentTime) {
               entity->bomb.explosionInstant = currentTime;
            }
            break;
         default: ;
      }
   }
   return stop;
}


static inline Direction * getDirectionToPlayers(World * world, Position pos)
{
   Level * level = &world->levels[world->currentLevelNum];
   Direction (* directionsToPlayers)[level->width] = (Direction (* )[level->width])world->directionsToPlayers;
   return &directionsToPlayers[pos.y][pos.x];
}

void calculateDirectionsToPlayers(World * world)
{
   Level * level = &world->levels[world->currentLevelNum];
   world->directionsToPlayers =
      (Direction *)al_realloc(world->directionsToPlayers, level->width * level->height * sizeof(*world->directionsToPlayers));
   for (int pos = 0; pos < level->height * level->width; ++pos) {
      world->directionsToPlayers[pos] = DIRECTION_NONE;
   }
   Position posQueue[level->width * level->height];
   int queueFront = 0, queueBack = 0;
   for (int playerNum = 0; playerNum < world->nbPlayers; ++playerNum) {
      AnimatedEntity * playerAnimEnt = world->players[playerNum]->animatedEntity;
      if (playerAnimEnt != NULL && playerAnimEnt->levelNum == world->currentLevelNum) {
         posQueue[queueBack] = playerAnimEnt->pos;
         ++queueBack;
         *getDirectionToPlayers(world, playerAnimEnt->pos) = getRandomDirection();
      }
   }
   while (queueFront < queueBack) {
      Position pos = posQueue[queueFront];
      ++queueFront;
      for (Direction dir = 0; dir < DIRECTION_COUNT; ++dir) {
         Position neighbor = shiftedPos(pos, dir, -1);
         if (isInsideLevel(level, neighbor) && *getDirectionToPlayers(world, neighbor) == DIRECTION_NONE) {
            *getDirectionToPlayers(world, neighbor) = dir;
            if (canGoOn(&(Entity){ .type = ENTITY_MONSTER }, level, neighbor)) {
               posQueue[queueBack] = neighbor;
               ++queueBack;
            }
         }
      }
   }
   for (int playerNum = 0; playerNum < world->nbPlayers; ++playerNum) {
      AnimatedEntity * playerAnimEnt = world->players[playerNum]->animatedEntity;
      if (playerAnimEnt != NULL && playerAnimEnt->levelNum == world->currentLevelNum) {
         *getDirectionToPlayers(world, playerAnimEnt->pos) = DIRECTION_NONE;
      }
   }
}


Direction getDirectionForMonster(AnimatedEntity * animEnt, World * world)
{
   if (world->difficultMode && animEnt->levelNum == world->currentLevelNum && rand() < RAND_MAX * .8) {
      if (world->directionsToPlayers == NULL) {
         calculateDirectionsToPlayers(world);
      }
      Direction dir = *getDirectionToPlayers(world, animEnt->pos);
      if (dir != DIRECTION_NONE) {
         return dir;
      }
   }
   return getRandomDirection();
}

void updateMonster(AnimatedEntity * animEnt, World * world, double currentTime)
{
   Monster * monster = &animEnt->entity->monster;
   if (currentTime >= monster->nextMoveTime) {
      tryMovingCharacter(animEnt, world, getDirectionForMonster(animEnt, world), currentTime);
      monster->nextMoveTime += 1. / monster->velocity;
   } else {
      tryFinishingCharacterMove(animEnt, world, currentTime);
   }
}


void updateBomb(AnimatedEntity * animEnt, Level * level, double currentTime)
{
   Bomb * bomb = &animEnt->entity->bomb;
   if (!bomb->exploded && currentTime >= bomb->explosionInstant) {
      bomb->exploded = true;
      bomb->rangeReached = 0;
   }
   if (bomb->exploded) {
      double progression = (currentTime - bomb->explosionInstant) / bomb->explosionDuration;
      int rangeToReach = progression < 1. ? progression * bomb->range : bomb->range;
      for (Direction dir = 0; dir < DIRECTION_COUNT; ++dir) {
         int rangeReached = bomb->rangeReached;
         while (!bomb->stopped[dir] && rangeReached <= rangeToReach) {
            Position reachedPos = shiftedPos(animEnt->pos, dir, rangeReached);
            bomb->stopped[dir] = !isInsideLevel(level, reachedPos) || explode(getLevelCell(level, reachedPos), currentTime);
            ++rangeReached;
         }
      }
      bomb->rangeReached = rangeToReach;
   }
}
