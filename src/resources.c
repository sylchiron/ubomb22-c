#include "resources.h"
#include "common.h"

#include <stdio.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_native_dialog.h>


#define NB_BOMB_STATES 4


GlobalResources globRes = { .cellW = 40, .cellH = 40, .digitW = 32 };

static bool resourceLoadingFailed = false;


ALLEGRO_PATH * getDataPath()
{
   ALLEGRO_PATH * path = al_get_standard_path(ALLEGRO_EXENAME_PATH);
   al_append_path_component(path, "data");
   al_set_path_filename(path, NULL);
   if (!al_filename_exists(al_path_cstr(path, ALLEGRO_NATIVE_PATH_SEP))) {
      al_drop_path_tail(path);
   }
   return path;
}


void loadingFailed(char const * path)
{
   fprintf(stderr, "Loading of %s failed!\n", path);
   if (!resourceLoadingFailed) {
      al_show_native_message_box(glob.display, "Error: Resource loading failed", path,
         "Loading of the resource failed. Please reinstall the program.", NULL, ALLEGRO_MESSAGEBOX_ERROR);
      resourceLoadingFailed = true;
   }
}


bool loadResources()
{
   ALLEGRO_PATH * path = getDataPath();

   al_append_path_component(path, "fonts");
   char const * fontFilenames[] = { "Verdanab.ttf", "Verdanab.ttf", "Verdana.ttf", "Verdanab.ttf", "Verdanab.ttf" };
   int const fontSizes[] = { 60, 48, 16, 28, 16 };
   for (FontIndex fontNum = 0; fontNum < FONT_COUNT; ++fontNum) {
      al_set_path_filename(path, fontFilenames[fontNum]);
      globRes.fonts[fontNum] = al_load_ttf_font(al_path_cstr(path, ALLEGRO_NATIVE_PATH_SEP), fontSizes[fontNum], 0);
      if (!globRes.fonts[fontNum]) {
         loadingFailed(al_path_cstr(path, ALLEGRO_NATIVE_PATH_SEP));
      }
   }
   al_drop_path_tail(path);

   al_append_path_component(path, "images");
   char const * imgFilenames[] = { "characters.png", "decors.png", "bombs.png", "banner_imgs.png", "icon.png" };
   for (ImageIndex imageNum = 0; imageNum < IMAGE_COUNT; ++imageNum) {
      al_set_path_filename(path, imgFilenames[imageNum]);
      globRes.images[imageNum] = al_load_bitmap(al_path_cstr(path, ALLEGRO_NATIVE_PATH_SEP));
      if (!globRes.images[imageNum]) {
         loadingFailed(al_path_cstr(path, ALLEGRO_NATIVE_PATH_SEP));
      }
   }
   al_drop_path_tail(path);

   al_destroy_path(path);

   return !resourceLoadingFailed;
}


void destroyResources()
{
   for (FontIndex fontNum = 0; fontNum < FONT_COUNT; ++fontNum) {
      al_destroy_font(globRes.fonts[fontNum]);
   }
   for (ImageIndex imageNum = 0; imageNum < IMAGE_COUNT; ++imageNum) {
      al_destroy_bitmap(globRes.images[imageNum]);
   }
}


void drawTileAt(ImageIndex imgNum, int rowNum, int colNum, float x, float y, float opacity)
{
   if (opacity >= 1.f) {
      al_draw_bitmap_region(globRes.images[imgNum],
         colNum * globRes.cellW, rowNum * globRes.cellH, globRes.cellW, globRes.cellH, x, y, 0);
   } else {
      al_draw_tinted_bitmap_region(globRes.images[imgNum], al_map_rgba_f(opacity, opacity, opacity, opacity),
         colNum * globRes.cellW, rowNum * globRes.cellH, globRes.cellW, globRes.cellH, x, y, 0);
   }
}

void drawTile(TileIndex tile, float x, float y, float opacity)
{
   typedef struct {
      ImageIndex imgNum;
      int rowNum, colNum;
   } TileInfo;
   static TileInfo const tilesInf[] = {
      { IMAGE_DECORS, 1, 0 },
      { IMAGE_DECORS, 1, 1 },
      { IMAGE_DECORS, 1, 2 },
      { IMAGE_DECORS, 1, 3 },
      { IMAGE_DECORS, 1, 4 },
      { IMAGE_DECORS, 0, 0 },
      { IMAGE_DECORS, 0, 1 },
      { IMAGE_DECORS, 0, 2 },
      { IMAGE_DECORS, 0, 3 },
      { IMAGE_DECORS, 0, 4 },
      { IMAGE_DECORS, 0, 5 },
      { IMAGE_DECORS, 0, 6 },
      { IMAGE_DECORS, 0, 7 },
      { IMAGE_DECORS, 1, 7 },
   };
   TileInfo tileInf = tilesInf[tile];
   drawTileAt(tileInf.imgNum, tileInf.rowNum, tileInf.colNum, x, y, opacity);
}

void drawPlayer(Direction dir, float x, float y, float opacity)
{
   drawTileAt(IMAGE_CHARACTERS, 0, dir, x, y, opacity);
}

void drawMonster(Direction dir, float x, float y, float opacity)
{
   drawTileAt(IMAGE_CHARACTERS, 1, dir, x, y, opacity);
}

void drawDoor(bool isOpen, float x, float y, float opacity)
{
   drawTileAt(IMAGE_DECORS, 2, isOpen ? 1 : 0, x, y, opacity);
}

void drawEditorDoor(bool isOpen, int levelIncr, float x, float y, float opacity)
{
   if (levelIncr == 0) {
      drawDoor(isOpen, x, y, opacity);
   } else {
      drawTileAt(IMAGE_DECORS, 2, 2 + (isOpen ? 2 : 0) + (levelIncr < 0 ? 1 : 0), x, y, opacity);
   }
}

void drawBomb(double progression, float x, float y, float opacity)
{
   int bombState = progression < 0. ? 0 : (progression < 1. ? progression * NB_BOMB_STATES : NB_BOMB_STATES - 1);
   drawTileAt(IMAGE_BOMBS, 0, bombState, x, y, opacity);
}

void drawExplosion(double progression, float x, float y)
{
   drawTileAt(IMAGE_BOMBS, 1, 0, x, y, 1.f);
}

void drawBannerDigit(int digit, float x, float y)
{
   al_draw_bitmap_region(globRes.images[IMAGE_BANNER_IMGS],
         digit * globRes.cellW + (globRes.cellW - globRes.digitW) / 2.f, 0.f, globRes.digitW, globRes.cellH, x, y, 0);
}

void drawBannerImg(int imgNum, float x, float y)
{
   drawTileAt(IMAGE_BANNER_IMGS, 1, imgNum, x, y, 1.f);
}
