#include "world_actions.h"
#include "common.h"


bool takeBonusInt(LevelCell * cell, Entity * entity, int * field, int minValue)
{
   if (*field <= minValue && entity->bonus.amount < 0) {
      return false;
   }
   *field += entity->bonus.amount;
   if (*field < minValue) {
      *field = minValue;
   }
   removeEntity(cell, entity);
   return true;
}
bool takeBonusFloat(LevelCell * cell, Entity * entity, double * field, double maxValue)
{
   double minValue = .025;
   if ((*field == maxValue && entity->bonus.factor < 1.) || (*field == minValue && entity->bonus.factor > 1.)) {
      return false;
   }
   *field /= entity->bonus.factor;
   if (*field > maxValue) {
      *field = maxValue;
   } else if (*field < minValue) {
      *field = minValue;
   }
   removeEntity(cell, entity);
   return true;
}

bool takeBonus(Player * player, LevelCell * cell, Entity * entity)
{
   typedef struct {
      int * field, minValue;
   } DataForIntBonus;
   DataForIntBonus dataForIntBonus[BONUS_COUNT] = {
      [BONUS_KEY] = { &player->nbKeys, 0 },
      [BONUS_HEART] = { &player->charProps.nbLives, 0 },
      [BONUS_BOMB_NUMBER] = { &player->maxNbBombs, 1 },
      [BONUS_BOMB_RANGE] = { &player->bombRange, 1 },
   };
   DataForIntBonus * dataInt = &dataForIntBonus[entity->bonus.type];
   if (dataInt->field != NULL) {
      return takeBonusInt(cell, entity, dataInt->field, dataInt->minValue);
   }
   typedef struct {
      double * field, maxValue;
   } DataForFloatBonus;
   DataForFloatBonus dataForFloatBonus[BONUS_COUNT] = {
      [BONUS_SPEED] = { &player->charProps.move.duration, 2. },
   };
   DataForFloatBonus * dataFloat = &dataForFloatBonus[entity->bonus.type];
   if (dataFloat->field != NULL) {
      return takeBonusFloat(cell, entity, dataFloat->field, dataFloat->maxValue);
   }
   return false;
}


void takeObjects(Player * player, World * world)
{
   Level * level = &world->levels[player->animatedEntity->levelNum];
   LevelCell * cell = getLevelCell(level, player->animatedEntity->pos);
   Entity * nextEntity = cell->first, * entity;
   while ((entity = nextEntity) != NULL) {
      nextEntity = entity->next;
      switch (entity->type) {
         case ENTITY_BONUS:
            takeBonus(player, cell, entity);
            break;
         case ENTITY_DOOR:
            if (entity->door.isOpen) {
               player->tookDoor = &entity->door;
               player->charProps.frozen = true;
            }
            break;
         case ENTITY_PRINCESS:
            if (!entity->princess.taken) {
               player->nbTakenPrincesses += entity->princess.quantity;
               entity->princess.taken = true;
            }
            player->charProps.frozen = true;
            break;
         default: ;
      }
   }
}


bool playerUseKey(Player * player, World * world)
{
   if (player->nbKeys <= 0) {
      return false;
   }
   Level * level = &world->levels[player->animatedEntity->levelNum];
   Position targetPos = nextPos(player->animatedEntity->pos, player->charProps.dir);
   if (isInsideLevel(level, targetPos)) {
      for (Entity * entity = getLevelCell(level, targetPos)->first; entity != NULL; entity = entity->next) {
         if (entity->type == ENTITY_DOOR && !entity->door.isOpen) {
            --player->nbKeys;
            entity->door.isOpen = true;
            return true;
         }
      }
   }
   return false;
}
