#include "editor.h"
#include "common.h"
#include "resources.h"
#include "world_repo.h"

#include <stdio.h>

#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>


Screen const editorScreen = { &initEditor, &drawEditor, &processEditorEvent, NULL, &reinitEditor, &deinitEditor };


static inline void editorIncrHistoryIndex(int * index)
{
   *index = (*index + 1) % EDITOR_HISTORY_MAX_SIZE;
}
static inline void editorDecrHistoryIndex(int * index)
{
   *index = (*index + EDITOR_HISTORY_MAX_SIZE - 1) % EDITOR_HISTORY_MAX_SIZE;
}

void editorDestroyHistory(EditorHistory * history)
{
   if (history->begin >= 0) {
      for (int index = history->begin; index != history->end; editorIncrHistoryIndex(&index)) {
         destroyWorld(history->states[index].world);
      }
   }
   history->begin = history->curIndex = history->savedIndex = -1;
   history->end = 0;
}

void editorSaveWorldInHistory(Editor * editor)
{
   EditorHistory * history = &editor->history;
   editorIncrHistoryIndex(&history->curIndex);
   if (history->curIndex != history->end) {
      for (int index = history->curIndex; index != history->end; editorIncrHistoryIndex(&index)) {
         destroyWorld(history->states[index].world);
      }
   } else if (history->curIndex == history->begin) {
      destroyWorld(history->states[history->curIndex].world);
      editorIncrHistoryIndex(&history->begin);
      if (history->curIndex == history->savedIndex) {
         history->savedIndex = -1;
      }
   } else if (history->begin < 0) {
      history->begin = history->curIndex;
   }
   history->states[history->curIndex].world = cloneWorld(editor->world);
   history->states[history->curIndex].levelNum = editor->world->currentLevelNum;
   history->end = history->curIndex;
   editorIncrHistoryIndex(&history->end);
}


void editorInitLevel(Editor * editor, int levelNum)
{
   World * world = editor->world;
   Level * level = &world->levels[levelNum];
   world->currentLevelNum = levelNum;

   Bounds bounds = editor->levelCanvas.bounds;
   int widthForLevel = level->width * globRes.cellW, heightForLevel = level->height * globRes.cellH;
   int dispW = bounds.x1 + widthForLevel + DISPLAY_W - bounds.x2, dispH = bounds.y1 + heightForLevel + DISPLAY_H - bounds.y2;
   dispW = dispW < 720 ? 720 : dispW;
   dispH = dispH < 560 ? 560 : dispH;
   if (editor->buttonList == NULL || dispW != DISPLAY_W || dispH != DISPLAY_H) {
      float scaleY = (float)DISPLAY_H / dispH;
      resizeDisplay(dispW, dispH);
      editorComputeWidgetsBounds(editor);
      if (editor->buttonList == NULL) {
         editorInitButtons(editor);
      } else {
         recomputeButtonsBounds(editor->buttonList, 1.f, scaleY);
         editorUpdateButtonTexts(editor, NULL);
      }
   } else {
      editorUpdateButtonTexts(editor, NULL);
   }
   bounds = editor->levelCanvas.bounds;
   widthForLevel = widthForLevel > getBoundsW(bounds) ? getBoundsW(bounds) : widthForLevel;
   heightForLevel = heightForLevel > getBoundsH(bounds) ? getBoundsH(bounds) : heightForLevel;
   editor->levelCanvas.strictBounds = createBoundsFromSize(
      bounds.x1 + (getBoundsW(bounds) - widthForLevel) / 2, bounds.y1 + (getBoundsH(bounds) - heightForLevel) / 2,
      widthForLevel, heightForLevel);
}

void editorNewWorld(Editor * editor)
{
   World * world = createWorld();
   world->nbNeededPrincesses = 1;
   addLevel(world, 0, 20, 15);
   world->levels[0].canonicalMonster = defaultMonster;
   world->difficultMode = editor->world->difficultMode;
   world->fileFormat = editor->world->fileFormat;
   destroyWorld(editor->world);
   editor->world = world;
   editorInitLevel(editor, 0);
   editorDestroyHistory(&editor->history);
   editorSaveWorldInHistory(editor);
   editor->history.savedIndex = 0;
}

void initEditor(Screen * screen)
{
   Editor * editor = (Editor *)al_malloc(sizeof(*editor));
   *editor = (Editor){
      .world = (World *)screen->data,
      .editMode = EDITOR_ENTITIES_PUT, .history = { .curIndex = -1, .begin = -1, .savedIndex = -1 },
      .toolbars = { .nbBars = 6, .barHeight = 24 }, .entListPanel = { .heightForType = 50 },
      .propPanel = { .spacing = 10, .labelSpacing = 2 },
   };
   screen->data = editor;

   editorComputeWidgetsBounds(editor);
   if (editor->world->nbLevels <= 0) {
      editorNewWorld(editor);
   } else {
      editorInitLevel(editor, 0);
      editorSaveWorldInHistory(editor);
      editor->history.savedIndex = 0;
   }
}


Position editorGetPointedTilePos(Editor const * editor, int mouseX, int mouseY)
{
   Bounds bounds = editor->levelCanvas.strictBounds;
   int xAdjust = mouseX < bounds.x1 ? -1 : 0, yAdjust = mouseY < bounds.y1 ? -1 : 0;
   return (Position){ (mouseX - bounds.x1) / globRes.cellW + xAdjust, (mouseY - bounds.y1) / globRes.cellH + yAdjust };
}

void editorGetCropBounds(Level const * level, Position tilePos, Bounds * bounds)
{
   *bounds = (Bounds){ tilePos.x, tilePos.y, level->width, level->height };
   if (level->width - (tilePos.x + 1) <= bounds->x1) {
      bounds->x1 = 0;
      bounds->x2 = tilePos.x + 1;
   }
   if (level->height - (tilePos.y + 1) <= bounds->y1) {
      bounds->y1 = 0;
      bounds->y2 = tilePos.y + 1;
   }
}


void drawEditor(Screen * screen)
{
   Editor * editor = (Editor *)screen->data;
   World * world = editor->world;
   Level * level = &world->levels[world->currentLevelNum];

   al_clear_to_color(COLOR(0xFFFFFF));
   drawEditorUI(editor, screen->currentTime);

   Bounds bounds = editor->levelCanvas.bounds;
   drawLevel(0., true, level, bounds.x1, bounds.y1, bounds.x2, bounds.y2);

   ALLEGRO_MOUSE_STATE mouseState;
   al_get_mouse_state(&mouseState);
   Bounds levelBounds = editor->levelCanvas.strictBounds;
   Position tilePos = editorGetPointedTilePos(editor, mouseState.x, mouseState.y);
   al_set_clipping_rectangle(levelBounds.x1, levelBounds.y1, getBoundsW(levelBounds), getBoundsH(levelBounds));
   if (editor->propPanel.entity != NULL) {
      Entity * entity = editor->propPanel.entity;
      if (editor->propPanel.entityLevelNum == world->currentLevelNum) {
         float drawX = levelBounds.x1 + editor->propPanel.entityPos.x * globRes.cellW,
            drawY = levelBounds.y1 + editor->propPanel.entityPos.y * globRes.cellH;
         al_draw_rectangle(drawX + .5f, drawY + .5f, drawX + globRes.cellW - .5f, drawY + globRes.cellH - .5f,
            COLOR(0x000000), 1.f);
      }
      if (entity->type == ENTITY_DOOR && editor->propPanel.entityLevelNum + entity->door.levelIncr == world->currentLevelNum) {
         float drawX = levelBounds.x1 + entity->door.targetPos.x * globRes.cellW,
            drawY = levelBounds.y1 + entity->door.targetPos.y * globRes.cellH;
         al_draw_ellipse(drawX + globRes.cellW * .5f, drawY + globRes.cellH * .5f, globRes.cellW * .5f, globRes.cellH * .5f,
            COLOR(0x0000FF), (globRes.cellW < globRes.cellH ? globRes.cellW : globRes.cellH) * .1f);
      }
   }
   switch (editor->editMode) {
      case EDITOR_LEVEL_CROP:
         if (isInBounds(bounds, mouseState.x, mouseState.y)) {
            Bounds cropBounds;
            editorGetCropBounds(level, tilePos, &cropBounds);
            float drawX1 = levelBounds.x1 + cropBounds.x1 * globRes.cellW, drawY1 = levelBounds.y1 + cropBounds.y1 * globRes.cellH,
               drawX2 = levelBounds.x1 + cropBounds.x2 * globRes.cellW, drawY2 = levelBounds.y1 + cropBounds.y2 * globRes.cellH;
            ALLEGRO_COLOR cropColor = COLOR_A(0XFF000080);
            al_set_clipping_rectangle(bounds.x1, bounds.y1, getBoundsW(bounds), getBoundsH(bounds));
            al_draw_filled_rectangle(bounds.x1, bounds.y1, bounds.x2, drawY1, cropColor);
            al_draw_filled_rectangle(bounds.x1, drawY2, bounds.x2, bounds.y2, cropColor);
            al_draw_filled_rectangle(bounds.x1, drawY1, drawX1, drawY2, cropColor);
            al_draw_filled_rectangle(drawX2, drawY1, bounds.x2, drawY2, cropColor);
         }
         break;
      case EDITOR_ENTITIES_PUT:
         if (isInBounds(levelBounds, mouseState.x, mouseState.y)) {
            float drawX = levelBounds.x1 + tilePos.x * globRes.cellW, drawY = levelBounds.y1 + tilePos.y * globRes.cellH;
            al_draw_filled_rectangle(drawX, drawY, drawX + globRes.cellW, drawY + globRes.cellH, COLOR_A(0xFFFFFF60));
            drawEntity(0., true, &defaultEntities[editor->entListPanel.selEntityNum], drawX, drawY, .6f);
         }
         break;
      case EDITOR_ENTITIES_PROPS:
      case EDITOR_ENTITY_DOOR_TARGET_POS_LABEL:
         if (isInBounds(levelBounds, mouseState.x, mouseState.y)) {
            float drawX = levelBounds.x1 + tilePos.x * globRes.cellW, drawY = levelBounds.y1 + tilePos.y * globRes.cellH;
            al_draw_rectangle(drawX + .5f, drawY + .5f, drawX + globRes.cellW - .5f, drawY + globRes.cellH - .5f,
               COLOR_A(0x00000080), 1.f);
         }
         break;
      case EDITOR_ENTITIES_MOVE:
      case EDITOR_ENTITIES_REMOVE:
         if (isInBounds(levelBounds, mouseState.x, mouseState.y)) {
            float drawX = levelBounds.x1 + tilePos.x * globRes.cellW, drawY = levelBounds.y1 + tilePos.y * globRes.cellH;
            if (editor->editMode == EDITOR_ENTITIES_REMOVE || editor->movedEntity == NULL) {
               al_draw_filled_rectangle(drawX, drawY, drawX + globRes.cellW, drawY + globRes.cellH, COLOR_A(0xFFFFFF60));
            } else {
               drawEntity(0., true, editor->movedEntity, drawX, drawY, .6f);
            }
         }
         break;
   }
   al_reset_clipping_rectangle();
}


void editorModifyIntValue(Editor * editor, ALLEGRO_USTR * ustr, int * field, int minValue)
{
   if (ustr != NULL) {
      int originalValue = *field;
      sscanf(al_cstr(ustr), "%d", field);
      *field = *field < minValue ? minValue : *field;
      if (originalValue != *field) {
         editorSaveWorldInHistory(editor);
      }
   }
   editorUpdateButtonTexts(editor, NULL);
}
void editorModifyFloatValue(Editor * editor, ALLEGRO_USTR * ustr, double * field, double minValue)
{
   if (ustr != NULL) {
      double originalValue = *field;
      sscanf(al_cstr(ustr), "%lg", field);
      *field = *field < minValue ? minValue : *field;
      if (originalValue != *field) {
         editorSaveWorldInHistory(editor);
      }
   }
   editorUpdateButtonTexts(editor, NULL);
}

bool editorNotSavedWarning(Editor * editor)
{
   if (editor->history.savedIndex != editor->history.curIndex) {
      int firstIndex = editor->history.savedIndex < 0 ? editor->history.begin : editor->history.savedIndex,
         indexDiff = (editor->history.curIndex + EDITOR_HISTORY_MAX_SIZE - firstIndex) % EDITOR_HISTORY_MAX_SIZE;
      ALLEGRO_USTR * msg = al_ustr_newf(
         editor->history.savedIndex < 0 ? "%s More than %d changes %s" : "%s %d changes %s",
         "It seems that you have changes in your world which are not saved.", indexDiff,
         "were done since last save. Do you want to save your world?");
      int response = al_show_native_message_box(glob.display, "Saving changes", "You have non-saved changes", al_cstr(msg),
         "Yes|No|Cancel", ALLEGRO_MESSAGEBOX_WARN | ALLEGRO_MESSAGEBOX_YES_NO);
      al_ustr_free(msg);
      if (response == 1) {
         editorAction(&(Screen){ .data = editor }, EDITOR_SAVE);
         if (editor->history.savedIndex != editor->history.curIndex) {
            return false;
         }
      } else if (response != 2) {
         return false;
      }
   }
   return true;
}

void editorAction(Screen * screen, int actionCode)
{
   Editor * editor = (Editor *)screen->data;
   EditorHistory * history = &editor->history;
   World * world = editor->world;
   Level * level = &world->levels[world->currentLevelNum];
   Entity * entity = editor->propPanel.entity;
   Character * charProps = entity != NULL && entityIsCharacter(entity->type) ? &entity->character : NULL;
   Button * button = &editor->buttonList->buttons[actionCode];
   switch (actionCode) {
      case EDITOR_UNDO:
         if (editor->currentlyEditingWithMouse) {
            editorSaveWorldInHistory(editor);
            editor->currentlyEditingWithMouse = false;
         }
         if (history->curIndex != history->begin) {
            int oldIndex = history->curIndex;
            editorDecrHistoryIndex(&history->curIndex);
            destroyWorld(world);
            editor->world = cloneWorld(history->states[history->curIndex].world);
            al_free(editor->movedEntity);
            editor->movedEntity = NULL;
            editor->propPanel.entity = NULL;
            editorInitLevel(editor, history->states[oldIndex].levelNum);
         } else {
            ALLEGRO_USTR * msg = al_ustr_newf("There is no action to undo. History maximum size is %d.", EDITOR_HISTORY_MAX_SIZE);
            al_show_native_message_box(glob.display, "Undo", "No action to undo", al_cstr(msg), NULL, ALLEGRO_MESSAGEBOX_WARN);
            al_ustr_free(msg);
         }
         break;
      case EDITOR_REDO:
         editorIncrHistoryIndex(&history->curIndex);
         if (history->curIndex == history->end) {
            editorDecrHistoryIndex(&history->curIndex);
            al_show_native_message_box(glob.display, "Redo", "No action to redo", "There is no action to redo.", NULL,
               ALLEGRO_MESSAGEBOX_WARN);
         } else {
            destroyWorld(world);
            editor->world = cloneWorld(history->states[history->curIndex].world);
            editor->propPanel.entity = NULL;
            editorInitLevel(editor, history->states[history->curIndex].levelNum);
         }
         break;
      case EDITOR_EXIT:
         if (editorNotSavedWarning(editor)) {
            editor->editMode = EDITOR_EXIT;
         }
         break;
      case EDITOR_NEW:
         if (editorNotSavedWarning(editor)) {
            editorNewWorld(editor);
         }
         break;
      case EDITOR_OPEN: {
         World * loadedWorld = getWorldFromDialog();
         if (loadedWorld != NULL) {
            if (editorNotSavedWarning(editor)) {
               loadedWorld->difficultMode = world->difficultMode;
               destroyWorld(world);
               editor->world = loadedWorld;
               editorInitLevel(editor, 0);
               editorDestroyHistory(&editor->history);
               editorSaveWorldInHistory(editor);
               editor->history.savedIndex = 0;
            } else {
               destroyWorld(loadedWorld);
            }
         }
      }  break;
      case EDITOR_SAVE:
         if (world->filePath == NULL) {
            editorAction(screen, EDITOR_SAVE_AS);
         } else {
            bool success = saveWorld(world);
            if (success) {
               editor->history.savedIndex = editor->history.curIndex;
            } else {
               al_show_native_message_box(glob.display, "Error: World saving failed", world->filePath,
                  "Saving the world failed. Sorry. Try another location.", NULL, ALLEGRO_MESSAGEBOX_ERROR);
            }
         }
         break;
      case EDITOR_SAVE_AS: {
         char * newPath = getWorldPathFromDialog();
         if (newPath != NULL) {
            al_free(world->filePath);
            world->filePath = newPath;
            editorAction(screen, EDITOR_SAVE);
         }
      }  break;
      case EDITOR_PLAY: {
         World * worldForGame = cloneWorld(world);
         if (worldForGame->fileFormat == WORLD_FF_LEGACY || worldForGame->fileFormat == WORLD_FF_LEGACY_RLE) {
            connectLegacyDoors(worldForGame);
         }
         worldForGame->currentLevelNum = 0;
         changeScreen(&gameScreen, worldForGame);
      }  break;
      case EDITOR_LEVEL_FIRST:
         editorInitLevel(editor, 0);
         break;
      case EDITOR_LEVEL_PREV:
         if (world->currentLevelNum > 0) {
            editorInitLevel(editor, world->currentLevelNum - 1);
         }
         break;
      case EDITOR_LEVEL_CHOOSE: {
         char const * input = button->ustr == NULL ? "" : al_cstr(button->ustr);
         int levelNum, nbRead = sscanf(input, "%d", &levelNum);
         if (nbRead < 1) {
            editorUpdateButtonTexts(editor, NULL);
         } else {
            levelNum = levelNum < 1 ? 1 : (levelNum > world->nbLevels ? world->nbLevels : levelNum);
            editorInitLevel(editor, levelNum - 1);
         }
      }  break;
      case EDITOR_LEVEL_NEXT:
         if (world->currentLevelNum + 1 < world->nbLevels) {
            editorInitLevel(editor, world->currentLevelNum + 1);
         }
         break;
      case EDITOR_LEVEL_LAST:
         editorInitLevel(editor, world->nbLevels - 1);
         break;
      case EDITOR_LEVEL_ADD:
         addLevel(world, world->currentLevelNum + 1, level->width, level->height);
         editor->propPanel.entity = NULL;
         editorInitLevel(editor, world->currentLevelNum + 1);
         level = &world->levels[world->currentLevelNum];
         level->canonicalMonster = world->levels[0].canonicalMonster;
         level->canonicalMonster.charProps.nbLives += world->currentLevelNum / 2;
         level->canonicalMonster.velocity *= (double)(world->nbLevels + world->currentLevelNum) / world->nbLevels;
         editorSaveWorldInHistory(editor);
         break;
      case EDITOR_LEVEL_CLONE:
         cloneLevel(world, world->currentLevelNum, world->currentLevelNum + 1);
         editor->propPanel.entity = NULL;
         editorInitLevel(editor, world->currentLevelNum + 1);
         editorSaveWorldInHistory(editor);
         break;
      case EDITOR_LEVEL_REMOVE:
         if (world->nbLevels > 1) {
            removeLevel(world, world->currentLevelNum);
            editor->propPanel.entity = NULL;
            editorInitLevel(editor, world->currentLevelNum >= world->nbLevels ? world->nbLevels - 1 : world->currentLevelNum);
            editorSaveWorldInHistory(editor);
         }
         break;
      case EDITOR_LEVEL_MOVE_BEFORE:
         if (world->currentLevelNum > 0) {
            Level tmpLevel = world->levels[world->currentLevelNum];
            world->levels[world->currentLevelNum] = world->levels[world->currentLevelNum - 1];
            world->levels[world->currentLevelNum - 1] = tmpLevel;
            editor->propPanel.entity = NULL;
            editorInitLevel(editor, world->currentLevelNum - 1);
            editorSaveWorldInHistory(editor);
         }
         break;
      case EDITOR_LEVEL_MOVE_AFTER:
         if (world->currentLevelNum + 1 < world->nbLevels) {
            Level tmpLevel = world->levels[world->currentLevelNum];
            world->levels[world->currentLevelNum] = world->levels[world->currentLevelNum + 1];
            world->levels[world->currentLevelNum + 1] = tmpLevel;
            editor->propPanel.entity = NULL;
            editorInitLevel(editor, world->currentLevelNum + 1);
            editorSaveWorldInHistory(editor);
         }
         break;
      case EDITOR_ENTITY_DOOR_TARGET_POS_LABEL: {
         int targetLevelNum = editor->propPanel.entityLevelNum + entity->door.levelIncr;
         if (0 <= targetLevelNum && targetLevelNum < world->nbLevels) {
            editorInitLevel(editor, targetLevelNum);
         }
      }
      case EDITOR_WORLD_PROPERTIES:
      case EDITOR_LEVEL_PROPERTIES:
      case EDITOR_LEVEL_CROP:
      case EDITOR_PLAY_AT_POS:
      case EDITOR_ENTITIES_PUT:
      case EDITOR_ENTITIES_MOVE:
      case EDITOR_ENTITIES_PROPS:
      case EDITOR_ENTITIES_REMOVE:
         editor->buttonList->buttons[editor->editMode].highlighted = false;
         editor->editMode = actionCode;
         button->highlighted = true;
         if (!isAmong(int, actionCode, EDITOR_ENTITIES_PUT, EDITOR_ENTITIES_PROPS, EDITOR_ENTITY_DOOR_TARGET_POS_LABEL)) {
            editor->propPanel.entity = NULL;
         }
         break;
      case EDITOR_LEVEL_INC_SIZE:
         resizeLevel(world, level, level->width + 4, level->height + 4, 2, 2);
         editor->propPanel.entityPos.x += 2;
         editor->propPanel.entityPos.y += 2;
         editorInitLevel(editor, world->currentLevelNum);
         editorSaveWorldInHistory(editor);
         break;
      case EDITOR_LEVEL_SIZE_CHOOSE: {
         char const * input = button->ustr == NULL ? "" : al_cstr(button->ustr);
         int nbCrtsToSkip = 0;
         sscanf(input, "%*[^0-9]%n", &nbCrtsToSkip);
         int width, height, nbRead = sscanf(input + nbCrtsToSkip, "%d%*[^0-9]%d", &width, &height);
         width = nbRead < 1 ? level->width : (width < 1 ? 1 : (width > 999 ? 999 : width));
         height = nbRead < 2 ? level->height : (height < 1 ? 1 : (height > 999 ? 999 : height));
         if (width != level->width || height != level->height) {
            resizeLevel(world, level, width, height, (width - level->width) / 2, (height - level->height) / 2);
            editor->propPanel.entity = NULL;
            editorInitLevel(editor, world->currentLevelNum);
            editorSaveWorldInHistory(editor);
         }
      }  break;

      case EDITOR_ENTITY_PANEL_PREV: {
         EntitiesListPanel * panel = &editor->entListPanel;
         panel->curPageNum = (panel->curPageNum + panel->nbPages - 1) % panel->nbPages;
      }  break;
      case EDITOR_ENTITY_PANEL_NEXT: {
         EntitiesListPanel * panel = &editor->entListPanel;
         panel->curPageNum = (panel->curPageNum + 1) % panel->nbPages;
      }  break;

      case EDITOR_WORLD_TITLE_LABEL:
      case EDITOR_WORLD_NB_PRINCESSES_LABEL:
      case EDITOR_LEVEL_MONSTER_NB_LIVES_LABEL:
      case EDITOR_LEVEL_MONSTER_MOVE_DURATION_LABEL:
      case EDITOR_LEVEL_MONSTER_INVINCIBILITY_DURATION_LABEL:
      case EDITOR_LEVEL_MONSTER_VELOCITY_LABEL:
      case EDITOR_ENTITY_CHARACTER_NB_LIVES_LABEL:
      case EDITOR_ENTITY_CHARACTER_MOVE_DURATION_LABEL:
      case EDITOR_ENTITY_CHARACTER_INVINCIBILITY_DURATION_LABEL:
      case EDITOR_ENTITY_PLAYER_MAX_NB_BOMBS_LABEL:
      case EDITOR_ENTITY_PLAYER_BOMB_RANGE_LABEL:
      case EDITOR_ENTITY_PLAYER_BOMB_BURNING_TIME_LABEL:
      case EDITOR_ENTITY_PLAYER_BOMB_EXPLOSION_TIME_LABEL:
      case EDITOR_ENTITY_MONSTER_VELOCITY_LABEL:
      case EDITOR_ENTITY_BONUS_AMOUNT_LABEL:
      case EDITOR_ENTITY_DOOR_LEVEL_INCR_LABEL:
      case EDITOR_ENTITY_BOMB_RANGE_LABEL:
      case EDITOR_ENTITY_BOMB_BURNING_DURATION_LABEL:
      case EDITOR_ENTITY_BOMB_EXPLOSION_DURATION_LABEL:
         buttonInitEdition(screen, editor->buttonList, actionCode + 1);
         break;
      case EDITOR_WORLD_TITLE:
         if (button->ustr != NULL) {
            al_ustr_trim_ws(button->ustr);
            if ((al_ustr_get(button->ustr, 0) >= 0 && world->title == NULL) ||
                  (world->title != NULL && strcmp(world->title, al_cstr(button->ustr)) != 0)) {
               al_free(world->title);
               if (al_ustr_get(button->ustr, 0) < 0) {
                  world->title = NULL;
               } else {
                  size_t titleLen = al_ustr_size(button->ustr) + 1;
                  world->title = (char *)al_malloc(titleLen * sizeof(*world->title));
                  al_ustr_to_buffer(button->ustr, world->title, titleLen);
               }
               editorSaveWorldInHistory(editor);
            }
         }
         editorUpdateButtonTexts(editor, NULL);
         break;
      case EDITOR_WORLD_FILE_FORMAT_LABEL:
      case EDITOR_WORLD_FILE_FORMAT:
         world->fileFormat = (world->fileFormat + 1) % WORLD_FF_COUNT;
         editorSaveWorldInHistory(editor);
         editorUpdateButtonTexts(editor, NULL);
         break;
      case EDITOR_WORLD_NB_PRINCESSES:
         editorModifyIntValue(editor, button->ustr, &world->nbNeededPrincesses, 0);
         break;
      case EDITOR_LEVEL_SURROUNDINGS_LABEL:
      case EDITOR_LEVEL_SURROUNDINGS:
         level->surrounding = (level->surrounding + 1) % SURROUNDING_COUNT;
         editorSaveWorldInHistory(editor);
         editorUpdateButtonTexts(editor, NULL);
         break;
      case EDITOR_LEVEL_MONSTER_NB_LIVES:
         editorModifyIntValue(editor, button->ustr, &level->canonicalMonster.charProps.nbLives, 1);
         break;
      case EDITOR_LEVEL_MONSTER_MOVE_DURATION:
         editorModifyFloatValue(editor, button->ustr, &level->canonicalMonster.charProps.move.duration, 0.);
         break;
      case EDITOR_LEVEL_MONSTER_INVINCIBILITY_DURATION:
         editorModifyFloatValue(editor, button->ustr, &level->canonicalMonster.charProps.invincibilityDuration, .1);
         break;
      case EDITOR_LEVEL_MONSTER_VELOCITY:
         editorModifyFloatValue(editor, button->ustr, &level->canonicalMonster.velocity, .1);
         break;
      case EDITOR_LEVEL_MONSTER_APPLY_TO_ALL:
         forAllEntities(level, pos, entity) {
            if (entity->type == ENTITY_MONSTER) {
               entity->monster = level->canonicalMonster;
            }
         }
         editorSaveWorldInHistory(editor);
         break;
      case EDITOR_ENTITY_CHARACTER_NB_LIVES:
         editorModifyIntValue(editor, button->ustr, &charProps->nbLives, 1);
         break;
      case EDITOR_ENTITY_CHARACTER_MOVE_DURATION:
         editorModifyFloatValue(editor, button->ustr, &charProps->move.duration, 0.);
         break;
      case EDITOR_ENTITY_CHARACTER_INVINCIBILITY_DURATION:
         editorModifyFloatValue(editor, button->ustr, &charProps->invincibilityDuration, .1);
         break;
      case EDITOR_ENTITY_PLAYER_DIRECTION_LABEL:
      case EDITOR_ENTITY_PLAYER_DIRECTION:
         charProps->dir = (charProps->dir + 1) % DIRECTION_COUNT;
         editorSaveWorldInHistory(editor);
         editorUpdateButtonTexts(editor, NULL);
         break;
      case EDITOR_ENTITY_PLAYER_MAX_NB_BOMBS:
         editorModifyIntValue(editor, button->ustr, &entity->player.maxNbBombs, 1);
         break;
      case EDITOR_ENTITY_PLAYER_BOMB_RANGE:
         editorModifyIntValue(editor, button->ustr, &entity->player.bombRange, 1);
         break;
      case EDITOR_ENTITY_PLAYER_BOMB_BURNING_TIME:
         editorModifyFloatValue(editor, button->ustr, &entity->player.bombBurningTime, .1);
         break;
      case EDITOR_ENTITY_PLAYER_BOMB_EXPLOSION_TIME:
         editorModifyFloatValue(editor, button->ustr, &entity->player.bombExplosionTime, .1);
         break;
      case EDITOR_ENTITY_MONSTER_VELOCITY:
         editorModifyFloatValue(editor, button->ustr, &entity->monster.velocity, .1);
         break;
      case EDITOR_ENTITY_BONUS_AMOUNT:
         if (entity->type == ENTITY_PRINCESS) {
            editorModifyIntValue(editor, button->ustr, &entity->princess.quantity, 1);
         } else if (doesBonusUseFactor(entity->bonus.type)) {
            editorModifyFloatValue(editor, button->ustr, &entity->bonus.factor, .1);
         } else {
            editorModifyIntValue(editor, button->ustr, &entity->bonus.amount, INT_MIN);
         }
         break;
      case EDITOR_ENTITY_DOOR_OPEN_LABEL:
      case EDITOR_ENTITY_DOOR_OPEN:
         entity->door.isOpen = !entity->door.isOpen;
         editorSaveWorldInHistory(editor);
         editorUpdateButtonTexts(editor, NULL);
         break;
      case EDITOR_ENTITY_DOOR_LEVEL_INCR:
         editorModifyIntValue(editor, button->ustr, &entity->door.levelIncr, INT_MIN);
         break;
      case EDITOR_ENTITY_DOOR_TARGET_POS:
         if (button->ustr != NULL) {
            int nbCrtsToSkip = 0;
            Position originalPos = entity->door.targetPos;
            sscanf(al_cstr(button->ustr), "%*[^0-9]%n", &nbCrtsToSkip);
            sscanf(al_cstr(button->ustr) + nbCrtsToSkip, "%d%*[^0-9]%d", &entity->door.targetPos.x, &entity->door.targetPos.y);
            if (memcmp(&originalPos, &entity->door.targetPos, sizeof(originalPos)) != 0) {
               editorSaveWorldInHistory(editor);
            }
         }
         editorUpdateButtonTexts(editor, NULL);
         break;
      case EDITOR_ENTITY_BOMB_RANGE:
         editorModifyIntValue(editor, button->ustr, &entity->bomb.range, 0);
         break;
      case EDITOR_ENTITY_BOMB_BURNING_DURATION:
         editorModifyFloatValue(editor, button->ustr, &entity->bomb.explosionInstant, 0.);
         break;
      case EDITOR_ENTITY_BOMB_EXPLOSION_DURATION:
         editorModifyFloatValue(editor, button->ustr, &entity->bomb.explosionDuration, .1);
         break;
      default: ;
   }
}


void editorRemoveIncompatible(World * world, LevelCell * cell, Entity const * newEntity)
{
   Entity * nextEntity = cell->first, * entity;
   while ((entity = nextEntity) != NULL) {
      nextEntity = entity->next;
      if (!canStartOnSameCell(entity, newEntity)) {
         setPlayerToNull(world, entity);
         removeEntity(cell, entity);
      }
   }
   removeNullPlayers(world);
}

void editorAddPlayer(World * world, Player * player)
{
   ++world->nbPlayers;
   world->players = (Player * *)al_realloc(world->players, world->nbPlayers * sizeof(*world->players));
   world->players[world->nbPlayers - 1] = player;
}

void editorCropLevel(Editor * editor, int mouseX, int mouseY)
{
   if (!isInBounds(editor->levelCanvas.bounds, mouseX, mouseY)) {
      return;
   }
   Level * level = &editor->world->levels[editor->world->currentLevelNum];
   Position tilePos = editorGetPointedTilePos(editor, mouseX, mouseY);
   Bounds originalBounds = { 0, 0, level->width, level->height }, cropBounds;
   editorGetCropBounds(level, tilePos, &cropBounds);
   if (memcmp(&originalBounds, &cropBounds, sizeof(cropBounds)) != 0) {
      resizeLevel(editor->world, level, getBoundsW(cropBounds), getBoundsH(cropBounds), -cropBounds.x1, -cropBounds.y1);
      editorInitLevel(editor, editor->world->currentLevelNum);
      editorSaveWorldInHistory(editor);
   }
}

void editorPutEntity(Editor * editor, int mouseX, int mouseY)
{
   if (!isInBounds(editor->levelCanvas.strictBounds, mouseX, mouseY)) {
      return;
   }
   Position pos = editorGetPointedTilePos(editor, mouseX, mouseY);
   World * world = editor->world;
   Level * level = &world->levels[world->currentLevelNum];
   LevelCell * cell = getLevelCell(level, pos);
   Entity const * dflEntity = &defaultEntities[editor->entListPanel.selEntityNum];
   editorRemoveIncompatible(world, cell, dflEntity);
   if (dflEntity->type == ENTITY_NONE) {
      editor->propPanel.entity = NULL;
   } else {
      Entity * newEntity = addNewEntity(cell, dflEntity);
      if (newEntity->type == ENTITY_PLAYER) {
         newEntity->player = world->nbPlayers <= 0 ? defaultPlayer : *world->players[world->nbPlayers - 1];
         editorAddPlayer(world, &newEntity->player);
      } else if (newEntity->type == ENTITY_MONSTER) {
         newEntity->monster = level->canonicalMonster;
         newEntity->monster.charProps.dir = getRandomDirection();
      } else if (newEntity->type == ENTITY_DOOR) {
         int targetLevelNum = world->currentLevelNum + newEntity->door.levelIncr;
         if (targetLevelNum < 0 || world->nbLevels <= targetLevelNum) {
            newEntity->door.targetPos = pos;
         } else {
            Level * targetLevel = &world->levels[targetLevelNum];
            double doorXRatio = (double)pos.x / level->width, doorYRatio = (double)pos.y / level->height;
            newEntity->door.targetPos = findDoorTargetPos(targetLevel, doorXRatio, doorYRatio, -newEntity->door.levelIncr);
         }
      }
      editor->propPanel.entity = newEntity;
      editor->propPanel.entityLevelNum = world->currentLevelNum;
      editor->propPanel.entityPos = pos;
   }
   editorUpdateButtonTexts(editor, NULL);
}

void editorMoveEntity(Editor * editor, int mouseX, int mouseY)
{
   if (!isInBounds(editor->levelCanvas.strictBounds, mouseX, mouseY)) {
      return;
   }
   Position pos = editorGetPointedTilePos(editor, mouseX, mouseY);
   World * world = editor->world;
   LevelCell * cell = getLevelCell(&world->levels[world->currentLevelNum], pos);
   if (editor->movedEntity == NULL) {
      if (cell->last != NULL) {
         editor->movedEntity = cell->last;
         detachEntity(cell, cell->last);
         if (editor->movedEntity->type == ENTITY_PLAYER) {
            setPlayerToNull(world, editor->movedEntity);
            removeNullPlayers(world);
         }
         editorSaveWorldInHistory(editor);
      }
   } else {
      editorRemoveIncompatible(world, cell, editor->movedEntity);
      addEntity(cell, editor->movedEntity);
      if (editor->movedEntity->type == ENTITY_PLAYER) {
         editorAddPlayer(world, &editor->movedEntity->player);
      }
      editor->movedEntity = NULL;
      editorSaveWorldInHistory(editor);
   }
}

void editorSelectEntity(Editor * editor, int mouseX, int mouseY)
{
   if (!isInBounds(editor->levelCanvas.strictBounds, mouseX, mouseY)) {
      return;
   }
   Position pos = editorGetPointedTilePos(editor, mouseX, mouseY);
   World * world = editor->world;
   editor->propPanel.entity = getLevelCell(&world->levels[world->currentLevelNum], pos)->last;
   editor->propPanel.entityLevelNum = world->currentLevelNum;
   editor->propPanel.entityPos = pos;
   editorUpdateButtonTexts(editor, NULL);
}

void editorRemoveEntity(Editor * editor, int mouseX, int mouseY)
{
   if (!isInBounds(editor->levelCanvas.strictBounds, mouseX, mouseY)) {
      return;
   }
   Position pos = editorGetPointedTilePos(editor, mouseX, mouseY);
   World * world = editor->world;
   LevelCell * cell = getLevelCell(&world->levels[world->currentLevelNum], pos);
   if (cell->last != NULL) {
      setPlayerToNull(world, cell->last);
      removeEntity(cell, cell->last);
      removeNullPlayers(world);
   }
}

void editorSelectDoorTarget(Editor * editor, int mouseX, int mouseY)
{
   if (isInBounds(editor->levelCanvas.strictBounds, mouseX, mouseY)) {
      Door oldDoor = editor->propPanel.entity->door;
      editor->propPanel.entity->door.levelIncr = editor->world->currentLevelNum - editor->propPanel.entityLevelNum;
      editor->propPanel.entity->door.targetPos = editorGetPointedTilePos(editor, mouseX, mouseY);
      editorAction(&(Screen){ .data = editor }, EDITOR_ENTITIES_PROPS);
      if (memcpy(&oldDoor, &editor->propPanel.entity->door, sizeof(oldDoor)) != 0) {
         editorSaveWorldInHistory(editor);
      }
   }
}


void processEditorEvent(Screen * screen, ALLEGRO_EVENT * evt)
{
   Editor * editor = (Editor *)screen->data;
   typedef void (* EditorFct)(Editor * editor, int mouseX, int mouseY);
   static EditorFct const editorFctsDown[EDITOR_COUNT] = {
         [EDITOR_ENTITIES_PUT] = &editorPutEntity,
         [EDITOR_ENTITIES_PROPS] = &editorSelectEntity,
         [EDITOR_ENTITIES_REMOVE] = &editorRemoveEntity,
      }, editorFctsUp[EDITOR_COUNT] = {
         [EDITOR_LEVEL_CROP] = &editorCropLevel,
         [EDITOR_ENTITIES_MOVE] = &editorMoveEntity,
         [EDITOR_ENTITY_DOOR_TARGET_POS_LABEL] = &editorSelectDoorTarget,
      };
   processEditorUIEventBefore(screen, evt);
   switch (evt->type) {
      case ALLEGRO_EVENT_DISPLAY_RESIZE:
         destroyButtonList(editor->buttonList);
         editor->buttonList = NULL;
         editorInitLevel(editor, editor->world->currentLevelNum);
         break;
      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
         if (evt->mouse.button == 1 && editor->clickedWidget == &editor->levelCanvas && editorFctsDown[editor->editMode] != NULL) {
            editor->currentlyEditingWithMouse = isAmong(int, editor->editMode, EDITOR_ENTITIES_PUT, EDITOR_ENTITIES_REMOVE);
            (*editorFctsDown[editor->editMode])(editor, evt->mouse.x, evt->mouse.y);
         }
         break;
      case ALLEGRO_EVENT_MOUSE_AXES:
      case ALLEGRO_EVENT_MOUSE_WARPED: {
         ALLEGRO_MOUSE_STATE mouseState;
         al_get_mouse_state(&mouseState);
         Position pos = editorGetPointedTilePos(editor, evt->mouse.x, evt->mouse.y),
            prevPos = editorGetPointedTilePos(editor, evt->mouse.x - evt->mouse.dx, evt->mouse.y - evt->mouse.dy);
         if (memcmp(&pos, &prevPos, sizeof(pos)) != 0) {
            if ((mouseState.buttons & 1) == 1 && editor->clickedWidget == &editor->levelCanvas &&
                  editorFctsDown[editor->editMode] != NULL) {
               editor->currentlyEditingWithMouse = true;
               (*editorFctsDown[editor->editMode])(editor, evt->mouse.x, evt->mouse.y);
            }
            editorUpdateButtonTexts(editor, NULL);
         }
      }  break;
      case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
         if (evt->mouse.button == 1 && editor->clickedWidget == &editor->levelCanvas && editorFctsUp[editor->editMode] != NULL) {
            (*editorFctsUp[editor->editMode])(editor, evt->mouse.x, evt->mouse.y);
         }
         if (editor->currentlyEditingWithMouse) {
            editorSaveWorldInHistory(editor);
            editor->currentlyEditingWithMouse = false;
         }
         break;
   }
   processEditorUIEventAfter(screen, evt);
   if (editor->editMode == EDITOR_EXIT) {
      quitScreen();
   }
}


void reinitEditor(Screen * screen)
{
   Editor * editor = (Editor *)screen->data;
   resizeDisplay(editor->propPanel.bounds.x2, editor->propPanel.bounds.y2);
   editorInitLevel(editor, editor->world->currentLevelNum);
}


void deinitEditor(Screen * screen)
{
   Editor * editor = (Editor *)screen->data;

   if (glob.quit && editor->history.savedIndex != editor->history.curIndex) {
      int response = al_show_native_message_box(glob.display, "Saving changes", "You have non-saved changes",
         "You are quitting the program but it seems you have non-saved changes. It is the only chance to save your work. "
         "Do you want to save your world now?", NULL, ALLEGRO_MESSAGEBOX_YES_NO);
      if (response == 1) {
         editorAction(screen, EDITOR_SAVE);
         char * newPath;
         while (editor->world->filePath != NULL && editor->history.savedIndex != editor->history.curIndex &&
               (newPath = getWorldPathFromDialog()) != NULL) {
            al_free(editor->world->filePath);
            editor->world->filePath = newPath;
            editorAction(screen, EDITOR_SAVE);
         }
      }
   }

   al_free(editor->movedEntity);
   destroyButtonList(editor->buttonList);
   editorDestroyHistory(&editor->history);
   destroyWorld(editor->world);
   al_free(editor);
}
