#ifndef UBOMB_EDITOR_UI_H
#define UBOMB_EDITOR_UI_H


#include "world.h"
#include "buttons.h"


typedef struct {
   Bounds bounds;
   int nbBars, barHeight;
} Toolbars;

typedef struct {
   Bounds bounds;
   int heightForType, nbPages, curPageNum, selEntityNum;
} EntitiesListPanel;

typedef struct {
   Bounds bounds;
   int spacing, labelSpacing;
   Entity * entity;
   int entityLevelNum;
   Position entityPos;
} PropertiesPanel;

typedef struct {
   Bounds bounds, strictBounds;
} CanvasWithMargin;

typedef struct Editor Editor;


void editorComputeWidgetsBounds(Editor * editor);
void editorInitButtons(Editor * editor);
void editorUpdateButtonTexts(Editor * editor, Button * buttons);
void editorRepositionButtons(Editor * editor);

void drawEditorUI(Editor const * editor, double currentTime);

void processEditorUIEventBefore(Screen * screen, ALLEGRO_EVENT * evt);
void processEditorUIEventAfter(Screen * screen, ALLEGRO_EVENT * evt);


#endif
