#ifndef UBOMB_SCREEN_H
#define UBOMB_SCREEN_H


#include "resources.h"


typedef struct Screen Screen;
struct Screen {
   void (* init)(Screen * screen);
   void (* draw)(Screen * screen);
   void (* processEvent)(Screen * screen, ALLEGRO_EVENT * evt);
   void (* update)(Screen * screen);
   void (* reinit)(Screen * screen);
   void (* deinit)(Screen * screen);
   void * data;
   double currentTime;
};


void changeScreen(Screen const * newScreen, void * data);
void quitScreen();
void quitAllScreens();
void screenIteration();

void resizeDisplay(int w, int h);
void toggleFullscreenDisplay();
void clearKeyboardState();

void drawCenteredText(FontIndex fontIndex, uint32_t color, float xRatio, float yRatio, char const * text);


extern Screen const mainMenuScreen, gameScreen, editorScreen;


#endif
