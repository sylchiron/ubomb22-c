#ifndef UBOMB_WORLD_REPO_H
#define UBOMB_WORLD_REPO_H


#include "world.h"


void initWorldRepo();

Entity const * getEntityFromChar(char crt);
char getCharOfEntity(Entity const * entity);

Position findDoorTargetPos(Level const * level, double doorXRatio, double doorYRatio, int levelIncr);
void connectLegacyDoors(World * world);

// load
World * loadWorld(char const * filePath);

// save
bool saveWorld(World const * world);

// ui
World * getWorldFromDialog();
char * getWorldPathFromDialog();


extern char const * directionNames[];
extern char const eolCrt;

extern Player const defaultPlayer;
extern Monster const defaultMonster;
extern int const nbDefaultEntities;
extern Entity defaultEntities[];


#endif
