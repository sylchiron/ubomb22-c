#include "world_repo.h"
#include "common.h"

#include <ctype.h>
#include <stdio.h>


bool strToBool(char const * str)
{
   return str != NULL && strcmp(str, "true") == 0;
}

int strToInt(char const * str, int defVal)
{
   if (str == NULL) return defVal;
   return atoi(str);
}

double strToDouble(char const * str, double defVal)
{
   if (str == NULL) return defVal;
   return atof(str);
}

double msStrToSeconds(char const * str, double defVal)
{
   if (str == NULL) return defVal;
   long ms = atol(str);
   return ms / 1000.;
}

Direction strToDirection(char const * str, Direction defVal)
{
   if (str == NULL) return defVal;
   for (Direction dir = 0; dir < DIRECTION_COUNT; ++dir) {
      if (strcmp(str, directionNames[dir]) == 0) {
         return dir;
      }
   }
   return defVal;
}

Position strToPosition(char const * str, Position defVal)
{
   if (str != NULL) {
      char const * separator = strchr(str, 'x');
      if (separator != NULL) {
         return (Position){ atoi(str), atoi(separator + 1) };
      }
   }
   return defVal;
}


bool addEntityFromChar(LevelCell * cell, char typeCrt, Level * level)
{
   Entity const * entity = getEntityFromChar(typeCrt);
   if (entity == NULL) {
      return false;
   }
   if (entity->type != ENTITY_NONE) {
      Entity * newEntity = addNewEntity(cell, entity);
      if (newEntity->type == ENTITY_MONSTER) {
         newEntity->monster = level->canonicalMonster;
         newEntity->monster.charProps.dir = getRandomDirection();
      }
   }
   return true;
}


void countCells(char const * data, int * crtI, int * count)
{
   if (data[*crtI] == '[') {
      while (data[*crtI] != ']' && data[*crtI] != '\0') {
         ++*crtI;
      }
   } else if (isdigit(data[*crtI])) {
      *count += data[*crtI] - '0' - 1;
   } else {
      ++*count;
      if (data[*crtI] == '|') {
         ++*crtI;
         while (data[*crtI] != '|' && data[*crtI] != '\0') {
            ++*crtI;
         }
      }
   }
   ++*crtI;
}

bool loadLevel(Level * level, char const * data)
{
   // Check that the level description is a grid and determine its size
   int crtI = 0;
   while (data[crtI] != '\0' && data[crtI] != eolCrt) {
      countCells(data, &crtI, &level->width);
   }
   if (data[crtI] != eolCrt || level->width == 0) {
      return false;
   }
   ++level->height;
   ++crtI;
   int computedWidth = 0;
   while (data[crtI] != '\0') {
      if (data[crtI] == eolCrt) {
         if (computedWidth != level->width) {
            return false;
         }
         ++level->height;
         computedWidth = 0;
         ++crtI;
      } else {
         countCells(data, &crtI, &computedWidth);
      }
   }
   if (computedWidth > 1) {
      return false;
   }

   // Add entities on cells
   level->cells = (LevelCell *)al_calloc(level->width * level->height, sizeof(*level->cells));
   LevelMatrix(level, cells);
   Position pos = {};
   LevelCell * lastCell = NULL;
   bool inCellDesc = false;
   crtI = 0;
   while (pos.y < level->height) {
      if (data[crtI] == eolCrt) {
         if (inCellDesc) {
            goto invalidCellDesc;
         }
         ++pos.y;
         pos.x = 0;
         lastCell = NULL;
      } else if (data[crtI] == '|') {
         inCellDesc = !inCellDesc;
         if (inCellDesc) {
            lastCell = &cells[pos.y][pos.x];
         } else {
            ++pos.x;
         }
      } else if (data[crtI] == '[') {
         if (lastCell == NULL || lastCell->last == NULL) {
            goto invalidCellDesc;
         }
         ++crtI;
         Entity * entity = lastCell->last;
         int nbReadCrts = 0;
         switch (entity->type) {
            case ENTITY_PRINCESS:
               sscanf(data + crtI, "%d%n", &entity->princess.quantity, &nbReadCrts);
               break;
            case ENTITY_BONUS:
               if (doesBonusUseFactor(entity->bonus.type)) {
                  sscanf(data + crtI, "%lg%n", &entity->bonus.factor, &nbReadCrts);
               } else {
                  sscanf(data + crtI, "%d%n", &entity->bonus.amount, &nbReadCrts);
               }
               break;
            case ENTITY_DOOR:
               sscanf(data + crtI, "%d,%d,%d%n",
                  &entity->door.levelIncr, &entity->door.targetPos.x, &entity->door.targetPos.y, &nbReadCrts);
               break;
            case ENTITY_BOMB:
               sscanf(data + crtI, "%d,%lg,%lg%n",
                  &entity->bomb.range, &entity->bomb.explosionInstant, &entity->bomb.explosionDuration, &nbReadCrts);
               break;
            default: ;
         }
         crtI += nbReadCrts;
         if (data[crtI] != ']') {
            goto invalidCellDesc;
         }
      } else if (isdigit(data[crtI])) {
         if (inCellDesc || lastCell == NULL || data[crtI] == '0') {
            goto invalidCellDesc;
         }
         int nb = data[crtI] - '0';
         for (int count = 1; count < nb; ++count) {
            for (Entity * entity = lastCell->first; entity != NULL; entity = entity->next) {
               addNewEntity(&cells[pos.y][pos.x], entity);
            }
            ++pos.x;
         }
      } else {
         lastCell = &cells[pos.y][pos.x];
         if (!addEntityFromChar(lastCell, data[crtI], level)) {
            goto invalidCellDesc;
         }
         if (!inCellDesc) {
            ++pos.x;
         }
      }
      ++crtI;
   }
   if (data[crtI] == 'S') {
      level->surrounding = SURROUNDING_STONE;
   } else if (data[crtI] == 'T') {
      level->surrounding = SURROUNDING_TREE;
   }

   return true;

invalidCellDesc: ;
   forAllEntitiesWithRemove(level, pos, entity) {
      al_free(entity);
   }
   al_free(level->cells);
   return false;
}


World * loadWorld(char const * filePath)
{
   ALLEGRO_CONFIG * fileData = al_load_config_file(filePath);
   if (!fileData) {
      return NULL;
   }

   World * world = createWorld();

   world->filePath = (char *)al_malloc((strlen(filePath) + 1) * sizeof(*world->filePath));
   strcpy(world->filePath, filePath);
   char const * title = al_get_config_value(fileData, NULL, "title");
   if (title != NULL) {
      world->title = (char *)al_malloc((strlen(title) + 1) * sizeof(*world->title));
      strcpy(world->title, title);
   }
   char const * fileFormat = al_get_config_value(fileData, NULL, "format");
   bool legacyMode = fileFormat == NULL;
   if (legacyMode) {
      bool useRLECompression = strToBool(al_get_config_value(fileData, NULL, "compression"));
      world->fileFormat = useRLECompression ? WORLD_FF_LEGACY_RLE : WORLD_FF_LEGACY;
   } else {
      world->fileFormat = strcmp(fileFormat, "RLE") == 0 ? WORLD_FF_ENHANCED_RLE : WORLD_FF_ENHANCED;
   }
   world->nbNeededPrincesses = strToInt(al_get_config_value(fileData, NULL, "nbNeededPrincesses"), 1);

   ALLEGRO_USTR * levelKeyStr = al_ustr_new("level1");
   while (al_get_config_value(fileData, NULL, al_cstr(levelKeyStr)) != NULL) {
      ++world->nbLevels;
      al_ustr_truncate(levelKeyStr, 5);
      al_ustr_appendf(levelKeyStr, "%d", world->nbLevels + 1);
   };
   if (world->nbLevels <= 0) {
      goto noLevels;
   }
   world->levels = (Level *)al_calloc(world->nbLevels, sizeof(*world->levels));

   Player canonicalPlayer = {
         .charProps = {
            .move = { .duration = msStrToSeconds(al_get_config_value(fileData, NULL, "playerMoveTime"),
               defaultPlayer.charProps.move.duration) },
            .dir = strToDirection(al_get_config_value(fileData, NULL, "playerDirection"), defaultPlayer.charProps.dir),
            .nbLives = strToInt(al_get_config_value(fileData, NULL, "playerLives"), defaultPlayer.charProps.nbLives),
            .invincibilityDuration = msStrToSeconds(al_get_config_value(fileData, NULL, "playerInvincibilityTime"),
               defaultPlayer.charProps.invincibilityDuration),
         },
         .maxNbBombs = strToInt(al_get_config_value(fileData, NULL, "bombBagCapacity"),
            strToInt(al_get_config_value(fileData, NULL, "maxNbBombs"), defaultPlayer.maxNbBombs)),
         .bombRange = strToInt(al_get_config_value(fileData, NULL, "bombRange"), defaultPlayer.bombRange),
         .bombBurningTime = msStrToSeconds(al_get_config_value(fileData, NULL, "bombBurningTime"), defaultPlayer.bombBurningTime),
         .bombExplosionTime = msStrToSeconds(al_get_config_value(fileData, NULL, "bombExplosionTime"),
            defaultPlayer.bombExplosionTime),
      };
   Monster canonicalMonster = {
         .charProps = {
            .move = { .duration = msStrToSeconds(al_get_config_value(fileData, NULL, "monsterMoveTime"),
               defaultMonster.charProps.move.duration) },
            .dir = defaultMonster.charProps.dir,
            .nbLives = strToInt(al_get_config_value(fileData, NULL, "monsterLives"), defaultMonster.charProps.nbLives),
            .invincibilityDuration = msStrToSeconds(al_get_config_value(fileData, NULL, "monsterInvincibilityTime"),
               defaultMonster.charProps.invincibilityDuration),
         },
         .velocity = strToDouble(al_get_config_value(fileData, NULL, "monsterVelocity"), defaultMonster.velocity * 10.) / 10.,
      };
   for (int levelNum = 0; levelNum < world->nbLevels; ++levelNum) {
      Monster * levelCanonicalMonster = &world->levels[levelNum].canonicalMonster;
      *levelCanonicalMonster = canonicalMonster;
      levelCanonicalMonster->charProps.nbLives += levelNum / 2;
      levelCanonicalMonster->velocity *= (double)(world->nbLevels + levelNum) / world->nbLevels;
   }

   for (int levelNum = 0; levelNum < world->nbLevels; ++levelNum) {
      al_ustr_truncate(levelKeyStr, 5);
      al_ustr_appendf(levelKeyStr, "%d", levelNum + 1);
      Level * level = &world->levels[levelNum];
      if (!loadLevel(level, al_get_config_value(fileData, NULL, al_cstr(levelKeyStr)))) {
         goto invalidLevel;
      }
      forAllEntities(level, pos, entity) {
         if (entity->type == ENTITY_PLAYER) {
            ++world->nbPlayers;
         }
      }
   }
   al_ustr_free(levelKeyStr);

   if (world->nbPlayers > 0) {
      world->players = (Player * *)al_calloc(world->nbPlayers, sizeof(*world->players));
   }
   int playerNum = 0;
   for (int levelNum = 0; levelNum < world->nbLevels; ++levelNum) {
      forAllEntities(&world->levels[levelNum], pos, entity) {
         if (entity->type == ENTITY_PLAYER) {
            entity->player = canonicalPlayer;
            world->players[playerNum] = &entity->player;
            ++playerNum;
         }
      }
   }
   if (legacyMode) {
      connectLegacyDoors(world);
   }

   if (world->nbPlayers <= 0) {
      Position playerPos = strToPosition(al_get_config_value(fileData, NULL, "playerPosition"),
            strToPosition(al_get_config_value(fileData, NULL, "player"), (Position){ -1, -1 }));
      if (!isInsideLevel(&world->levels[0], playerPos)) {
         goto invalidEntity;
      }
      world->nbPlayers = 1;
      world->players = (Player * *)al_malloc(sizeof(*world->players));
      Entity * entity =
         addNewEntity(getLevelCell(&world->levels[0], playerPos), &(Entity){ .type = ENTITY_PLAYER, .player = canonicalPlayer });
      world->players[0] = &entity->player;
   }
   if (!legacyMode) {
      for (int playerNum = 0; playerNum < world->nbPlayers; ++playerNum) {
         ALLEGRO_USTR * sectionNameUstr = al_ustr_newf("player%d", playerNum + 1);
         char const * sectionName = al_cstr(sectionNameUstr);
         Player * player = world->players[playerNum];
         *player = (Player){
            .charProps = {
               .move = { .duration = strToDouble(al_get_config_value(fileData, sectionName, "moveTime"),
                  player->charProps.move.duration) },
               .dir = strToDirection(al_get_config_value(fileData, sectionName, "direction"), player->charProps.dir),
               .nbLives = strToInt(al_get_config_value(fileData, sectionName, "lives"), player->charProps.nbLives),
               .invincibilityDuration = strToDouble(al_get_config_value(fileData, sectionName, "invincibilityTime"),
                  player->charProps.invincibilityDuration),
            },
            .maxNbBombs = strToInt(al_get_config_value(fileData, sectionName, "maxNbBombs"), player->maxNbBombs),
            .bombRange = strToInt(al_get_config_value(fileData, sectionName, "bombRange"), player->bombRange),
            .bombBurningTime = strToDouble(al_get_config_value(fileData, sectionName, "bombBurningTime"),
               player->bombBurningTime),
            .bombExplosionTime = strToDouble(al_get_config_value(fileData, sectionName, "bombExplosionTime"),
               player->bombExplosionTime),
         };
         al_ustr_free(sectionNameUstr);
      }
   }

   al_destroy_config(fileData);

   return world;

invalidLevel:
   al_ustr_free(levelKeyStr);
invalidEntity:
   al_free(world->levels);
noLevels:
   al_free(world);
   al_destroy_config(fileData);
   return NULL;
}
