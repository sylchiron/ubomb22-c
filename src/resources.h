#ifndef UBOMB_RESOURCES_H
#define UBOMB_RESOURCES_H


#include "world.h"

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>


typedef enum {
   FONT_BIGGEST,
   FONT_BIG_BUTTON,
   FONT_SMALL_BUTTON,
   FONT_VALUE,
   FONT_SECTION_TITLE,
   FONT_COUNT
} FontIndex;

typedef enum {
   IMAGE_CHARACTERS,
   IMAGE_DECORS,
   IMAGE_BOMBS,
   IMAGE_BANNER_IMGS,
   IMAGE_ICON,
   IMAGE_COUNT
} ImageIndex;

typedef enum {
   TILE_STONE,
   TILE_TREE,
   TILE_FRAGILE_TREE,
   TILE_BOX,
   TILE_SOLID_BOX,
   TILE_HEART,
   TILE_KEY,
   TILE_BOMB_NB_INC,
   TILE_BOMB_NB_DEC,
   TILE_BOMB_RANGE_INC,
   TILE_BOMB_RANGE_DEC,
   TILE_SPEED_INC,
   TILE_SPEED_DEC,
   TILE_PRINCESS,
} TileIndex;

typedef struct {
   ALLEGRO_FONT * fonts[FONT_COUNT];
   ALLEGRO_BITMAP * images[IMAGE_COUNT];

   int const cellW, cellH, digitW;
} GlobalResources;


ALLEGRO_PATH * getDataPath();
bool loadResources();
void destroyResources();

void drawTile(TileIndex tile, float x, float y, float opacity);
void drawPlayer(Direction dir, float x, float y, float opacity);
void drawMonster(Direction dir, float x, float y, float opacity);
void drawDoor(bool isOpen, float x, float y, float opacity);
void drawEditorDoor(bool isOpen, int levelIncr, float x, float y, float opacity);
void drawBomb(double progression, float x, float y, float opacity);
void drawExplosion(double progression, float x, float y);
void drawBannerDigit(int digit, float x, float y);
void drawBannerImg(int imgNum, float x, float y);


extern GlobalResources globRes;


#endif
