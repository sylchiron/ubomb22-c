#include "world_repo.h"
#include "common.h"
#include "resources.h"

#include <allegro5/allegro_native_dialog.h>


World * getWorldFromDialog()
{
   World * world = NULL;
   ALLEGRO_PATH * path = getDataPath();
   al_append_path_component(path, "worlds");
   ALLEGRO_FILECHOOSER * dialog = al_create_native_file_dialog(
      al_path_cstr(path, ALLEGRO_NATIVE_PATH_SEP), "Load world from file", NULL, ALLEGRO_FILECHOOSER_FILE_MUST_EXIST);
   al_destroy_path(path);
   al_show_native_file_dialog(glob.display, dialog);
   if (al_get_native_file_dialog_count(dialog) > 0) {
      char const * worldPath = al_get_native_file_dialog_path(dialog, 0);
      world = loadWorld(worldPath);
      if (world == NULL) {
         al_show_native_message_box(glob.display, "Error: World loading failed", worldPath,
            "The file you attempted to open seems not to be a valid world file, or it can’t be read. Sorry.", NULL,
            ALLEGRO_MESSAGEBOX_ERROR);
      }
   }
   al_destroy_native_file_dialog(dialog);
   return world;
}


char * getWorldPathFromDialog()
{
   char * worldPath = NULL;
   ALLEGRO_PATH * path = getDataPath();
   al_append_path_component(path, "worlds");
   ALLEGRO_FILECHOOSER * dialog = al_create_native_file_dialog(
      al_path_cstr(path, ALLEGRO_NATIVE_PATH_SEP), "Save world to file", NULL, ALLEGRO_FILECHOOSER_SAVE);
   al_destroy_path(path);
   al_show_native_file_dialog(glob.display, dialog);
   if (al_get_native_file_dialog_count(dialog) > 0) {
      char const * dialogPath = al_get_native_file_dialog_path(dialog, 0);
      worldPath = (char *)al_malloc((strlen(dialogPath) + 1) * sizeof(*worldPath));
      strcpy(worldPath, dialogPath);
   }
   al_destroy_native_file_dialog(dialog);
   return worldPath;
}
