#include "world_repo.h"
#include "common.h"


typedef struct {
   char const * key;
   ALLEGRO_USTR * value;
} ConfigEntry;


void saveConfigEntries(ALLEGRO_CONFIG * fileData, char const * sectionName, ConfigEntry entries[], int nbEntries)
{
   for (int entryNum = 0; entryNum < nbEntries; ++entryNum) {
      al_set_config_value(fileData, sectionName, entries[entryNum].key, al_cstr(entries[entryNum].value));
      al_ustr_free(entries[entryNum].value);
   }
}


void saveCell(ALLEGRO_USTR * levelDesc, ALLEGRO_USTR const * cellDesc, int nbEqual, int nbEntities)
{
   while (nbEqual >= 1) {
      if (nbEntities > 1) {
         al_ustr_appendf(levelDesc, "|%s|", al_cstr(cellDesc));
      } else {
         al_ustr_append(levelDesc, cellDesc);
      }
      if (nbEqual == 2 && al_ustr_length(cellDesc) == 1) {
         al_ustr_append(levelDesc, cellDesc);
         nbEqual = 0;
      } else if (nbEqual > 1) {
         int nb = nbEqual > 9 ? 9 : nbEqual;
         al_ustr_append_chr(levelDesc, nb + '0');
         nbEqual -= nb;
      } else {
         nbEqual = 0;
      }
   }
}


ALLEGRO_USTR * saveLevel(Level const * level, WorldFileFormat format)
{
   bool legacyMode = format == WORLD_FF_LEGACY || format == WORLD_FF_LEGACY_RLE,
      useRLECompression = format == WORLD_FF_LEGACY_RLE || format == WORLD_FF_ENHANCED_RLE;
   ALLEGRO_USTR * desc = al_ustr_new("");
   Position pos;
   for (pos.y = 0; pos.y < level->height; ++pos.y) {
      ALLEGRO_USTR * previousCellDesc = al_ustr_new(""), * cellDesc;
      int nbPreviousEntities = 0, nbEqual = 0;
      for (pos.x = 0; pos.x < level->width; ++pos.x) {
         cellDesc = al_ustr_new("");
         int nbEntities = 0;
         for (Entity const * entity = getConstLevelCell(level, pos)->first; entity != NULL; entity = entity->next) {
            if (entity->type == ENTITY_PLAYER && legacyMode) {
               continue;
            }
            char crt = getCharOfEntity(entity);
            if (crt == '\0') {
               continue;
            }
            al_ustr_append_chr(cellDesc, crt);
            ++nbEntities;
            if (legacyMode) {
               break;
            } else {
               if (entity->type == ENTITY_PRINCESS) {
                  if (entity->princess.quantity != 1) {
                     al_ustr_appendf(cellDesc, "[%d]", entity->princess.quantity);
                  }
               } else if (entity->type == ENTITY_BONUS) {
                  Entity const * canonicalEntity = getEntityFromChar(crt);
                  if (doesBonusUseFactor(entity->bonus.type)) {
                     if (entity->bonus.factor != canonicalEntity->bonus.factor) {
                        al_ustr_appendf(cellDesc, "[%lg]", entity->bonus.factor);
                     }
                  } else if (entity->bonus.amount != canonicalEntity->bonus.amount) {
                     al_ustr_appendf(cellDesc, "[%d]", entity->bonus.amount);
                  }
               } else if (entity->type == ENTITY_DOOR) {
                  al_ustr_appendf(cellDesc, "[%d,%d,%d]",
                     entity->door.levelIncr, entity->door.targetPos.x, entity->door.targetPos.y);
               } else if (entity->type == ENTITY_BOMB) {
                  al_ustr_appendf(cellDesc, "[%d,%lg,%lg]",
                     entity->bomb.range, entity->bomb.explosionInstant, entity->bomb.explosionDuration);
               }
            }
         }
         if (al_ustr_length(cellDesc) <= 0) {
            al_ustr_append_chr(cellDesc, '_');
         }
         if (useRLECompression && al_ustr_equal(previousCellDesc, cellDesc)) {
            ++nbEqual;
         } else {
            saveCell(desc, previousCellDesc, nbEqual, nbPreviousEntities);
            nbEqual = 1;
            nbPreviousEntities = nbEntities;
         }
         al_ustr_free(previousCellDesc);
         previousCellDesc = cellDesc;
      }
      saveCell(desc, previousCellDesc, nbEqual, nbPreviousEntities);
      al_ustr_free(previousCellDesc);
      al_ustr_append_chr(desc, eolCrt);
   }
   if (!legacyMode && level->surrounding != SURROUNDING_DEFAULT) {
      al_ustr_append_chr(desc, level->surrounding == SURROUNDING_STONE ? 'S' : 'T');
   }
   return desc;
}


bool saveWorld(World const * world)
{
   ALLEGRO_CONFIG * fileData = al_create_config();

   bool legacyMode = world->fileFormat == WORLD_FF_LEGACY || world->fileFormat == WORLD_FF_LEGACY_RLE,
      useRLECompression = world->fileFormat == WORLD_FF_LEGACY_RLE || world->fileFormat == WORLD_FF_ENHANCED_RLE;
   if (legacyMode) {
      al_set_config_value(fileData, NULL, "compression", useRLECompression ? "true" : "false");
   } else {
      al_set_config_value(fileData, NULL, "format", useRLECompression ? "RLE" : "flat");
   }
   if (world->title != NULL) {
      al_set_config_value(fileData, NULL, "title", world->title);
   }

   if (legacyMode) {
      ALLEGRO_USTR * nbLevelsStr = al_ustr_newf("%d", world->nbLevels);
      al_set_config_value(fileData, NULL, "levels", al_cstr(nbLevelsStr));
      al_ustr_free(nbLevelsStr);
   }
   for (int levelNum = 0; levelNum < world->nbLevels; ++levelNum) {
      ALLEGRO_USTR * levelKeyStr = al_ustr_newf("level%d", levelNum + 1),
         * levelContentsStr = saveLevel(&world->levels[levelNum], world->fileFormat);
      al_set_config_value(fileData, NULL, al_cstr(levelKeyStr), al_cstr(levelContentsStr));
      al_ustr_free(levelKeyStr);
      al_ustr_free(levelContentsStr);
   }

   ALLEGRO_USTR * nbPrincessesStr = al_ustr_newf("%d", world->nbNeededPrincesses);
   al_set_config_value(fileData, NULL, "nbNeededPrincesses", al_cstr(nbPrincessesStr));
   al_ustr_free(nbPrincessesStr);

   if (legacyMode) {
      Level * level1 = &world->levels[0];

      Position playerPos = {};
      forAllConstEntities(level1, pos, entity) {
         if (entity->type == ENTITY_PLAYER) {
            playerPos = pos;
            pos = (Position){ level1->width, level1->height };
         }
      }

      Player const * player = world->players[0];
      ConfigEntry playerEntries[] = {
            { "player", al_ustr_newf("%dx%d", playerPos.x, playerPos.y) },
            { "playerDirection", al_ustr_new(directionNames[player->charProps.dir]) },
            { "playerMoveTime", al_ustr_newf("%d", (int)(player->charProps.move.duration * 1000.)) },
            { "playerLives", al_ustr_newf("%d", player->charProps.nbLives) },
            { "playerInvincibilityTime", al_ustr_newf("%d", (int)(player->charProps.invincibilityDuration * 1000.)) },
            { "bombBagCapacity", al_ustr_newf("%d", player->maxNbBombs) },
            { "bombRange", al_ustr_newf("%d", player->bombRange) },
            { "bombBurningTime", al_ustr_newf("%d", (int)(player->bombBurningTime * 1000.)) },
            { "bombExplosionTime", al_ustr_newf("%d", (int)(player->bombExplosionTime * 1000.)) },
         };
      saveConfigEntries(fileData, NULL, playerEntries, arraySize(playerEntries));

      Monster * monster = &level1->canonicalMonster;
      ConfigEntry monsterEntries[] = {
            { "monsterMoveTime", al_ustr_newf("%d", (int)(monster->charProps.move.duration * 1000.)) },
            { "monsterLives", al_ustr_newf("%d", monster->charProps.nbLives) },
            { "monsterInvincibilityTime",
               al_ustr_newf("%d", (int)(monster->charProps.invincibilityDuration * 1000.)) },
            { "monsterVelocity", al_ustr_newf("%d", (int)(monster->velocity * 10.)) },
         };
      saveConfigEntries(fileData, NULL, monsterEntries, arraySize(monsterEntries));
   } else {
      int playerNum = 0;
      for (int levelNum = 0; levelNum < world->nbLevels; ++levelNum) {
         forAllConstEntities(&world->levels[levelNum], pos, entity) {
            if (entity->type == ENTITY_PLAYER) {
               ++playerNum;
               Player const * player = &entity->player;
               ConfigEntry entries[] = {
                     { "direction", al_ustr_new(directionNames[player->charProps.dir]) },
                     { "moveTime", al_ustr_newf("%lg", player->charProps.move.duration) },
                     { "lives", al_ustr_newf("%d", player->charProps.nbLives) },
                     { "invincibilityTime", al_ustr_newf("%lg", player->charProps.invincibilityDuration) },
                     { "maxNbBombs", al_ustr_newf("%d", player->maxNbBombs) },
                     { "bombRange", al_ustr_newf("%d", player->bombRange) },
                     { "bombBurningTime", al_ustr_newf("%lg", player->bombBurningTime) },
                     { "bombExplosionTime", al_ustr_newf("%lg", player->bombExplosionTime) },
                  };
               ALLEGRO_USTR * sectionName = al_ustr_newf("player%d", playerNum);
               saveConfigEntries(fileData, al_cstr(sectionName), entries, arraySize(entries));
               al_ustr_free(sectionName);
            }
         }
      }
   }

   bool success = al_save_config_file(world->filePath, fileData);

   al_destroy_config(fileData);

   return success;
}
