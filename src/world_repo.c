#include "world_repo.h"
#include "common.h"

#include <math.h>


typedef struct {
   char crt;
   Entity entity;
} CrtWithEntity;


char const * directionNames[] = { "up", "right", "down", "left" };
char const eolCrt = 'x';
static CrtWithEntity const crtsWithEntities[] = {
      { '_', { .type = ENTITY_NONE } },
      { 'P', { .type = ENTITY_PLAYER } },
      { 'M', { .type = ENTITY_MONSTER } },
      { 'W', { .type = ENTITY_PRINCESS, .princess = { .quantity = 1 } } },
      { 'S', { .type = ENTITY_STONE } },
      { 'T', { .type = ENTITY_TREE } },
      { 't', { .type = ENTITY_FRAGILE_TREE } },
      { 'B', { .type = ENTITY_BOX } },
      { 'b', { .type = ENTITY_SOLID_BOX } },
      { 'K', { .type = ENTITY_BONUS, .bonus = { BONUS_KEY, .amount = 1 } } },
      { 'H', { .type = ENTITY_BONUS, .bonus = { BONUS_HEART, .amount = 1 } } },
      { '+', { .type = ENTITY_BONUS, .bonus = { BONUS_BOMB_NUMBER, .amount = 1 } } },
      { '-', { .type = ENTITY_BONUS, .bonus = { BONUS_BOMB_NUMBER, .amount = -1 } } },
      { '>', { .type = ENTITY_BONUS, .bonus = { BONUS_BOMB_RANGE, .amount = 1 } } },
      { '<', { .type = ENTITY_BONUS, .bonus = { BONUS_BOMB_RANGE, .amount = -1 } } },
      { '*', { .type = ENTITY_BONUS, .bonus = { BONUS_SPEED, .factor = 1. / .8 } } },
      { '/', { .type = ENTITY_BONUS, .bonus = { BONUS_SPEED, .factor = .8 } } },
      { 'V', { .type = ENTITY_DOOR, .door = { .isOpen = true, .levelIncr = -1 } } },
      { 'v', { .type = ENTITY_DOOR, .door = { .isOpen = false, .levelIncr = -1 } } },
      { 'N', { .type = ENTITY_DOOR, .door = { .isOpen = true, .levelIncr = 1 } } },
      { 'n', { .type = ENTITY_DOOR, .door = { .isOpen = false, .levelIncr = 1 } } },
      { 'X', { .type = ENTITY_BOMB, .bomb = { .range = 3, .explosionInstant = 4., .explosionDuration = .2 } } },
   };
static CrtWithEntity entitiesByCrts[arraySize(crtsWithEntities)], entitiesByType[arraySize(crtsWithEntities)];

Player const defaultPlayer = {
      .charProps = { .move = { .duration = .15 }, .dir = DIRECTION_DOWN, .nbLives = 3, .invincibilityDuration = 2. },
      .maxNbBombs = 3, .bombRange = 1, .bombBurningTime = 4., .bombExplosionTime = .2,
   };
Monster const defaultMonster = {
      .charProps = { .move = { .duration = .3 }, .dir = DIRECTION_DOWN, .nbLives = 1, .invincibilityDuration = 1. },
      .velocity = .5,
   };
int const nbDefaultEntities = arraySize(crtsWithEntities);
Entity defaultEntities[arraySize(crtsWithEntities)];


static int compareCrts(void const * ptr1, void const * ptr2)
{
   return *(char *)ptr1 - *(char *)ptr2;
}

static int compareEntities(void const * ptr1, void const * ptr2)
{
   CrtWithEntity * val1 = (CrtWithEntity *)ptr1, * val2 = (CrtWithEntity *)ptr2;
   if ((val1->entity.type == ENTITY_PRINCESS || val1->entity.type == ENTITY_BOMB) && val1->entity.type == val2->entity.type) {
      return 0;
   }
   return memcmp(&val1->entity, &val2->entity, sizeof(val1->entity));
}


void initWorldRepo()
{
   for (int entityNum = 0; entityNum < nbDefaultEntities; ++entityNum) {
      defaultEntities[entityNum] = crtsWithEntities[entityNum].entity;
      if (defaultEntities[entityNum].type == ENTITY_PLAYER) {
         defaultEntities[entityNum].player = defaultPlayer;
      } else if (defaultEntities[entityNum].type == ENTITY_MONSTER) {
         defaultEntities[entityNum].monster = defaultMonster;
      }
   }
   memcpy(entitiesByCrts, crtsWithEntities, sizeof(crtsWithEntities));
   qsort(entitiesByCrts, arraySize(entitiesByCrts), sizeof(*entitiesByCrts), &compareCrts);
   memcpy(entitiesByType, crtsWithEntities, sizeof(crtsWithEntities));
   qsort(entitiesByType, arraySize(entitiesByType), sizeof(*entitiesByType), &compareEntities);
}


Entity const * getEntityFromChar(char crt)
{
   CrtWithEntity const * crtWithEntity =
      bsearch(&(CrtWithEntity){ crt }, entitiesByCrts, arraySize(entitiesByCrts), sizeof(*entitiesByCrts), &compareCrts);
   if (crtWithEntity == NULL) {
      return NULL;
   }
   return &crtWithEntity->entity;
}

char getCharOfEntity(Entity const * entity)
{
   Entity canonicalEntity = { .type = entity->type };
   if (entity->type == ENTITY_BONUS) {
      canonicalEntity.bonus.type = entity->bonus.type;
      if (doesBonusUseFactor(entity->bonus.type)) {
         canonicalEntity.bonus.factor = entity->bonus.factor < 1. ? .8 : 1. / .8;
      } else {
         canonicalEntity.bonus.amount = entity->bonus.amount < 0 ? -1 : 1;
      }
   } else if (entity->type == ENTITY_DOOR) {
      canonicalEntity.door = (Door){ .isOpen = entity->door.isOpen, .levelIncr = entity->door.levelIncr < 0 ? -1 : 1 };
   }
   CrtWithEntity * const crtWithEntity = bsearch(&(CrtWithEntity){ .entity = canonicalEntity },
         entitiesByType, arraySize(entitiesByType), sizeof(*entitiesByType), &compareEntities);
   if (crtWithEntity == NULL) {
      return '\0';
   }
   return crtWithEntity->crt;
}


Position findDoorTargetPos(Level const * level, double doorXRatio, double doorYRatio, int levelIncr)
{
   double minDistSquare = 4.;
   Position chosenPos = { doorXRatio * level->width, doorYRatio * level->height };
   forAllConstEntities(level, pos, entity) {
      if (entity->type == ENTITY_DOOR && entity->door.levelIncr == levelIncr) {
         double xRatioDiff = (double)pos.x / level->width - doorXRatio,
            yRatioDiff = (double)pos.y / level->height - doorYRatio;
         double distSquare = xRatioDiff * xRatioDiff + yRatioDiff * yRatioDiff;
         if (distSquare < minDistSquare) {
            chosenPos = pos;
            minDistSquare = distSquare;
         }
      }
   }
   return chosenPos;
}

void connectLegacyDoors(World * world)
{
   for (int levelNum = 0; levelNum < world->nbLevels; ++levelNum) {
      Level * level = &world->levels[levelNum];
      forAllEntities(level, pos, entity) {
         if (entity->type == ENTITY_DOOR) {
            int targetLevelNum = levelNum + entity->door.levelIncr;
            if (targetLevelNum < 0 || world->nbLevels <= targetLevelNum) {
               entity->door.targetPos = pos;
            } else {
               Level * targetLevel = &world->levels[targetLevelNum];
               double doorXRatio = (double)pos.x / level->width, doorYRatio = (double)pos.y / level->height;
               entity->door.targetPos = findDoorTargetPos(targetLevel, doorXRatio, doorYRatio, -entity->door.levelIncr);
            }
         }
      }
   }
}
