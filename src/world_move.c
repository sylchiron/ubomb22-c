#include "world_actions.h"
#include "common.h"


bool canGoOn(Entity const * entity, Level const * level, Position pos)
{
   if (!isInsideLevel(level, pos)) {
      return false;
   }
   for (Entity * cellEntity = getConstLevelCell(level, pos)->first; cellEntity != NULL; cellEntity = cellEntity->next) {
      if ((entity->type == ENTITY_BOX && cellEntity->type != ENTITY_NONE) ||
            isAmong(EntityType, cellEntity->type, ENTITY_STONE, ENTITY_TREE, ENTITY_FRAGILE_TREE) ||
            (entity->type != ENTITY_PLAYER && isAmong(EntityType, cellEntity->type, ENTITY_BOX, ENTITY_SOLID_BOX, ENTITY_DOOR)) ||
            (cellEntity->type == ENTITY_DOOR && !cellEntity->door.isOpen) ||
            (isAmong(EntityType, cellEntity->type, ENTITY_BOX, ENTITY_SOLID_BOX) &&
               !canGoOn(cellEntity, level, nextPos(pos, entity->player.charProps.dir)))) {
         return false;
      }
   }
   return true;
}


void moveEntityBack(Entity * entity, Level * level, double currentTime)
{
   Movement * move = &entity->move;
   Position currentPos = move->from;
   move->from = move->to;
   move->to = currentPos;
   move->endInstant = currentTime * 2. - move->endInstant + move->duration;
   if (move->running) {
      for (Entity * cellEntity = getLevelCell(level, move->from)->first; cellEntity != NULL; cellEntity = cellEntity->next) {
         if (isAmong(EntityType, cellEntity->type, ENTITY_BOX, ENTITY_SOLID_BOX) && cellEntity->move.running) {
            moveEntityBack(cellEntity, level, currentTime);
            break;
         }
      }
   }
   move->running = !move->running;
}


bool tryMovingEntity(Entity * entity, Level * level, Position from, Direction dir, double currentTime)
{
   Movement * move = &entity->move;
   Position targetPos = nextPos(from, dir);
   if (currentTime < move->endInstant || !canGoOn(entity, level, targetPos)) {
      return false;
   }
   for (Entity * cellEntity = getLevelCell(level, targetPos)->first; cellEntity != NULL; cellEntity = cellEntity->next) {
      if (isAmong(EntityType, cellEntity->type, ENTITY_BOX, ENTITY_SOLID_BOX)) {
         cellEntity->move.duration = move->duration;
         bool moved = tryMovingEntity(cellEntity, level, targetPos, dir, currentTime);
         if (!moved) {
            return false;
         }
         break;
      }
   }
   move->running = true;
   move->from = from;
   move->to = targetPos;
   move->endInstant = currentTime + move->duration;
   return true;
}

bool tryMovingCharacter(AnimatedEntity * animEnt, World * world, Direction dir, double currentTime)
{
   Character * charProps = &animEnt->entity->character;
   if (currentTime < charProps->move.endInstant) {
      Level * level = &world->levels[animEnt->levelNum];
      if (dir == getOppositeDirection(charProps->dir) && canGoOn(animEnt->entity, level, animEnt->pos)) {
         charProps->dir = dir;
         moveEntityBack(animEnt->entity, level, currentTime);
         return true;
      }
      return false;
   }
   charProps->dir = dir;
   return tryMovingEntity(animEnt->entity, &world->levels[animEnt->levelNum], animEnt->pos, dir, currentTime);
}

bool tryMovingPlayer(Player * player, World * world, Direction dir, double currentTime)
{
   bool success = tryMovingCharacter(player->animatedEntity, world, dir, currentTime);
   if (success) {
      player->tookDoor = NULL;
      if (!player->charProps.move.running) {
         takeObjects(player, world);
      }
   }
   return success;
}


bool tryFinishingEntityMove(Entity * entity, Level * level, double currentTime)
{
   Movement * move = &entity->move;
   if (!move->running) {
      return false;
   }
   if (!canGoOn(entity, level, move->to)) {
      moveEntityBack(entity, level, currentTime);
      return false;
   }
   if (move->endInstant - currentTime > move->duration / 2.) {
      return false;
   }
   move->running = false;
   moveEntity(entity, level, move->from, move->to);
   return true;
}

bool tryFinishingCharacterMove(AnimatedEntity * animEnt, World * world, double currentTime)
{
   Level * level = &world->levels[animEnt->levelNum];
   if (!tryFinishingEntityMove(animEnt->entity, level, currentTime)) {
      return false;
   }
   Character * charProps = &animEnt->entity->character;
   animEnt->pos = charProps->move.to;
   for (Entity * cellEntity = getLevelCell(level, animEnt->pos)->first; cellEntity != NULL; cellEntity = cellEntity->next) {
      if (isAmong(EntityType, cellEntity->type, ENTITY_BOX, ENTITY_SOLID_BOX) && cellEntity->move.running) {
         cellEntity->move.running = false;
         moveEntity(cellEntity, level, animEnt->pos, nextPos(animEnt->pos, charProps->dir));
         break;
      }
   }
   return true;
}

bool tryFinishingPlayerMove(Player * player, World * world, double currentTime)
{
   bool moved = tryFinishingCharacterMove(player->animatedEntity, world, currentTime);
   if (!moved) {
      return false;
   }
   takeObjects(player, world);
   return true;
}
