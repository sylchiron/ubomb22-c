#include "screen.h"
#include "common.h"


Screen screenStack[5], * curScreen = NULL;


void changeScreen(Screen const * newScreen, void * data)
{
   if (curScreen == NULL) {
      curScreen = screenStack;
   } else {
      ++curScreen;
   }
   *curScreen = *newScreen;
   curScreen->data = data;
   curScreen->currentTime = al_get_time();
   if (curScreen->init != NULL) {
      (*curScreen->init)(curScreen);
   }
}

void quitScreen()
{
   if (curScreen != NULL && curScreen->deinit != NULL) {
      (*curScreen->deinit)(curScreen);
   }
   if (curScreen == NULL || curScreen == screenStack) {
      curScreen = NULL;
      glob.quit = true;
   } else {
      --curScreen;
      curScreen->currentTime = al_get_time();
      if (curScreen->reinit != NULL) {
         (*curScreen->reinit)(curScreen);
      }
   }
}

void quitAllScreens()
{
   glob.quit = true;
   while (curScreen != NULL) {
      if (curScreen->deinit != NULL) {
         (*curScreen->deinit)(curScreen);
      }
      if (curScreen == screenStack) {
         curScreen = NULL;
      } else {
         --curScreen;
      }
   }
}

void screenIteration()
{
   curScreen->currentTime = al_get_time();

   ALLEGRO_EVENT evt;
   bool evtObtained = al_wait_for_event_timed(glob.evtQueue, &evt, .01);
   while (evtObtained) {
      switch (evt.type) {
         case ALLEGRO_EVENT_DISPLAY_CLOSE:
            quitAllScreens();
            break;
         case ALLEGRO_EVENT_DISPLAY_SWITCH_OUT:
            clearKeyboardState();
            break;
         case ALLEGRO_EVENT_KEY_CHAR: {
            uint64_t keyComb = getKeyComb(&evt);
            if (keyComb == KEY(F11) || keyComb == KEY_COMB_1(ALT, ENTER)) {
               toggleFullscreenDisplay();
            }
         }  break;
      }
      if (!glob.quit) {
         (*curScreen->processEvent)(curScreen, &evt);
      }
      evtObtained = al_get_next_event(glob.evtQueue, &evt);
   }

   if (!glob.quit) {
      if (curScreen->update != NULL) {
         (*curScreen->update)(curScreen);
      }

      (*curScreen->draw)(curScreen);
      al_flip_display();
   }
}


void resizeDisplay(int w, int h)
{
   bool setPosition = false;
   int x, y;
   if (glob.display != NULL) {
      if (glob.reqDispW == w && glob.reqDispH == h) {
         int const testedFlags = ALLEGRO_WINDOWED | ALLEGRO_FULLSCREEN_WINDOW;
         if ((al_get_display_flags(glob.display) & testedFlags) == (al_get_new_display_flags() & testedFlags)) {
            return;
         }
      }
      al_get_window_position(glob.display, &x, &y);
      x -= (w - DISPLAY_W) / 2;
      y -= (h - DISPLAY_H) / 2;
      setPosition = (al_get_new_display_flags() & ALLEGRO_FULLSCREEN_WINDOW) != ALLEGRO_FULLSCREEN_WINDOW;
      al_unregister_event_source(glob.evtQueue, al_get_display_event_source(glob.display));
      al_destroy_display(glob.display);
      al_set_new_window_position(x, y);
   }
   glob.reqDispW = w;
   glob.reqDispH = h;
   glob.display = al_create_display(w, h);
   if (glob.display != NULL) {
      al_set_display_icon(glob.display, globRes.images[IMAGE_ICON]);
      al_register_event_source(glob.evtQueue, al_get_display_event_source(glob.display));
      if (setPosition) {
         al_set_window_position(glob.display, x, y);
      }
   }
}

void toggleFullscreenDisplay()
{
   al_set_new_display_flags((al_get_new_display_flags() ^ ALLEGRO_WINDOWED) ^ ALLEGRO_FULLSCREEN_WINDOW);
   resizeDisplay(glob.reqDispW, glob.reqDispH);
   al_emit_user_event(al_get_display_event_source(glob.display),
      &(ALLEGRO_EVENT){ .user = { .type = ALLEGRO_EVENT_DISPLAY_RESIZE } }, NULL);
}

void clearKeyboardState()
{
   al_unregister_event_source(glob.evtQueue, al_get_keyboard_event_source());
   al_clear_keyboard_state(glob.display);
   al_register_event_source(glob.evtQueue, al_get_keyboard_event_source());
}


void drawCenteredText(FontIndex fontIndex, uint32_t color, float xRatio, float yRatio, char const * text)
{
   al_draw_text(globRes.fonts[fontIndex], COLOR(color), DISPLAY_W * xRatio,
      DISPLAY_H * yRatio - al_get_font_line_height(globRes.fonts[fontIndex]) * .6f, ALLEGRO_ALIGN_CENTRE, text);
}
