#ifndef UBOMB_WORLD_ACTIONS_H
#define UBOMB_WORLD_ACTIONS_H


#include "world.h"


// move
bool canGoOn(Entity const * entity, Level const * level, Position pos);

bool tryMovingCharacter(AnimatedEntity * animEnt, World * world, Direction dir, double currentTime);
bool tryMovingPlayer(Player * player, World * world, Direction dir, double currentTime);

bool tryFinishingCharacterMove(AnimatedEntity * animEnt, World * world, double currentTime);
bool tryFinishingPlayerMove(Player * player, World * world, double currentTime);


// objects
void takeObjects(Player * player, World * world);
bool playerUseKey(Player * player, World * world);


// fight
bool tryPlacingBomb(Player * player, World * world, double currentTime);
bool loseLifeAgainstMonsters(Character * charProps, LevelCell * cell, double currentTime);
bool explode(LevelCell * cell, double currentTime);
void updateMonster(AnimatedEntity * animEnt, World * world, double currentTime);
void updateBomb(AnimatedEntity * animEnt, Level * level, double currentTime);


#endif
