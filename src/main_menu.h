#ifndef UBOMB_MAIN_MENU_H
#define UBOMB_MAIN_MENU_H


#include "buttons.h"


enum {
   MENU_MAIN,
      MENU_ABOUT,
      MENU_EXIT,
   MENU_PLAY,
      MENU_PLAY_FILE,
      MENU_PLAY_DIFFICULT,
   MENU_EDITOR,
      MENU_EDIT_NEW,
      MENU_EDIT_FILE,
   MENU_COUNT,
} MainMenuCode;

typedef struct {
   ButtonList * buttonList;
   bool difficultMode;
} MainMenu;


void initMainMenu(Screen * screen);
void drawMainMenu(Screen * screen);
void processMainMenuEvent(Screen * screen, ALLEGRO_EVENT * evt);
void reinitMainMenu(Screen * screen);
void deinitMainMenu(Screen * screen);

void mainMenuAction(Screen * screen, int actionCode);


#endif
