#include "buttons.h"
#include "common.h"

#include <uchar.h>
#include <math.h>
#include <stdio.h>


double const buttonHighlightTime = .2;


bool isInBounds(Bounds bounds, int x, int y)
{
   return bounds.x1 <= x && x < bounds.x2 && bounds.y1 <= y && y < bounds.y2;
}


char const * getButtonText(Button const * button)
{
   return button->ustr != NULL ? al_cstr(button->ustr) : button->text;
}


ButtonList * createButtonList(int nbButtons, Button const buttons[], ButtonActionFct actionFct,
   uint64_t const shortcuts[][nbButtons])
{
   ButtonList * buttonList = (ButtonList *)al_malloc(sizeof(*buttonList));
   *buttonList = (ButtonList){ .nbButtons = nbButtons, .pointedButtonNum = -1, .clickedButtonNum = -1, .editedButtonNum = -1,
      .actionFct = actionFct, .shortcuts = (uint64_t const *)shortcuts };
   buttonList->buttons = (Button *)al_calloc(nbButtons, sizeof(*buttonList->buttons));
   memcpy(buttonList->buttons, buttons, nbButtons * sizeof(*buttonList->buttons));
   recomputeButtonsBounds(buttonList, 1.f, 1.f);
   return buttonList;
}

void destroyButtonList(ButtonList * buttonList)
{
   if (buttonList != NULL) {
      for (int buttonNum = 0; buttonNum < buttonList->nbButtons; ++buttonNum) {
         al_ustr_free(buttonList->buttons[buttonNum].ustr);
      }
      al_free(buttonList->buttons);
      al_free(buttonList);
   }
}


bool computeMultilineButtonBounds(int lineNum, char const * text, int size, void * buttonPtr)
{
   Button * button = (Button *)buttonPtr;
   button->bounds.y2 += al_get_font_line_height(globRes.fonts[button->fontIndex]);
   char line[size + 1];
   sprintf(line, "%.*s", size, text);
   int width = al_get_text_width(globRes.fonts[button->fontIndex], line);
   if (width > getBoundsW(button->bounds)) {
      button->bounds.x1 = floorf(button->x * DISPLAY_W - width / 2.f);
      button->bounds.x2 = ceilf(button->x * DISPLAY_W + width / 2.f);
   }
   return true;
}

void recomputeButtonsBounds(ButtonList * buttonList, float scaleX, float scaleY)
{
   float prevY = 0.f;
   for (int buttonNum = 0; buttonNum < buttonList->nbButtons; ++buttonNum) {
      Button * button = &buttonList->buttons[buttonNum];
      char const * buttonText = getButtonText(button);
      if (buttonText != NULL) {
         button->x *= scaleX;
         button->y *= scaleY;
         float x = button->x * DISPLAY_W, y = ((button->relativeY ? prevY : 0) + button->y) * DISPLAY_H;
         if (button->maxW <= 0.f) {
            int bbx, bby, bbw, bbh;
            al_get_text_dimensions(globRes.fonts[button->fontIndex], buttonText, &bbx, &bby, &bbw, &bbh);
            bbx += x - bbw / 2.f;
            bby += y - (button->vAlignTop ? 0.f : al_get_font_line_height(globRes.fonts[button->fontIndex]) * .6f);
            button->bounds = (Bounds){ bbx, bby, bbx + bbw, bby + bbh };
         } else {
            button->bounds = (Bounds){ x, y, x, y };
            al_do_multiline_text(globRes.fonts[button->fontIndex], button->maxW, buttonText, &computeMultilineButtonBounds,
               button);
            if (button->bounds.y1 == button->bounds.y2) {
               button->bounds.y2 += al_get_font_line_height(globRes.fonts[button->fontIndex]);
            }
            if (!button->vAlignTop) {
               int bbh = getBoundsH(button->bounds);
               button->bounds.y1 -= bbh / 2.f;
               button->bounds.y2 -= bbh / 2.f;
            }
         }
         prevY = (float)button->bounds.y2 / DISPLAY_H;
      }
   }
}


bool getCursorLineInMultilineText(int lineNum, char const * text, int size, void * cursorInfoPtr)
{
   struct CursorInfo { char const * cursorPos; int lineNum; char const * textBefore; int lineLen; };
   struct CursorInfo * cursorInfo = (struct CursorInfo *)cursorInfoPtr;
   if (cursorInfo->cursorPos <= text + size) {
      *cursorInfo = (struct CursorInfo){ cursorInfo->cursorPos, lineNum, text, size };
      return false;
   }
   return true;
}

void drawButtons(double currentTime, ButtonList const * buttonList)
{
   for (int buttonNum = 0; buttonNum < buttonList->nbButtons; ++buttonNum) {
      Button const * button = &buttonList->buttons[buttonNum];
      char const * buttonText = getButtonText(button);
      if (!button->hidden && buttonText != NULL) {
         uint32_t color = button->color;
         if (buttonNum == buttonList->editedButtonNum) {
            char textBeforeCursor[buttonList->editCursorPos + 1];
            al_ustr_to_buffer(button->ustr, textBeforeCursor, buttonList->editCursorPos + 1);
            struct CursorInfo { char const * cursorPos; int lineNum; char const * textBefore; int lineLen; };
            struct CursorInfo cursorInfo = { buttonText + buttonList->editCursorPos, 0, buttonText };
            ALLEGRO_FONT * font = globRes.fonts[button->fontIndex];
            float cursorX = button->x, cursorY = button->y + (button->vAlignTop ? al_get_font_line_height(font) * .5f : 0.f);
            if (button->maxW <= 0.f) {
               cursorX += (al_get_text_width(font, textBeforeCursor) - getBoundsW(button->bounds) / 2.f) / DISPLAY_W;
            } else {
               al_do_multiline_text(globRes.fonts[button->fontIndex], button->maxW, buttonText, &getCursorLineInMultilineText,
                  &cursorInfo);
               char textOnCursorLine[cursorInfo.lineLen + 1];
               sprintf(textOnCursorLine, "%.*s", cursorInfo.lineLen, cursorInfo.textBefore);
               cursorX += (al_get_text_width(font, textBeforeCursor + (cursorInfo.textBefore - buttonText)) -
                  al_get_text_width(font, textOnCursorLine) / 2.f) / DISPLAY_W;
               cursorY = (button->bounds.y1 + (cursorInfo.lineNum + .5f) * al_get_font_line_height(font)) / DISPLAY_H;
            }
            drawCenteredText(button->fontIndex, color, cursorX, cursorY, "|");
         } else if (!button->highlighted) {
            int state = (button->endOfFading - currentTime) / buttonHighlightTime * 0xFF;
            state = state < 0 ? 0 : (state > 0xFF ? 0xFF : state);
            state = buttonNum != buttonList->pointedButtonNum ? state : 0xFF - state;
            color = 0;
            for (int compNum = 2; compNum >= 0; --compNum) {
               int comp = button->color >> (compNum * 8) & 0xFF;
               comp = (0x80 * (0xFF - state) + comp * state) / 0xFF;
               color |= comp << (compNum * 8);
            }
         }
         if (button->maxW <= 0.f) {
            drawCenteredText(button->fontIndex, color, button->x, button->y, buttonText);
         } else {
            al_draw_multiline_text(globRes.fonts[button->fontIndex], COLOR(color), button->x * DISPLAY_W, button->bounds.y1,
               button->maxW, 0.f, ALLEGRO_ALIGN_CENTRE, buttonText);
         }
      }
   }
}


void updateButtons(double currentTime, ButtonList * buttonList, int mouseX, int mouseY)
{
   int pointedButtonNum = -1;
   for (int buttonNum = 0; buttonNum < buttonList->nbButtons; ++buttonNum) {
      Button * button = &buttonList->buttons[buttonNum];
      char const * buttonText = getButtonText(button);
      if (!button->hidden && buttonText != NULL) {
         Bounds bounds = button->bounds;
         bool pointed = bounds.x1 <= mouseX && mouseX < bounds.x2 && bounds.y1 <= mouseY && mouseY < bounds.y2;
         if (pointed != (buttonNum == buttonList->pointedButtonNum)) {
            double diff = button->endOfFading - currentTime;
            button->endOfFading = currentTime + buttonHighlightTime - (diff > 0. ? diff : 0.);
         }
         if (pointed) {
            pointedButtonNum = buttonNum;
         }
      }
   }
   buttonList->pointedButtonNum = pointedButtonNum;
}

void buttonsKeyboardShortcut(Screen * screen, ButtonList * buttonList, uint64_t keyComb)
{
   uint64_t const (* shortcuts)[buttonList->nbButtons] = (uint64_t const (* )[buttonList->nbButtons])buttonList->shortcuts;
   bool prevIsEmpty = false;
   for (int shortcutNum = 0; !prevIsEmpty; ++shortcutNum) {
      prevIsEmpty = true;
      for (int actionCode = 0; actionCode < buttonList->nbButtons; ++actionCode) {
         uint64_t shortcut = shortcuts[shortcutNum][actionCode];
         if (shortcut == 0) {
            continue;
         }
         prevIsEmpty = false;
         if (keyComb == shortcut) {
            (*buttonList->actionFct)(screen, actionCode);
            return;
         }
      }
   }
}

void buttonInitEdition(Screen * screen, ButtonList * buttonList, int buttonNum)
{
   int editedButtonNum = buttonList->editedButtonNum;
   if (buttonNum != editedButtonNum) {
      Button * button = &buttonList->buttons[buttonNum];
      if (button->eraseOnEdit) {
         al_ustr_truncate(button->ustr, 0);
         button->bounds.x1 = button->bounds.x2 = (button->bounds.x1 + button->bounds.x2) / 2.f;
      }
      buttonList->editCursorPos = al_ustr_size(button->ustr);
      buttonList->editedButtonNum = buttonNum;
      if (editedButtonNum >= 0) {
         (*buttonList->actionFct)(screen, editedButtonNum);
      }
   }
}

void processEditableButtonsEvent(Screen * screen, ButtonList * buttonList, ALLEGRO_EVENT * evt)
{
   switch (evt->type) {
      case ALLEGRO_EVENT_KEY_CHAR: {
         uint64_t keyComb = getKeyComb(evt);
         buttonsKeyboardShortcut(screen, buttonList, keyComb);
         if (buttonList->editedButtonNum >= 0) {
            int buttonNum = buttonList->editedButtonNum;
            Button * button = &buttonList->buttons[buttonNum];
            bool recomputeBounds = false;
            char32_t crt = evt->keyboard.unichar;
            if (keyComb == KEY(ESCAPE)) {
               al_ustr_free(button->ustr);
               button->ustr = NULL;
               buttonList->editedButtonNum = -1;
               (*buttonList->actionFct)(screen, buttonNum);
            } else if (crt == '\n' || crt == '\r') {
               buttonList->editedButtonNum = -1;
               (*buttonList->actionFct)(screen, buttonNum);
            } else if (keyComb == KEY_COMB_1(CTRL, BACKSPACE)) {
               al_ustr_remove_range(button->ustr, 0, buttonList->editCursorPos);
               buttonList->editCursorPos = 0;
               recomputeBounds = true;
            } else if (keyComb == KEY_COMB_1(CTRL, DELETE)) {
               al_ustr_truncate(button->ustr, buttonList->editCursorPos);
               recomputeBounds = true;
            } else if (keyComb == KEY(BACKSPACE) || crt == '\b') {
               if (al_ustr_prev(button->ustr, &buttonList->editCursorPos)) {
                  al_ustr_remove_chr(button->ustr, buttonList->editCursorPos);
                  recomputeBounds = true;
               }
            } else if (keyComb == KEY(DELETE) || crt == 127) {
               al_ustr_remove_chr(button->ustr, buttonList->editCursorPos);
               recomputeBounds = true;
            } else if (keyComb == KEY(LEFT)) {
               al_ustr_prev(button->ustr, &buttonList->editCursorPos);
            } else if (keyComb == KEY(RIGHT)) {
               al_ustr_next(button->ustr, &buttonList->editCursorPos);
            } else if (keyComb == KEY(HOME)) {
               buttonList->editCursorPos = 0;
            } else if (keyComb == KEY(END)) {
               buttonList->editCursorPos = al_ustr_size(button->ustr);
            } else if (crt >= ' ') {
               buttonList->editCursorPos += al_ustr_insert_chr(button->ustr, buttonList->editCursorPos, crt);
               recomputeBounds = true;
            }
            if (recomputeBounds) {
               recomputeButtonsBounds(buttonList, 1.f, 1.f);
            }
         }
      }  break;
      case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
         if (evt->mouse.button == 1) {
            int buttonNum = buttonList->clickedButtonNum;
            if (buttonNum >= 0 && buttonNum == buttonList->pointedButtonNum && buttonList->buttons[buttonNum].editable) {
               buttonInitEdition(screen, buttonList, buttonNum);
            }
         }
         break;
   }
}

void processButtonsEvent(Screen * screen, ButtonList * buttonList, ALLEGRO_EVENT * evt)
{
   processEditableButtonsEvent(screen, buttonList, evt);
   switch (evt->type) {
      case ALLEGRO_EVENT_MOUSE_AXES:
      case ALLEGRO_EVENT_MOUSE_WARPED:
         updateButtons(screen->currentTime, buttonList, evt->mouse.x, evt->mouse.y);
         break;
      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
         if (evt->mouse.button == 1 && buttonList->pointedButtonNum >= 0) {
            buttonList->clickedButtonNum = buttonList->pointedButtonNum;
         }
         break;
      case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
         if (evt->mouse.button == 1 && buttonList->clickedButtonNum >= 0) {
            int buttonNum = buttonList->clickedButtonNum;
            buttonList->clickedButtonNum = -1;
            if (buttonNum == buttonList->pointedButtonNum && !buttonList->buttons[buttonNum].editable) {
               int editedButtonNum = buttonList->editedButtonNum;
               buttonList->editedButtonNum = -1;
               ButtonActionFct actionFct = buttonList->actionFct;
               if (editedButtonNum >= 0) {
                  (*actionFct)(screen, editedButtonNum);
               }
               (*actionFct)(screen, buttonNum);
            }
         }
         break;
   }
}
