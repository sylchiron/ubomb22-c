#ifndef UBOMB_BUTTONS_H
#define UBOMB_BUTTONS_H


#include "screen.h"


typedef void (* ButtonActionFct)(Screen * screen, int actionCode);

typedef struct {
   int x1, y1, x2, y2;
} Bounds;

typedef struct {
   float x, y;
   FontIndex fontIndex;
   uint32_t color;
   char const * text;
   ALLEGRO_USTR * ustr;
   float maxW;
   bool relativeY, vAlignTop;
   bool editable, eraseOnEdit, highlighted, hidden;
   Bounds bounds;
   double endOfFading;
} Button;

typedef struct {
   int nbButtons;
   Button * buttons;
   int pointedButtonNum, clickedButtonNum, editedButtonNum, editCursorPos;
   ButtonActionFct actionFct;
   uint64_t const * shortcuts;
} ButtonList;


static inline Bounds createBoundsFromSize(int x, int y, int w, int h)
{
   return (Bounds){ x, y, x + w, y + h };
}
static inline int getBoundsW(Bounds bounds)
{
   return bounds.x2 - bounds.x1;
}
static inline int getBoundsH(Bounds bounds)
{
   return bounds.y2 - bounds.y1;
}
bool isInBounds(Bounds bounds, int x, int y);

char const * getButtonText(Button const * button);

ButtonList * createButtonList(int nbButtons, Button const buttons[], ButtonActionFct actionFct,
   uint64_t const shortcuts[][nbButtons]);
void destroyButtonList(ButtonList * buttonList);

void recomputeButtonsBounds(ButtonList * buttonList, float scaleX, float scaleY);

void drawButtons(double currentTime, ButtonList const * buttonList);

void updateButtons(double currentTime, ButtonList * buttonList, int mouseX, int mouseY);
void buttonInitEdition(Screen * screen, ButtonList * buttonList, int buttonNum);
void processButtonsEvent(Screen * screen, ButtonList * buttonList, ALLEGRO_EVENT * evt);


#endif
